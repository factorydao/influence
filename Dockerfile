FROM node:18-alpine
WORKDIR /app
ENV NODE_ENV production
RUN npm install sharp
COPY package*.json ./
# RUN npm ci
COPY ./public ./public
COPY /.next/standalone ./
COPY /.next/static ./.next/static
COPY next.config.js ./

EXPOSE 3000
# CMD ["npm", "start"]
CMD ["node", "server.js"]