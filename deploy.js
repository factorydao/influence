const FormData = require('form-data');
const nacl_factory = require('js-nacl');
const fs = require('fs');
const { zip } = require('zip-a-folder');
const axios = require('axios');

let nacl;

function makeSig(message, privKey) {
  const encoded = nacl.encode_utf8(message);
  const sig = nacl.crypto_sign(encoded, privKey);
  return nacl.to_hex(sig);
}

async function instantiateNacl() {
  return new Promise((resolve) => {
    nacl_factory.instantiate((x) => {
      nacl = x;
      resolve(nacl);
    });
  });
}

(async () => {
  try {
    await instantiateNacl();
    const pk = nacl.from_hex(process.env.PRIVATE_KEY);
    await zip('./build', `./build.zip`);
    const sig = makeSig(`${process.env.gitlabSourceRepoName}-${process.env.gitlabBranch}`, pk);
    const formData = new FormData();
    formData.append('file', fs.createReadStream(`./build.zip`));
    await axios.post(
      `${process.env.SPACE_STATION_URL || 'http://localhost:3001/deploy'}?signature=${sig}`,
      formData,
      {
        headers: formData.getHeaders()
      }
    );
  } catch (err) {
    console.error(`Error during deployment`);
    console.error(err);
  }
})();
// const { globSource, create } = require('ipfs-http-client');
// const all = require('it-all');
// const ipfs = create({
//   host: 'ipfs.infura.io',
//   port: 5001,
//   protocol: 'https',
//   apiPath: '/api/v0'
// });

// const deploy = async () => {
//   console.log('submit to ipfs');
//   const response = await all(
//     ipfs.addAll(globSource('./build/', { recursive: true }), { pin: true })
//   );
//   let root;

//   for await (let file of response) {
//     if (file.path === 'build') root = file;
//   }

//   if (!root) {
//     throw new Error('could not determine the CID');
//   }

//   const cid = root.cid.toString();

//   console.log(`${cid} ( https://ipfs.infura.io/ipfs/${cid} )`);
// };

// (async () => {
//   await deploy();
// })();
