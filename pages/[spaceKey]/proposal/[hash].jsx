const { useRouter } = require('next/router');
import Proposal from 'pages/Proposal';

const ProposalPage = () => {
  const router = useRouter();
  const { hash } = router.query;
  return <Proposal id={hash} />;
};

export default ProposalPage;
