import axios from 'axios';
import SEO from 'components/SEO';
import App from 'next/app';
import dynamic from 'next/dynamic';
import Head from 'next/head';
import { useEffect, useState } from 'react';
import 'react-dropdown/style.css';
import { QueryClient, QueryClientProvider } from 'react-query';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Footer from '../src/components/Footer';
import Header from '../src/components/Header';
import { GlobalStateProvider } from '../src/globalStateStore';
import '../src/styles.scss';

const PageLoading = dynamic(() => import('components/Ui/PageLoading/PageLoading.component'));
const queryClient = new QueryClient();

const isServer = () => {
  return typeof window === 'undefined';
};

async function getMetatags(id) {
  const {
    data: {
      submission: { title, description, image }
    }
  } = await axios.get(`/api/submissionimages/${id}`);

  return { title, description, image };
}

const DefaultHead = () => (
  <Head>
    <title>influence - Throw Your Weight Around</title>
    <meta
      name="description"
      content="Join the revolution. Use your voice. Organize without permission."
    />
  </Head>
);

function MyApp({ Component, pageProps, seo }) {
  const [isBrowser, setIsBrowser] = useState(false);

  useEffect(() => {
    setIsBrowser(true);
  }, []);

  return (
    <>
      {seo ? (
        <SEO
          url={seo.url}
          title={seo.title}
          description={seo.description}
          image={`${axios.defaults.baseURL}${seo.image}`}
        />
      ) : (
        <DefaultHead />
      )}
      <ToastContainer limit={2} />
      <QueryClientProvider client={queryClient}>
        {isBrowser ? (
          <GlobalStateProvider>
            <div className="main">
              <Header />
              <Component {...pageProps} />
            </div>
            <Footer />
          </GlobalStateProvider>
        ) : (
          <PageLoading />
        )}
      </QueryClientProvider>
    </>
  );
}

MyApp.getInitialProps = async (appContext) => {
  const appProps = await App.getInitialProps(appContext);
  const isServer = !!appContext.ctx.req;
  const additional = {};

  if (isServer && appContext.ctx.req.url.includes('/task/') && appContext.router.query.id) {
    additional.seo = await getMetatags(appContext.router.query.id);
  }
  return { ...appProps, ...additional };
};

export default MyApp;

if (!isServer()) {
  const getCookieValue = (name) =>
    document.cookie.match('(^|;)\\s*' + name + '\\s*=\\s*([^;]+)')?.pop() || '';

  axios.interceptors.request.use((config) => {
    const address = getCookieValue(`walletAddress`);
    const sessionToken = getCookieValue(`session_${address}`);
    const anotherContentType = config.headers['Content-Type'];

    config.headers = {
      'Content-Type': anotherContentType ?? 'application/json',
      [`session_${address}`]: sessionToken,
      credentials: 'include'
    };

    return config;
  });
}

axios.defaults.baseURL = process.env.NEXT_PUBLIC_API_URL || 'http://localhost:8080';
axios.defaults.withCredentials = true;

axios.interceptors.response.use(
  (request) => {
    return request;
  },
  (error) => {
    if (axios.isCancel(error)) throw Error(error);
    const status = error.response?.status;
    const ethersError = error.response?.data?.error?.code;
    let errorData =
      error.response?.data?.error ??
      error.response?.error ??
      error.response?.data?.error_description ??
      error.message;
    if (ethersError) {
      errorData = `${errorData.code}, ${errorData.argument}, ${errorData.value}`;
    }
    let msg;
    switch (status) {
      case 401:
        msg = '401 Unauthorized';
        break;
      case 400:
        msg = 'Bad request';
        break;
      case 500:
        msg = 'Technical error';
        break;
      default:
        msg = `'Unexpected error, status: ${status}:`;
        break;
    }
    if (console && console.error) console.error(msg, errorData);

    if (errorData) {
      toast.clearWaitingQueue();
      toast.error(
        <p className="toast__centered">
          Error occured.
          <br />
          Check console for more details
        </p>,
        { position: 'top-center' }
      );
    }
    throw Error(errorData);
  }
);
