import AdminRealmPage from 'pages/admin/SpaceForm';

const AdminRealm = () => {
  return <AdminRealmPage isEdit={false} />;
};

export default AdminRealm;