import { hosts } from 'helpers/hostList';
import { useRouter } from 'next/router';
import { useEffect } from 'react';
import SpacesPage from '../src/pages/spaces/Spaces';

const Homepage = () => {
  const router = useRouter();

  useEffect(() => {
    const currentURL = window.location.href;
    if (currentURL === hosts.ethPrague) {
      router.push(hosts.ethPraguePath);
    } else if (currentURL === hosts.ethBrno) {
      router.push(hosts.ethBrnoPath);
    }
  }, []);

  return <SpacesPage />;
};

export default Homepage;
