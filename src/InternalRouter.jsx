import CsvParser from 'pages/csvParser/CsvParser';
import SpacesPage from 'pages/spaces/Spaces';
import React from 'react';
import { Navigate, Route, Routes } from 'react-router-dom';

const CommunityForm = React.lazy(() => import('./pages/CommunityForm'));
const NewProposalPage = React.lazy(() => import('./pages/NewProposal'));
const NewQuestionPage = React.lazy(() => import('./pages/question/NewQuestion'));
const AddMerkleRoot = React.lazy(() => import('./pages/admin/AddMerkleRoot'));
const SpaceForm = React.lazy(() => import('./pages/admin/SpaceForm'));
const ProposalDetail = React.lazy(() => import('./pages/Proposal'));
const WalletBadges = React.lazy(() => import('./pages/walletBadges/WalletBadges'));
const HoldersForm = React.lazy(() => import('./pages/holdersForm/HoldersForm'));
const SubmissionsPage = React.lazy(() => import('./pages/submissions/questionsList/QuestionsList'));
const QuestionPage = React.lazy(() => import('./pages/submissions/question/Question'));
const StrategyForm = React.lazy(() => import('./pages/admin/StrategyForm'));
const StrategyList = React.lazy(() => import('./pages/admin/StrategyList'));
const Space = React.lazy(() => import('./pages/Space'));
export const isBrnoHostname = () => {
  return window.location.hostname === ETHBRNO_HOSTNAME;
};
export const ETHBRNO_HOSTNAME = 'vote.ethbrno.cz';
export const ETHBRNO_SPACEKEY = 'ethbrno';

const IndexPathElement = () => {
  switch (location.hostname) {
    case ETHBRNO_HOSTNAME:
      return <Space />;
    case 'vote.ethberlin.ooo':
      return <Navigate to="/2022" replace />;
    default:
      return <SpacesPage />;
  }
};

const InternalRouter = () => {
  return (
    <Routes>
      <Route path="/" element={<IndexPathElement />} />
      <Route path="/:key" element={<Space />} />
      <Route path="/:key/proposal/:id" element={<ProposalDetail />} />
      <Route path="/:key/admin/edit/*" element={<SpaceForm isEdit />} />
      <Route path="/:key/form" element={<CommunityForm />} />
      <Route path="/:key/admin/add-merkle-root/*" element={<AddMerkleRoot />} />
      <Route path="/:key/admin/create/" element={<NewProposalPage />} />
      <Route path="/:key/admin/task/" element={<NewQuestionPage />} />
      <Route path="/:key/submissions" element={<SubmissionsPage />} />
      <Route path="/:key/task/:hash/:id" element={<QuestionPage />} />
      <Route path="/:key/task/:hash" element={<QuestionPage />} />
      <Route path="/badges" element={<WalletBadges />} />
      <Route path="/holder-form" element={<HoldersForm />} />
      <Route path="/csv-parser" element={<CsvParser />} />
      <Route path="/admin/add-new-realm/*" element={<SpaceForm isEdit={false} />} />
      <Route path="/admin/add-strategy" element={<StrategyForm />} />
      <Route path="/admin/strategies" element={<StrategyList />} />
    </Routes>
  );
};

export default InternalRouter;
