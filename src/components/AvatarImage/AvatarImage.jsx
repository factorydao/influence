import InfluenceId from 'components/InfluenceId';
import NftImage from 'components/NftImage/NftImage';
import { getAllIdentitiesForAccount } from 'helpers/influence';
import { generateRandomBackground } from 'helpers/utils';
import { useEffect, useState } from 'react';

const AvatarImage = ({
  style,
  voter,
  nftAddress,
  voterAddress,
  isOs1155 = false,
  voterNetwork = null
}) => {
  const [voterId, setVoterId] = useState(null);
  useEffect(() => {
    if (voter || !nftAddress) return;
    (async () => {
      const voterIdentity = await getAllIdentitiesForAccount(voterAddress, voterNetwork, 'nft');
      if (voterIdentity?.length) setVoterId(voterIdentity[0]);
    })();
  }, [voter, nftAddress]);

  const nftId = voterId || voter;

  return nftId ? (
    <>
      <NftImage
        style={style}
        nftId={nftId}
        contractAddress={nftAddress}
        isOs1155={isOs1155}
        chainId={voterNetwork}
      />
    </>
  ) : (
    <InfluenceId value={generateRandomBackground(voterAddress)} style={style} />
  );
};

export default AvatarImage;
