import Label from './Ui/Label';

/**
 *
 * Badge component.
 * Small icon to show if someone is member
 * For decision space only.
 *
 * @param {string} address - wallet address
 * @param {Object} space - current space
 * @returns Badge component
 */
const Badge = ({ space, address }) => {
  const isCore = () => {
    if (!space || !space.members) return false;
    const members = space.members.map((address) => address.toLowerCase());
    return members.includes(address.toLowerCase());
  };

  return space && isCore() && <Label text="Core" />;
};
export default Badge;
