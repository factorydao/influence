import classnames from 'classnames';

const HamburgerIcon = ({ styles, isActive, onClick }) => {
  return (
    <button
      onClick={onClick}
      className={classnames(styles.hamburger, isActive ? styles.isActive : null)}>
      <span className={styles.line} />
      <span className={styles.line} />
      <span className={styles.line} />
    </button>
  );
};

export default HamburgerIcon;
