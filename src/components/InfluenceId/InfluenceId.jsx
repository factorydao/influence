import styles from './InfluenceId.module.scss';

const userProfileLogo = (bgColor) => {
  const result = `radial-gradient(
      circle at 50% 50%,
      #fff,
      ${bgColor})`;
  return result;
};

const InfluenceId = ({ style, value }) => {
  return (
    // <div className={styles.container} style={style}>
    <div className={styles.avatar} style={{ ...style, backgroundImage: userProfileLogo(value) }} />
    // </div>
  );
};
export default InfluenceId;
