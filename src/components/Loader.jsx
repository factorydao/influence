import Image from 'next/image';

/**
 * Small loader
 */
const Loader = ({ size }) => (
  <Image src="/loader.gif" alt="Loading..." width={size || 37} height={size || 37} />
);

export default Loader;
