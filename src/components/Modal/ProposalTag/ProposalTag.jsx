import classNames from 'classnames';
import Dialog from 'components/Ui/Dialog';
import { Field, Form, Formik } from 'formik';
import styles from './ProposalTag.module.scss';

const NewTagForm = ({ onSave, tagOptions }) => {
  const validate = ({ name, color }) => {
    const errors = {};
    if (!name || name.trim().length === 0) {
      errors.name = 'You must provide a name!';
    }
    if (tagOptions.some((tag) => tag.label === name.toUpperCase())) {
      errors.name = 'Tag name exist!';
    }
    if (!color) {
      errors.color = 'You must choose color!';
    }
    return errors;
  };

  return (
    <Formik
      initialValues={{
        name: '',
        color: '#000000'
      }}
      validate={validate}
      validateOnChange={false}
      validateOnBlur={false}
      onSubmit={onSave}>
      {({ errors }) => (
        <Form className={styles.form}>
          <div className={styles.inputs}>
            <div>
              <label htmlFor="proposalTagName">Name</label>
              <Field id="proposalTagName" name="name" placeholder="Max. 4 chars" maxLength={4} />
              {errors.name ? <p style={{ color: 'red' }}>{errors.name}</p> : null}
            </div>
            <div>
              <label htmlFor="proposalTagColor">Color</label>
              <Field id="proposalTagColor" name="color" type="color" />
            </div>
          </div>

          <button className={styles.save} type="submit">
            Save
          </button>
        </Form>
      )}
    </Formik>
  );
};

const NewTagModal = ({ isOpen, tagOptions, onClose, onSave }) => {
  return (
    <Dialog isOpen={isOpen} handleClose={onClose}>
      <div className={classNames('modal', styles.newProposalTag)}>
        <div className="shell">
          <h3 className="text-center mt-3">Add new proposal tag</h3>
          <div className="modal-body">
            <NewTagForm onSave={onSave} tagOptions={tagOptions} />
          </div>
        </div>
      </div>
    </Dialog>
  );
};

export default NewTagModal;
