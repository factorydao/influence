import placeholder from 'assets/placeholder.png';
import classNames from 'classnames';
import RichMedia from 'components/RichMediaComponent';
import DialogWhite from 'components/Ui/DialogWhite';
import HackathonDialogWhite from 'components/Ui/DialogWhite/HackathonDialogWhite';
import Markdown from 'components/Ui/Markdown';
import { explorer, ipfsUrl } from 'helpers/utils';
import moment from 'moment';
import Image from 'next/image';
import Link from 'next/link';
import { useEffect, useRef, useState } from 'react';
import styles from './ReadMore.module.scss';
import hackathonStyles from './hackathon.module.scss';

const ReadMore = (props) => {
  const { type } = props;
  let ReadMoreModal;
  switch (type) {
    case 'submission':
      ReadMoreModal = SubmissionReadMore;
      break;
    case 'description':
      ReadMoreModal = DescriptionReadMore;
      break;
    case 'hackathonView':
      ReadMoreModal = HackathonReadMore;
      break;
  }
  return <ReadMoreModal {...props} />;
};

const DescriptionReadMore = ({ isOpen, data, onClose }) => {
  return (
    <DialogWhite isOpen={isOpen} handleClose={onClose}>
      <div className={classNames('modal', styles.container)}>
        <span className={styles.title}>{data.value}</span>
        <div className={classNames(styles.body, 'markdown-body break-word')}>
          <Markdown className="mb-6" body={data.description} />
        </div>
        {data.richMedia ? (
          <div className={styles.body}>
            <RichMedia type={data.richMedia.type} link={data.richMedia.link} />
          </div>
        ) : null}
      </div>
    </DialogWhite>
  );
};

const SubmissionReadMore = ({ isOpen, data, onClose }) => {
  return (
    <>
      <DialogWhite isOpen={isOpen} handleClose={onClose}>
        <div className={classNames('modal', styles.container)}>
          <span className={styles.title}>{data.title}</span>
          <div>
            <span className={styles.userInfo}>
              {'Submitted by '}
              <span>
                <a
                  href={explorer(data.networkId || '1', data.contractAddress)}
                  target="_blank"
                  rel="noopener noreferrer">
                  <button className={styles.profile}>
                    <span>ID {data.userId}</span>
                  </button>
                </a>
                {` / ${moment(data.timestamp).fromNow()}`}
              </span>
            </span>
            <div className={classNames(styles.body, 'markdown-body break-word')}>
              <Markdown className="mb-6" body={data.body} />
            </div>
            {data.richMedia ? (
              <div className={styles.body}>
                <RichMedia type={data.richMedia.type} link={data.richMedia.link} />
              </div>
            ) : null}
          </div>
        </div>
      </DialogWhite>
    </>
  );
};

const HackathonReadMore = ({ isOpen, data, onClose }) => {
  const elementRef = useRef([]);
  const [isOverflowing, setIsOverflowing] = useState({ article: false, links: false });

  useEffect(() => {
    const elements = elementRef.current;
    const tags = Object.keys(elements);

    tags.map((tag) => {
      if (elements[tag]?.scrollHeight > elements[tag]?.clientHeight) {
        setIsOverflowing((prev) => {
          return { ...prev, [tag]: true };
        });
      } else {
        setIsOverflowing((prev) => {
          return { ...prev, [tag]: false };
        });
      }
    });
  }, [elementRef, isOpen]);
  return (
    <HackathonDialogWhite isOpen={isOpen} handleClose={onClose} className={hackathonStyles.dialog}>
      <div className={classNames('modal', styles.container, hackathonStyles.container)}>
        <div className={hackathonStyles.heading}>
          <Image
            className={hackathonStyles.image}
            src={data.imageLink?.length ? ipfsUrl(data.imageLink) : placeholder}
            width={200}
            height={200}
            alt="cover image"
          />
          <div>
            <h1 className={hackathonStyles.title}>{data.value}</h1>
            <p className={hackathonStyles.info}>{data.info}</p>
            {data.url && (
              <div className={hackathonStyles.devfolioLink}>
                <span className={hackathonStyles.devfolioLinkHeader}>Devfolio link: </span>
                <Link rel="noopener noreferrer" target="_blank" href={data.url || ''}>
                  {data.url}
                </Link>
              </div>
            )}
            {data.prizeTracks?.length ? (
              <div className={hackathonStyles.prizeTrack}>
                <span className={hackathonStyles.prizeTrackHeader}>Prize Tracks:</span>
                <ul className={hackathonStyles.prizeTrackList}>
                  {data.prizeTracks?.map((prizeTrack, index) => (
                    <li key={index}>{prizeTrack}</li>
                  ))}
                </ul>
              </div>
            ) : null}
          </div>
        </div>
        {data.links && <span className={hackathonStyles.linkHeader}>Links</span>}
        <div className={hackathonStyles.contentContainer}>
          <article
            ref={(el) => (elementRef.current['article'] = el)}
            className={classNames(
              styles.body,
              isOverflowing.article && hackathonStyles.overflowGradient,
              'markdown-body break-word'
            )}>
            <Markdown className="mb-6" body={data.description} />
          </article>
          <div
            ref={(el) => (elementRef.current['links'] = el)}
            className={classNames(
              isOverflowing.links && hackathonStyles.overflowGradient,
              hackathonStyles.links
            )}>
            {data.links?.map((link, index) => (
              <p key={index}>
                <a href={link} target="_blank" rel="noreferrer">
                  {link}
                </a>
              </p>
            ))}
          </div>
        </div>
      </div>
    </HackathonDialogWhite>
  );
};

export default ReadMore;
