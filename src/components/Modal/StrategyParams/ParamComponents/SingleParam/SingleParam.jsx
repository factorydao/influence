import Label from 'components/Ui/Label';
import styles from './SingleParam.module.scss';

const SingleParam = ({ paramName, paramKey, dynamicParam, setFieldValue, errors }) => {
  const { defaultValue, dynamicParamData, originParamData } = dynamicParam;
  const handleCheckboxChange = (checked) => {
    setFieldValue(paramKey, {
      defaultValue: checked,
      dynamicParamData: checked ? originParamData : dynamicParamData,
      originParamData
    });
  };

  const handleChange = (event) => {
    const value = event.target.value;
    setFieldValue(paramKey, { defaultValue, dynamicParamData: value, originParamData });
  };
  const inputError = errors[paramKey]?.dynamicParamData;
  return (
    <div className={styles.container}>
      <div className={styles.left}>
        <div>{paramName}:</div>
        <div className={styles.editDefault}>
          <input
            name="default"
            value={defaultValue}
            className={styles.info}
            defaultChecked={defaultValue}
            type="checkbox"
            onChange={(event) => handleCheckboxChange(event.target.checked)}
          />
          <Label text="Default" />
        </div>
      </div>
      <div className={styles.right}>
        <input
          name={paramKey}
          placeholder="Param value"
          value={dynamicParamData}
          type="text"
          data-role="descriptioninput"
          className="input"
          disabled={defaultValue}
          onChange={(event) => handleChange(event)}
        />
        {inputError && (
          <div className={styles.error}>
            <span>{inputError}</span>
          </div>
        )}
      </div>
    </div>
  );
};

export default SingleParam;
