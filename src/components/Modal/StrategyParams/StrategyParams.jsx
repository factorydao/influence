import classNames from 'classnames';
import Dialog from 'components/Ui/Dialog';
import { toChecksumAddress } from 'evm-chain-scripts';
import { Form, Formik } from 'formik';
import * as Yup from 'yup';
import { SingleParam, ValuesPerChain } from './ParamComponents';
import styles from './StrategyParams.module.scss';

const validateContractAddress = (contractAddress) => {
  let isValid = false;
  try {
    toChecksumAddress(contractAddress);
    isValid = true;
  } catch (error) {
    console.log(error);
  }
  return isValid;
};

const StrategyParamsModal = ({
  isOpen,
  onClose,
  strategyInfo,
  acceptableChains,
  dynamicStrategyParams,
  setDynamicStrategyParams
}) => {
  const { binary, name, key } = strategyInfo;
  const strategyParams = dynamicStrategyParams ? Object.keys(dynamicStrategyParams) : [];

  const validateAddresses = (value) =>
    Yup.object({
      defaultValue: Yup.boolean(),
      dynamicParamData: Yup.mixed().when('defaultValue', {
        is: false,
        then: Yup.array(
          Yup.object({
            chainId: validateChainId(value.dynamicParamData),
            value: Yup.string()
              .required('Value is required')
              .test('test-name', 'Wrong address value', (value) => validateContractAddress(value))
          })
        )
      })
    });

  const validationSchema = Yup.object().shape({
    address: Yup.lazy((value) => {
      if (value !== undefined) {
        return Yup.object({
          dynamicParamData: Yup.string().when('defaultValue', {
            is: false,
            then: Yup.string().test('test-name', 'Wrong address value', (value) =>
              validateContractAddress(value)
            )
          })
        });
      }
      return Yup.mixed().notRequired();
    }),
    maxScore: Yup.object({
      dynamicParamData: Yup.number()
        .typeError('Please input number value')
        .integer('Please input integer value')
        .positive('Please input positive value')
    }),
    addresses: Yup.lazy((value) => {
      if (value !== undefined) {
        return validateAddresses(value);
      }
      return Yup.mixed().notRequired();
    }),
    nftAddresses: Yup.lazy((value) => {
      if (value !== undefined) {
        return validateAddresses(value);
      }
      return Yup.mixed().notRequired();
    }),
    depositPools: Yup.lazy((value) => {
      if (value !== undefined) {
        return Yup.object({
          defaultValue: Yup.boolean(),
          dynamicParamData: Yup.mixed().when('defaultValue', {
            is: false,
            then: Yup.array(
              Yup.object({
                chainId: validateChainId(value.dynamicParamData),
                value: Yup.number().typeError('Please input number value').notRequired()
              })
            )
          })
        });
      }
      return Yup.mixed().notRequired();
    }),
    depositMaxScore: Yup.object({
      dynamicParamData: Yup.number().typeError('Please input number value')
    }),
    minBalance: Yup.object({
      dynamicParamData: Yup.number().typeError('Please input number value')
    }),
    divider: Yup.object({
      dynamicParamData: Yup.number().typeError('Please input number value')
    }),
    powerPerIdentity: Yup.object({
      dynamicParamData: Yup.number().typeError('Please input number value')
    }),
    symbol: Yup.lazy((value) => {
      if (value !== undefined) {
        return Yup.object({
          dynamicParamData: Yup.string().when('defaultValue', {
            is: false,
            then: Yup.string()
              .matches(/^\$[A-Za-z]*$/, 'Token symbol must be string and start with $')
              .max(6, 'Please input $ and max 5 letters')
          })
        });
      }
      return Yup.mixed().notRequired();
    }),

    collectionName: Yup.lazy((value) => {
      if (value !== undefined) {
        return Yup.object({
          dynamicParamData: Yup.string().when('defaultValue', {
            is: false,
            then: Yup.string()
              .notRequired()
              .matches(/^[A-Za-z]*$/, 'Please input only letters')
          })
        });
      }
      return Yup.mixed().notRequired();
    })
  });

  const validateChainId = (dynamicParamData) =>
    Yup.number()
      .required('Chain id is required')
      .typeError('Please input number value')
      .test('is-chain-exist', 'Chain ID is already exist', (chainId) =>
        isChainNoExistYet(chainId, dynamicParamData)
      )
      .test('test-name', 'Chain ID is not allowed.', (chainId) => isAcceptableChain(chainId));

  const isChainNoExistYet = (chainId, dynamicStrategyParams) =>
    dynamicStrategyParams.map((chain) => chain.chainId).filter((id) => id === chainId).length <= 1;

  const isAcceptableChain = (chain) => acceptableChains.includes(chain);

  const allParameters = {
    address: {
      Component: SingleParam,
      paramName: 'Address'
    },
    maxScore: { Component: SingleParam, paramName: 'Max score' },
    addresses: {
      Component: ValuesPerChain,
      paramName:
        key === 'deposit-balance' || key === 'deposit-rank'
          ? 'Pool contract addresses'
          : 'Token contract addresses'
    },
    nftAddresses: {
      Component: ValuesPerChain,
      paramName: 'NFT contract addresses'
    },
    nftName: {
      Component: SingleParam,
      paramName: 'NFT token name'
    },
    depositPools: {
      Component: ValuesPerChain,
      paramName: 'Deposit pools',
      valueHeaderText: 'Pool ID'
    },
    depositMaxScore: {
      Component: SingleParam,
      paramName: 'Deposit max score'
    },
    minBalance: {
      Component: SingleParam,
      paramName: 'Min balance'
    },
    divider: {
      Component: SingleParam,
      paramName: 'Divider'
    },
    powerPerIdentity: {
      Component: SingleParam,
      paramName: 'Power per identity'
    },
    symbol: {
      Component: SingleParam,
      paramName: 'Token symbol'
    },
    collectionName: {
      Component: SingleParam,
      paramName: 'Collection Name'
    }
  };

  const onSubmitStrategyForm = (values) => {
    setDynamicStrategyParams({ ...values });
    onClose();
  };

  return (
    <Dialog isOpen={isOpen} type="submit" form="strategy-params">
      <div className={classNames('modal', styles.strategyContainer)}>
        <span className={styles.warning}>
          Warning! Don&apos;t change these values if you don&apos;t know what you are doing
        </span>
        <div className={styles.main}>
          <div className={styles.item}>
            <span className={styles.label}>Strategy name: </span>
            <span className={styles.info}>{name}</span>
          </div>
          <div className={styles.item}>
            <span className={styles.label}>Binary voting: </span>
            <span className={styles.info}>{binary ? 'Yes' : 'No'}</span>
          </div>
          <div className={styles.item}>
            <span className={styles.label}>Params:</span>
          </div>
        </div>

        <Formik
          initialValues={dynamicStrategyParams}
          enableReinitialize
          onSubmit={onSubmitStrategyForm}
          validateOnBlur
          validateOnChange
          validationSchema={validationSchema}>
          {({ errors, values, setFieldValue, touched, setFieldTouched }) => (
            <Form id="strategy-params" className={styles.params}>
              {strategyParams.map((paramKey, index) => {
                if (allParameters[paramKey]) {
                  const { Component, paramName, valueHeaderText } = allParameters[paramKey];
                  return (
                    <div className={styles.singleParam} key={`componentId${index}`}>
                      <Component
                        paramKey={paramKey}
                        paramName={paramName}
                        dynamicParam={values[paramKey]}
                        valueHeaderText={valueHeaderText}
                        acceptableChains={acceptableChains}
                        values={values}
                        setFieldValue={setFieldValue}
                        setFieldTouched={setFieldTouched}
                        errors={errors}
                        touched={touched}
                      />
                    </div>
                  );
                }
              })}
            </Form>
          )}
        </Formik>
      </div>
    </Dialog>
  );
};

export default StrategyParamsModal;
