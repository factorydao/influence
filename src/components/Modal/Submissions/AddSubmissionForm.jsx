import axios from 'axios';
import classNames from 'classnames';
import NftImage from 'components/NftImage';
import Button from 'components/Ui/Button';
import DialogWhite from 'components/Ui/DialogWhite';
import TextArea from 'components/Ui/Formik/TextArea';
import TextInput from 'components/Ui/Formik/TextInput';
import PageLoading from 'components/Ui/PageLoading/PageLoading.component';
import { Form, Formik } from 'formik';
import { useGlobalState } from 'globalStateStore';
import { shortenWalletAddress } from 'helpers/influence';
import { validateMediaUrl } from 'helpers/richMedia';
import { generateRandomBackground, getParsedUserId, toBase64 } from 'helpers/utils';
import { useState } from 'react';
import Dropdown from 'react-dropdown';
import 'react-dropdown/style.css';
import { toast } from 'react-toastify';
import styles from './AddSubmissionForm.module.scss';

const userProfileLogo = (bgColor) => {
  const result = `radial-gradient(
      circle at 50% 50%,
      #fff,
      ${bgColor})`;
  return result;
};

const AddSubmissionForm = ({
  isOpen,
  onClose,
  reloadData,
  questionHash,
  identities,
  allowRichMedia,
  maxSubmLength,
  type
}) => {
  const MEDIA_OPTIONS = ['none', 'youtube', 'vimeo', 'x.com', 'tiktok', 'image'];
  if (type === 'media') MEDIA_OPTIONS.shift();
  const [space] = useGlobalState('space');
  const [address] = useGlobalState('address');
  const [isLoading, setIsLoading] = useState(false);
  const [pageNumber, setPageNumber] = useState(1);
  const [richMediaType, setRichMediaType] = useState(MEDIA_OPTIONS[0]);
  const [imageFile, setImageFile] = useState(null);

  const identitiesDropOptions = Object.keys(identities ?? {}).map((id) => {
    const userData = getParsedUserId(id);
    let labelData = '';

    if (Object.keys(userData).length === 1) {
      labelData = 'Token';
    } else {
      const formattedNftId = userData.os1155
        ? shortenWalletAddress(userData.nftId)
        : userData.nftId;
      labelData = `${userData.nftName} ${formattedNftId}`;
    }

    return {
      value: id,
      label: labelData,
      className: classNames(styles.open, styles.option)
    };
  });

  const validate = ({ title, submission, richMediaLink }) => {
    const errors = {};
    if (!title) {
      errors.title = 'You must provide a title!';
    }
    if (title?.length > 32) errors.title = 'Title is too long, must be less than 32 characters';
    if (!type === 'text' && !submission) {
      errors.submission = 'You must provide a submission!';
    }
    if (submission.length > maxSubmLength)
      errors.maxSubmLength = 'Submission body is too long! Max: ' + maxSubmLength.toString();

    if (allowRichMedia && richMediaType !== 'none') {
      if (richMediaType === 'image') {
        if (imageFile?.error) errors.image = imageFile.error;
      } else {
        if (!richMediaLink) errors.richMediaLink = 'You must provide link to rich media';
        else if (!validateMediaUrl(richMediaType, richMediaLink))
          errors.richMediaLink = 'Wrong URL';
      }
    }
    return errors;
  };

  const onSave = async (values) => {
    try {
      setIsLoading(true);
      const payload = {
        title: values.title,
        body: values.submission,
        userId: values?.identity?.value,
        timestamp: Date.now().toFixed()
      };

      let baseFile;

      if (allowRichMedia && richMediaType !== 'none') {
        payload.richMedia = { type: richMediaType, link: values.richMediaLink };
        if (richMediaType === 'image' && imageFile?.file) baseFile = await toBase64(imageFile.file);
      }

      const msg = JSON.stringify(payload);

      const requestBody = {
        msg,
        address,
        baseFile
      };

      const { status } = await axios.post(
        `/api/tasks/${space.key}/${questionHash}/add-submission`,
        { data: requestBody, address }
      );

      if (status === 200)
        toast.success(`Successfully added submission`, { position: 'top-center' });

      await reloadData();
      onClose();
    } catch (e) {
      console.error(e);
      toast.error(e, { position: 'top-center' });
    } finally {
      setPageNumber(1);
      setIsLoading(false);
    }
  };

  const onNextClick = () => {
    setPageNumber(2);
  };

  const handleOnClose = () => {
    setPageNumber(1);
    onClose();
  };

  const onNftIdSet = (id, setFieldValue) => {
    const userData = getParsedUserId(id.value);
    setFieldValue('identity', {
      ...id,
      ...userData,
      isOs1155: !!userData.os1155
    });
  };

  const validateFile = (file) => {
    const imageFile = { file };
    const fileSize = file.size / 1024 / 1024;
    if (fileSize > 5) imageFile.error = 'File is too big, maximum size: 5MB';
    if (file['type'].split('/')[0] !== 'image') imageFile.error = 'File must be an image';
    setImageFile(imageFile);
  };
  const mediaInput = (mediaType) => {
    if (mediaType === 'image')
      return (
        <>
          <label className={styles.label}>
            Upload image here:
            <input
              name="image"
              className={styles.upload}
              onChange={(e) => validateFile(e.target.files[0])}
              type="file"
              placeholder="Insert your image here"
            />
          </label>
          {imageFile?.error ? <p className={styles.error}>{imageFile?.error}</p> : null}
        </>
      );

    return (
      <TextInput
        className={styles.inputLink}
        name="richMediaLink"
        type="text"
        placeholder="Media link"
        maxLength={256}
      />
    );
  };

  return (
    <DialogWhite isOpen={isOpen} handleClose={handleOnClose}>
      {isLoading && <PageLoading />}
      <div className={styles.container}>
        <Formik
          initialValues={{
            title: '',
            submission: '',
            identity: null
          }}
          validate={validate}
          onSubmit={onSave}>
          {({ values, errors, isValid, dirty, setFieldValue }) => (
            <Form className={styles.form}>
              <div className={styles.inputs}>
                {pageNumber === 1 && (
                  <div>
                    <div className={styles.selectText}>Select an ID</div>
                    {values.identity?.nftId ? (
                      <NftImage style={{ maxWidth: '270px' }} {...values.identity} />
                    ) : (
                      <div
                        className={styles.profileIcon}
                        style={{
                          backgroundImage: userProfileLogo(generateRandomBackground(address))
                        }}></div>
                    )}
                    <div className={styles.info}>
                      <Dropdown
                        className={styles.dropdown}
                        controlClassName={styles.control}
                        placeholderClassName={styles.placeholder}
                        menuClassName={styles.menu}
                        options={identitiesDropOptions}
                        placeholder="Select ID"
                        value={values.identity}
                        onChange={(id) => onNftIdSet(id, setFieldValue)}
                        arrowClosed={<span className={styles.arrowClosed} />}
                        arrowOpen={<span className={styles.arrowOpen} />}
                      />
                      {errors.identity ? (
                        <p style={{ color: 'red', fontSize: 18 }}>{errors.identity}</p>
                      ) : null}
                    </div>
                  </div>
                )}
                {pageNumber === 2 && (
                  <>
                    <TextInput
                      className={styles.input}
                      name="title"
                      type="text"
                      placeholder="Title"
                      maxLength={32}
                    />
                    {type !== 'media' && (
                      <TextArea
                        className={styles.textarea}
                        name="submission"
                        type="text"
                        placeholder="Type your submission here"
                      />
                    )}
                    {errors.maxSubmLength ? (
                      <p style={{ color: 'red', fontSize: 18 }}>{errors.maxSubmLength}</p>
                    ) : null}
                    {allowRichMedia && (
                      <div className={styles.info}>
                        <label htmlFor="media-type-dropdown" className={styles.label}>
                          Attach rich media:
                        </label>
                        <Dropdown
                          id="media-type-dropdown"
                          className={styles.dropdown}
                          controlClassName={styles.control}
                          placeholderClassName={styles.placeholder}
                          menuClassName={styles.menu}
                          options={MEDIA_OPTIONS}
                          value={richMediaType}
                          onChange={(type) => setRichMediaType(type.value)}
                          arrowClosed={<span className={styles.arrowClosed} />}
                          arrowOpen={<span className={styles.arrowOpen} />}
                        />
                        {richMediaType !== 'none' && mediaInput(richMediaType)}
                      </div>
                    )}
                  </>
                )}
              </div>
              {pageNumber === 1 && (
                <Button disabled={!values.identity} className={styles.save} onClick={onNextClick}>
                  next
                </Button>
              )}
              {pageNumber === 2 && (
                <Button className={styles.save} type="submit" disabled={!(isValid && dirty)}>
                  submit
                </Button>
              )}
            </Form>
          )}
        </Formik>
      </div>
    </DialogWhite>
  );
};

export default AddSubmissionForm;
