import axios from 'axios';
import classNames from 'classnames';
import Button from 'components/Ui/Button';
import Dialog from 'components/Ui/Dialog';
import Progress from 'components/Ui/Progress';
import { ethInstance } from 'evm-chain-scripts';
import { setProposalState, useGlobalState, useProposalState } from 'globalStateStore';
import cloneDeep from 'lodash/cloneDeep';
import { memo, useMemo, useState } from 'react';
import styles from './InfluenceVote.module.scss';

const validate = (choices) => {
  if (choices.some((choice) => choice.opinion.length > 400))
    return 'One of the opinion is too long, must be less than 400 characters';
};

/**
 * Opinions left confirmation dialog
 */
const OpinionsLeftConfirmation = ({ amount, max, setShowOpinionsConfirmation, submit }) => {
  const getBackToJustifyingVotes = () => setShowOpinionsConfirmation(false);
  return (
    <div className={styles.confirmation}>
      <div className={styles.topbox}>
        <p>
          {amount}/{max}
          <br />
          Votes Justified
        </p>
      </div>
      <p className={styles.text}>Are you sure you are ready to submit your vote?</p>
      <div className={styles.buttons}>
        <Button
          className={classNames(styles.button, styles.justify)}
          onClick={getBackToJustifyingVotes}>
          Justify votes
        </Button>
        <Button className={classNames(styles.button, styles.submit)} onClick={submit}>
          Submit vote
        </Button>
      </div>
    </div>
  );
};

const VotesJustifyView = ({
  items,
  setAddedOpinionsAmount,
  setShowOpinionsConfirmation,
  submit,
  itemsResult,
  setItemsResult
}) => {
  const [unsortedChoices, setUnsortedChoices] = useProposalState('unsortedChoices');
  const [errors, setErrors] = useState();

  const max = useMemo(
    () => unsortedChoices.map((x) => Math.abs(x.value)).reduce((a, b) => a + Math.abs(b), 0),
    [unsortedChoices]
  );

  const handleOpinionChange = (id, opinion) => {
    const item = itemsResult.find((c) => c.choice._id === id);
    const index = itemsResult.findIndex((c) => c.choice._id === id);
    const indexChoices = unsortedChoices.findIndex((c) => c.choice._id === id);

    if (item) {
      const editChoice = {
        ...item,
        opinion: opinion.trim()
      };

      const itemsResultCopy = [...itemsResult];
      itemsResultCopy.splice(index, 1, editChoice);

      const unsortedChoicesCopy = [...unsortedChoices];
      unsortedChoicesCopy.splice(indexChoices, 1, editChoice);

      setItemsResult(itemsResultCopy);
      setUnsortedChoices(unsortedChoicesCopy);
    }
  };

  const handleEdit = (id) => {
    const items = itemsResult.map((item) => {
      if (item.choice._id !== id) {
        return { ...item, toEdit: false, hidden: item.opinion.length === 0 };
      }

      return {
        ...item,
        toEdit: true,
        hidden: false
      };
    });

    setItemsResult(items);
  };

  const handleToggle = (id) => {
    const items = itemsResult.map((item) => {
      if (item.choice._id !== id) {
        return { ...item, toEdit: false, hidden: item.opinion.length === 0 };
      }

      return {
        ...item,
        toEdit: !item.toEdit,
        hidden: item.toEdit ? item.opinion.length === 0 : false
      };
    });

    setItemsResult(items);
  };

  const checkOpinions = async () => {
    const errors = validate(itemsResult);
    if (errors) return setErrors(errors);
    else setErrors();

    const itemsAmount = itemsResult.length;
    const addedOpinionsAmount = itemsResult.filter((vote) => !!vote.opinion).length;
    setAddedOpinionsAmount(addedOpinionsAmount);
    if (itemsAmount !== addedOpinionsAmount) {
      return setShowOpinionsConfirmation(true);
    }
    await submit();
  };

  const hasItems = items && items.length > 0;

  return (
    <div>
      <div className={styles.header}>
        <div>Justify your Vote</div>
        <div className={styles.information}>
          Make your voice heard by justifying your choices in the comment boxes below.
          <br />
          Click on an option from the list to select and write your comment.
          <br />
          Don’t want to justify? Just click “submit vote”.
        </div>
      </div>
      <div className={styles.container}>
        <div className={styles.content}>
          <div className={styles.ballot}>
            {hasItems ? (
              <>
                {itemsResult.map((item) => (
                  <div key={item.choice._id}>
                    <div
                      role="button"
                      tabIndex={0}
                      onClick={() => handleToggle(item.choice._id)}
                      onKeyDown={() => handleToggle(item.choice._id)}
                      key={`choice${item.choice._id}`}
                      className={classNames(styles.item, { [styles.empty]: item.progress === 0 })}>
                      <div className={styles.volume} key={`choice-progress${item.choice._id}`}>
                        <Progress
                          value={item.value}
                          max={max}
                          isNegative={item.progress + item.value < 0}
                        />
                      </div>
                      <div className={styles.info}>
                        <div className={styles.data}>
                          <div className={styles.text}>{item.choice.value}</div>
                          {item.choice.tag ? (
                            <div className={styles.tag}>{item.choice.tag}</div>
                          ) : null}
                        </div>
                        <div className={styles.box}>
                          <fieldset className={classNames(styles.item, styles.disabled)}>
                            <legend className={styles.disabled}>$Vote</legend>
                            <input
                              className={styles.editable}
                              disabled={true}
                              type="number"
                              placeholder="0"
                              value={item.value}
                            />
                          </fieldset>
                        </div>
                      </div>
                    </div>
                    <div className={styles.extract} hidden={item.hidden}>
                      <div className={styles.gray}>
                        {item.toEdit ? (
                          <textarea
                            className={styles.opinion}
                            maxLength={400}
                            placeholder="Justify your vote"
                            onChange={(e) => handleOpinionChange(item.choice._id, e.target.value)}
                            defaultValue={item.opinion}
                          />
                        ) : (
                          <div
                            role="button"
                            tabIndex={0}
                            className={styles.text}
                            onClick={() => handleEdit(item.choice._id)}
                            onKeyDown={() => handleEdit(item.choice._id)}>
                            {`${
                              item.opinion.length > 50
                                ? item.opinion.slice(0, 40).trim() + `...`
                                : item.opinion
                            }`}
                          </div>
                        )}
                      </div>
                    </div>
                  </div>
                ))}
              </>
            ) : (
              <p className={styles.emptyList}>List is empty</p>
            )}
          </div>
          {errors ? <p style={{ color: 'red', fontSize: 18 }}>{errors}</p> : null}
          <div className={styles.footer}>
            <div>
              <Button onClick={checkOpinions} className={styles.votebutton}>
                Submit Vote
              </Button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

/**
 *
 * Influence vote modal component.
 * Confirms the vote and allow to entry the opinion about every choices.
 *
 * @param {Boolean} isOpen - show/hide dialog
 * @param {Array} choices - Array of selected choices
 * @param {Object} strategy - proposal strategy data
 * @param {Object} initData - initial proposal data
 * @param {String} identity - identity
 * @param {String} spaceKey - key space
 * @param {Function} onFinishVote - function to call after vote
 * @param {Function} setPageLoading - function to set page loading
 *
 * @returns Confirmation dialog
 */
const InfluenceVoteModal = ({ strategy, refetchProposalData }) => {
  const [address] = useGlobalState('address');
  const [space] = useGlobalState('space');
  const [currentNetwork] = useGlobalState('currentNetwork');
  const [, setIsPageLoading] = useProposalState('isPageLoading');
  const [isOpen, setVoteDialogOpen] = useProposalState('voteDialogOpen');
  const [initData] = useProposalState('initData');
  const [identity] = useProposalState('identity');
  const [identities] = useProposalState('identities');
  const [unsortedChoices, setUnsortedChoices] = useProposalState('unsortedChoices');
  const items = cloneDeep(
    unsortedChoices.filter((choice) => choice.value !== 0).sort((a, b) => b.value - a.value)
  );

  const [showOpinionsConfirmation, setShowOpinionsConfirmation] = useState(false);
  const [addedOpinionsAmount, setAddedOpinionsAmount] = useState(0);
  const someOpinionExist = useMemo(
    () => unsortedChoices?.some((item) => !!item.opinion),
    [unsortedChoices]
  );
  const [itemsResult, setItemsResult] = useState(
    items.map((item, index) => {
      return {
        ...item,
        hidden: (!someOpinionExist && index === 0) || item.opinion ? false : true,
        opinion: item.opinion || '',
        toEdit: index === 0 ? true : false
      };
    })
  );

  const onFinish = () => {
    if (strategy?.key.includes('binary'))
      return setUnsortedChoices(
        unsortedChoices.map((i) => {
          return { ...i, progress: 0, value: 0 };
        })
      );
  };

  const handleOnClose = () => {
    onFinish();
    setVoteDialogOpen(false);
  };

  const vote = async (items) => {
    try {
      setIsPageLoading(true);

      let identitiesToSave = {};
      const isCombinedChainsVoting = strategy.params.type === 'sim-multi';
      if (isCombinedChainsVoting) {
        for (const key in identities) {
          const elem = identities[key];
          identitiesToSave[key] = elem.map((idToVote) => idToVote.nftId);
        }
      }

      const chainRelatedData = {};
      if (!isCombinedChainsVoting) {
        chainRelatedData.network = ethInstance.getChainId();
      } else if (Object.keys(identitiesToSave).length > 0) {
        chainRelatedData.contractsVoterIds = identitiesToSave;
      }
      const model = {
        proposal: initData.id,
        metadata: {},
        choice: initData.selectChoice,
        ...chainRelatedData
      };

      model.choice = items
        .filter((item) => item.value !== 0)
        .map((item) => {
          return {
            id: item.choice.id,
            value: Math.abs(item.value),
            opinion: item.opinion,
            isNegative: strategy.negativeVote && item.value < 0
          };
        });

      const getPayload = async () => {
        if (
          strategy.key === 'nft-sum' ||
          strategy.key === 'nft-sum-binary' ||
          strategy.key === 'fvt-nft-sum' ||
          strategy.key === 'fvt-nft-sum-binary'
        ) {
          const {
            data: { ids }
          } = await axios.get(
            `/api/votes/${Object.keys(identities).join(',')}/${
              model.proposal
            }?network=${currentNetwork}`
          );
          const voterIds = Object.keys(identities).filter((identity) => {
            return !ids.includes(identity);
          });
          if (!voterIds.length) throw new Error("No identities that haven't voted available");
          return { ...model, voterId: voterIds };
        }

        if (strategy.params.type === 'nft') return { ...model, voterId: identity };
        return model;
      };

      const msg = JSON.stringify({
        payload: isCombinedChainsVoting ? model : await getPayload(),
        space: space.key,
        type: 'vote',
        timestamp: Date.now().toFixed()
      });

      await axios.post('/api/votes', {
        msg,
        address
      });
    } catch (error) {
      console.log('Saving vote error: ' + error.error_description);
    } finally {
      setProposalState('voteDialogOpen', false);
      setIsPageLoading(false);
      refetchProposalData();
    }
  };

  const submit = async () => await vote(itemsResult);
  const handleShowOpinionsConfirmation = (value) => setShowOpinionsConfirmation(value);
  const handleItemsResult = (value) => setItemsResult(value);
  const handleAddedOpinionsAmount = (value) => setAddedOpinionsAmount(value);

  return (
    <Dialog isOpen={isOpen} handleClose={handleOnClose}>
      {showOpinionsConfirmation ? (
        <OpinionsLeftConfirmation
          amount={addedOpinionsAmount}
          max={itemsResult.length}
          setShowOpinionsConfirmation={handleShowOpinionsConfirmation}
          submit={submit}
        />
      ) : (
        <VotesJustifyView
          items={items}
          setAddedOpinionsAmount={handleAddedOpinionsAmount}
          setShowOpinionsConfirmation={handleShowOpinionsConfirmation}
          submit={submit}
          itemsResult={itemsResult}
          setItemsResult={handleItemsResult}
        />
      )}
    </Dialog>
  );
};

export default memo(InfluenceVoteModal);
