import classNames from 'classnames';
import Dialog from 'components/Ui/Dialog';
import TextArea from 'components/Ui/Formik/TextArea';
import { Form, Formik } from 'formik';
import { useEffect, useState } from 'react';
import CreatableSelect from 'react-select/creatable';
import * as Yup from 'yup';
import styles from './VoterListModal.module.scss';

const validationSchema = Yup.object({
  members: Yup.array().required()
});

const customStyles = {
  control: (prev) => ({
    ...prev,
    backgroundColor: '#091d2c'
  }),
  multiValue: (prev) => ({
    ...prev,
    backgroundColor: '#091d2c',
    color: '#fff'
  }),
  multiValueLabel: (prev) => ({
    ...prev,
    backgroundColor: '#091d2c',
    color: 'var(--pale-blue)'
  }),
  input: (prev) => ({
    ...prev,
    backgroundColor: '#091d2c',
    color: 'var(--pale-blue)'
  }),
  menu: (prev) => ({
    ...prev,
    backgroundColor: '#091d2c',
    color: '#fff'
  }),
  option: (prev) => ({
    ...prev,
    backgroundColor: '#091d2c',
    color: '#fff'
  })
};

const generateValuesArray = (JsonValues = []) => {
  const arr = [];

  for (const item of JsonValues) {
    arr.push(item.value);
  }

  return arr;
};

const NewListInput = ({ onClose, onSave, mode, values, network, placeholderItemKind }) => {
  const [jsonValues, setJsonValues] = useState(generateValuesArray(values));
  useEffect(() => {
    setJsonValues(generateValuesArray(values));
  }, [values, network]);

  const handleSaveExit = (values) => {
    onSave(values);
    onClose();
  };
  return (
    <Formik
      enableReinitialize
      initialValues={{
        members: JSON.stringify(jsonValues)
      }}
      validationSchema={validationSchema}
      onSubmit={handleSaveExit}>
      <Form className={styles.form}>
        <TextArea
          className={styles.textarea}
          name="members"
          type="text"
          placeholder={`Please insert here JSON Array of ${mode} ${placeholderItemKind}`}
          onBlur={(e) => onSave({ members: e.target.value })}
        />
        <button className={styles.save} type="submit">
          Save
        </button>
      </Form>
    </Formik>
  );
};

const NetworkButtons = ({ selectedNetwork, networks, handleNetworkChange }) => {
  return (
    <div className={styles.networks}>
      {networks.map((netw, index) => {
        return (
          <button
            className={selectedNetwork === netw ? styles.networkBtnActive : styles.networkBtn}
            key={index}
            onClick={() => handleNetworkChange(netw)}>
            Network: {netw}
          </button>
        );
      })}
    </div>
  );
};

const VoterListModal = ({
  networks = [1],
  option,
  blacklistSave,
  whitelistSave,
  isOpen,
  onClose,
  blacklist,
  whitelist,
  isTokenWeighted
}) => {
  const [isRawJSON, setIsRawJSON] = useState(false);
  const [networkSelected, setNetworkSelected] = useState(networks[0]);

  const saveFunc = option === 'blacklist' ? blacklistSave : whitelistSave;
  const list = option === 'blacklist' ? blacklist : whitelist;
  const listItemKind = isTokenWeighted ? 'wallet address' : 'Token ID';
  const placeholderItemKind = isTokenWeighted ? 'wallet addresses' : 'Token IDs';
  const values = list[networkSelected];

  const handleJsonChange = ({ members }) => {
    const replacement = members.substr(members.lastIndexOf(']'), members.length);
    let values = members.replace(replacement, ']');
    const val = JSON.parse(values);
    const arr = [];
    for (const i in val) {
      arr.push({ label: val[i], value: val[i], __isNew__: true });
    }
    handleJsonSavePerNetwork(arr);
  };

  useEffect(() => {
    if (!whitelist.length && !blacklist.length) {
      const tokenIdObject = {};
      networks.forEach((netw) => (tokenIdObject[netw] = []));
      whitelistSave(tokenIdObject);
      blacklistSave(tokenIdObject);
    }
  }, [networks]);

  const handleJsonSavePerNetwork = (values) => saveFunc({ ...list, [networkSelected]: values });

  const handleListChange = (prevValue, newValue) => {
    const copy = [...list[networkSelected]];
    if (newValue.action === 'remove-value') {
      const idx = copy.findIndex((i) => i === newValue.removedValue);
      copy.splice(idx, 1);
      const saveObj = { ...list, [networkSelected]: copy };
      saveFunc(saveObj);
    } else {
      const saveObj = { ...list, [networkSelected]: [...copy, newValue.option] };
      saveFunc(saveObj);
    }
  };
  const toggleRawJSON = () => setIsRawJSON(!isRawJSON);

  return (
    <Dialog isOpen={isOpen} handleClose={onClose}>
      <div className={classNames('modal', styles.modal)}>
        <div className={styles.shell}>
          <h3 className="text-center mt-3">
            Add {listItemKind} to {option}
          </h3>
          <button className={styles.save} onClick={toggleRawJSON}>
            {isRawJSON ? 'Change to select input' : 'Change to JSON RAW input'}
          </button>
          <NetworkButtons
            handleNetworkChange={setNetworkSelected}
            selectedNetwork={networkSelected}
            networks={networks}
          />
          <div className={styles.body}>
            {isRawJSON ? (
              <NewListInput
                mode={option}
                onSave={handleJsonChange}
                values={values}
                network={networkSelected}
                onClose={onClose}
                placeholderItemKind={placeholderItemKind}
              />
            ) : (
              <>
                <CreatableSelect
                  isMulti
                  onChange={handleListChange}
                  value={values}
                  className={styles.select}
                  styles={customStyles}
                />
                <button className={styles.save} onClick={onClose}>
                  Save
                </button>
              </>
            )}
          </div>
        </div>
      </div>
    </Dialog>
  );
};

export default VoterListModal;
