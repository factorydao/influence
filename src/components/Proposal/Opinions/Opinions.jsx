import InfluenceOpinions from 'components/Spaces/Influence/Blocks/Opinions';
import Icon from 'components/Ui/Icon';
import { useProposalState } from 'globalStateStore';
import { useEffect, useState } from 'react';
import Dropdown from 'react-dropdown';
import 'react-dropdown/style.css';
import styles from './Opinions.module.scss';

const dropDownOptions = [
  {
    value: 'newest',
    label: 'Newest',
    className: styles.option
  },
  {
    value: 'power',
    label: '$I Power',
    className: styles.option
  },
  {
    value: 'old',
    label: 'Old',
    className: styles.option
  }
];

const TagsList = ({ choices, handleSelectTag }) => {
  const hasTags = choices.some((choiceData) => choiceData.choice.tag?.length);
  return (
    <div className={styles.list}>
      {choices.map((choiceData, index) => {
        const { tag, value } = choiceData.choice;
        return (
          <button
            className={styles.item}
            key={`opinion-tag${index}`}
            onClick={() => handleSelectTag(hasTags ? tag : value)}>
            {hasTags ? tag : value}
          </button>
        );
      })}
    </div>
  );
};
// opinions choices
const OpinionsTab = ({ strategy }) => {
  const [opinions] = useProposalState('opinions');
  const [choices] = useProposalState('choices');
  const [opinionFilter, setOpinionFilter] = useState({ value: 'newest', label: 'Newest' });
  const [opinionSelectedTags, setOpinionSelectedTags] = useState([]);
  const [filteredOpinions, setFilteredOpinions] = useState([]);
  useEffect(() => {
    filterOpinions();
  }, [opinionSelectedTags, opinionFilter]);

  const handleOpinionFilter = (filter) => {
    setOpinionFilter(filter);
  };

  const filterOpinions = () => {
    let filtered =
      opinionSelectedTags.length > 0
        ? opinions.filter((x) => opinionSelectedTags.includes(x.tag || x.choiceValue))
        : opinions;
    let newOne = [...filtered];

    if (filtered.length > 0) {
      switch (opinionFilter.value) {
        case 'newest':
          newOne.sort((x) => x.time.getTime());
          break;
        case 'old':
          newOne.sort((a, b) => a.time.getTime() - b.time.getTime());
          break;
        case 'top-rated':
          break;
        case 'power':
          newOne.sort((a, b) => b.value - a.value);
          break;
        default:
          newOne.sort((x) => x.time.getTime());
          break;
      }
    }

    setFilteredOpinions(newOne);
  };

  const handleSelectTag = (tag) => {
    if (opinionSelectedTags.find((x) => x === tag)) {
      return;
    }

    let arr = [...opinionSelectedTags];
    arr.push(tag);
    setOpinionSelectedTags(arr);
  };

  const handleRemoveTag = (tag) => {
    let arr = [];
    arr = opinionSelectedTags.filter((item) => item !== tag);

    setOpinionSelectedTags(arr);
  };

  return (
    <>
      <div className={styles.opinions}>
        <InfluenceOpinions items={filteredOpinions} strategy={strategy} />
      </div>
      {opinions.length > 0 && (
        <div className={styles.filters}>
          <div className={styles.sort}>
            <div className={styles.label}>SORT BY</div>
            <div className={styles.dropdownBtn}>
              <Dropdown
                className={styles.dropdown}
                controlClassName={styles.control}
                placeholderClassName={styles.placeholder}
                menuClassName={styles.menu}
                options={dropDownOptions}
                value={opinionFilter}
                onChange={handleOpinionFilter}
                arrowClosed={<span className={styles.arrowClosed} />}
                arrowOpen={<span className={styles.arrowClosed} />}
              />
            </div>
          </div>
          <div className={styles.tags}>
            <div className={styles.label}>TAGS</div>
            <div className={styles.selected}>
              {opinionSelectedTags.map((selected, index) => {
                return (
                  <span key={selected}>
                    <div className={styles.item} key={`selected-tag${index}`}>
                      #{selected.replace('#', '')}
                      <Icon name="close" onClick={() => handleRemoveTag(selected)} />
                    </div>
                  </span>
                );
              })}
            </div>
            <TagsList handleSelectTag={handleSelectTag} choices={choices} />
          </div>
        </div>
      )}
    </>
  );
};

export default OpinionsTab;
