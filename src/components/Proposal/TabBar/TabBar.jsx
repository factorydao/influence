/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import classNames from 'classnames';
import Counter from 'components/Ui/Counter';
import Icon from 'components/Ui/Icon';
import { useGlobalState } from 'globalStateStore';
import { useEffect, useState } from 'react';
import styles from './TabBar.module.scss';
/**
 *
 * Tab bar component for proposal page.
 *
 * @param {Function} onItemClick - function after item click
 * @param {String} className - counter such like a Badge - show the number of voters
 * @param {Number} counter - counter such like a Badge - show the number of voters
 * @param {Boolean} showMyVoteTab - flag that allows the display of "My vote" tab
 * @returns Underline nav bar
 */

const tabItems = [
  {
    id: 2,
    text: 'Vote Results',
    tab: 'vote',
    counter: false
  }
];

const myVoteTab = {
  id: 1,
  text: 'My Vote',
  tab: 'myVote',
  counter: false
};

const proposalResultsItems = [
  {
    id: 3,
    text: 'Opinions',
    tab: 'opinions',
    counter: false
  },
  {
    id: 4,
    text: 'Votes List',
    tab: 'vlist',
    counter: true
  }
];

const TabBar = ({
  hackathonView,
  onItemClick,
  className = '',
  counter = 0,
  selectedTab,
  showMyVoteTab,
  showTabsForUserWithNft,
  hideVotesTabs,
  isLoading
}) => {
  let tabs = !hackathonView ? [...tabItems] : [...tabItems];

  const [address] = useGlobalState('address');
  const [selectedData, setSelectedData] = useState(tabs.find((x) => x.tab === selectedTab));
  const element = document.getElementById('tabBar');
  const isScrollBar = element?.scrollWidth > element?.offsetWidth;

  useEffect(() => {
    tabs.length && handleItemClick(tabs[0]);
  }, [tabs?.length]);

  useEffect(() => {
    if (!address.length && tabs.length) {
      handleItemClick(tabs[0]);
    }
  }, [address.length]);

  if (showMyVoteTab) {
    tabs.unshift(myVoteTab);
  }

  if (showTabsForUserWithNft) {
    tabs = [...tabs, ...proposalResultsItems];
  }

  if (hideVotesTabs) {
    tabs = tabs.filter((elem) => elem.id !== 2 && elem.id !== 3 && elem.id !== 4);
  }

  const handleItemClick = (item) => {
    setSelectedData(item);
    onItemClick(item.tab);
  };

  return (
    <div className={styles.container}>
      {isScrollBar ? <Icon name="back" className={styles.scrollArrow} /> : null}
      <nav id="tabBar" className={classNames(styles.tabBar, className)}>
        {isLoading ? (
          <div className={styles.loading}>Loading...</div>
        ) : (
          tabs.map((item, index) => {
            return (
              <div key={index} className={styles.item}>
                <div
                  onClick={() => handleItemClick(item)}
                  title={item.text}
                  className={classNames(styles.text, {
                    [styles.selected]: item.id === selectedData?.id
                  })}>
                  <div>{item.text}</div>
                </div>
                {item.counter && (
                  <div className={styles.counter}>
                    <Counter counter={counter} />
                  </div>
                )}
              </div>
            );
          })
        )}
      </nav>
    </div>
  );
};

export default TabBar;
