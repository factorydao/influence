import classNames from 'classnames';
import If from 'components/If';
import Binary from 'components/Spaces/Influence/Blocks/Binary';
import MyInfluence from 'components/Spaces/Influence/Blocks/MyInfluence';
import QuadraticVote from 'components/Spaces/Influence/Blocks/QuadraticVote';
import { useGlobalState, useProposalState } from 'globalStateStore';
import {
  useGetMultiChainIdentitiesDropdownOptions,
  useGetMyInfluenceProps,
  useGetUserMultiChainIdentities,
  useGetVoteComponentProps,
  useLoadChoicesData,
  useMultiChainScoreQuery
} from 'helpers/queries';
import { memo, useEffect, useState } from 'react';
import MultipleVotingNotification from '../MultipleVotingNotification';
import styles from './VoteComponent.module.scss';

const CombinedChainsVoteComponent = ({ strategy, tab }) => {
  const [address] = useGlobalState('address');
  const [choices, setChoices] = useProposalState('choices');
  const [initData] = useProposalState('initData');
  const [space] = useGlobalState('space');
  const [identity, setIdentity] = useProposalState('identity');
  const [identities, setIdentities] = useProposalState('identities');
  const [myChoices, setMyChoices] = useProposalState('myChoices');
  const [unsortedChoices, setUnsortedChoices] = useProposalState('unsortedChoices');
  const [loading] = useProposalState('loading');
  const [totalI] = useProposalState('totalI');

  const [tokensLeft, setTokensLeft] = useState(0);
  const [initBalance, setInitBalance] = useState(0);
  const [selectedIdentitesId, setSelectedIdentitiesId] = useState();
  const [infResults, setInfResults] = useState([]);

  const { id, isOpenVote, hasMyVotesVoted, votes, payload, isProposalVisible, results, isDraft } =
    initData;

  const { data: score, isLoading: tokensLoading } = useMultiChainScoreQuery({
    address,
    proposalHash: id
  });

  const { data: myInfluenceProps, isFetching: myInfluencePropsFetching } = useGetMyInfluenceProps(
    tab,
    isProposalVisible,
    address,
    totalI,
    strategy,
    choices
  );

  const { data: voteComponentProps, isFetching: voteComponentPropsFetching } =
    useGetVoteComponentProps(address, strategy, setUnsortedChoices, setTokensLeft, tab, choices);

  const { data: identitySelectableOptions, isFetching: identitySelectableOptionsFetching } =
    useGetMultiChainIdentitiesDropdownOptions(identities, styles.option);

  const { data: userIdentities, isFetching: userIdentitiesFetching } =
    useGetUserMultiChainIdentities(
      votes,
      strategy,
      address,
      initData.proposal.msg.payload.acceptableChains || space?.networks
    );

  const { data: choicesData, isFetching: choicesDataFetching } = useLoadChoicesData(
    infResults,
    payload,
    tab
  );

  useEffect(() => {
    if (!userIdentities) return;
    setIdentities(userIdentities);
    setIdentity(userIdentities ? Object.keys(userIdentities)[0] : '');
  }, [userIdentities]);

  useEffect(() => {
    const _score = score ? score.split('.')[0] : '0';
    setTokensLeft(_score ?? 0);
    setInitBalance(_score ?? 0);
  }, [tab, score]);

  useEffect(() => {
    if (!choicesData || loading) return;
    const [unsortedChoices, sortedChoices] = choicesData;
    setMyChoices(sortedChoices);
    setUnsortedChoices(unsortedChoices);
    setChoices(sortedChoices);
  }, [choicesData]);
  useEffect(() => setInfResults(getInfluenceResults(results)), [results, identity, tab]);
  useEffect(() => {
    let identitiesResult = {};
    if (identity) {
      for (const identityData of identities[identity]) {
        identitiesResult[identityData.nftId] = identityData.symbol;
      }
      setSelectedIdentitiesId(identitiesResult);
    }
  }, [identity, tab, infResults]);

  const getInfluenceResults = (results) => {
    return tab === 'vote' ? results?.influenceResult : results?.myInfluenceResult;
  };

  const VotingComponent = strategy?.binary ? Binary : QuadraticVote;

  const isMyInfluenceLoading =
    identitySelectableOptionsFetching ||
    choicesDataFetching ||
    userIdentitiesFetching ||
    myInfluencePropsFetching;

  const isVotesLoading = choicesDataFetching || voteComponentPropsFetching;

  return (
    <>
      <If condition={tab === 'myVote'}>
        <div className={styles.voteData}>
          <MyInfluence
            isLoading={isMyInfluenceLoading}
            identitySelectableOptions={identitySelectableOptions}
            isOpenVote={isOpenVote}
            hasVoted={hasMyVotesVoted}
            items={unsortedChoices}
            setIdentity={setIdentity}
            selectedIdentity={identity}
            secondData={`${Math.floor(initBalance)} ${strategy?.params?.symbol || ''}`}
            {...(selectedIdentitesId && { nftId: Object.keys(selectedIdentitesId)[0] })}
            {...myInfluenceProps}
          />
        </div>
        <div className={classNames(styles.mobilepadding, styles.ballot)}>
          {identities && Object.keys(identities).length > 0 && (
            <MultipleVotingNotification
              ids={hasMyVotesVoted ? null : selectedIdentitesId}
              isLoading={userIdentitiesFetching}
            />
          )}
          <VotingComponent
            isLoading={isVotesLoading}
            isMyVotesTab
            isOpenVote={isOpenVote}
            hasVoted={hasMyVotesVoted}
            choices={myChoices}
            items={unsortedChoices}
            tokensLeft={Math.floor(tokensLeft)}
            setChoices={setMyChoices}
            isVotePermission={!isDraft}
            tokensLoading={tokensLoading}
            tokensAvailable={initBalance}
            {...voteComponentProps}
          />
        </div>
      </If>
      <If condition={tab === 'vote' && choices.length && isProposalVisible}>
        <div className={styles.voteData}>
          <MyInfluence
            isLoading={isMyInfluenceLoading}
            identitySelectableOptions={identitySelectableOptions}
            isOpenVote={isOpenVote}
            items={choices}
            secondData={isProposalVisible && Object.keys(votes).length}
            {...(selectedIdentitesId && { nftId: Object.keys(selectedIdentitesId)[0] })}
            {...myInfluenceProps}
          />
        </div>
        <div className={classNames(styles.mobilepadding, styles.ballot)}>
          <QuadraticVote
            isLoading={isVotesLoading}
            isOpenVote={isOpenVote}
            choices={choices}
            items={choices}
            tokensLeft={Math.floor(tokensLeft)}
            setChoices={setChoices}
            tokensAvailable={initBalance}
            {...voteComponentProps}
          />
        </div>
      </If>
    </>
  );
};

export default memo(CombinedChainsVoteComponent);
