import classNames from 'classnames';
import If from 'components/If';
import MyInfluence from 'components/Spaces/Influence/Blocks/MyInfluence';
import { useGlobalState, useProposalState } from 'globalStateStore';
import { checkIfIdentityOnLists } from 'helpers/proposal';
import {
  useGetIdentitiesDropdownOptions,
  useGetMyInfluenceProps,
  useGetVoteComponentProps,
  useGetVotersNftIdentities,
  useLoadChoicesData,
  useScoreQuery
} from 'helpers/queries';
import dynamic from 'next/dynamic';
import { memo, useEffect, useState } from 'react';
import MultipleVotingNotification from '../MultipleVotingNotification';
import styles from './VoteComponent.module.scss';

const Binary = dynamic(() => import('components/Spaces/Influence/Blocks/Binary'));
const QuadraticVote = dynamic(() => import('components/Spaces/Influence/Blocks/QuadraticVote'));

const NftVoteComponent = ({ strategy, tab }) => {
  const [address] = useGlobalState('address');
  const [currentNetwork] = useGlobalState('currentNetwork');

  const [choices, setChoices] = useProposalState('choices');
  const [initData, setInitData] = useProposalState('initData');
  const [identity, setIdentity] = useProposalState('identity');
  const [identities, setIdentities] = useProposalState('identities');
  const [myChoices, setMyChoices] = useProposalState('myChoices');
  const [unsortedChoices, setUnsortedChoices] = useProposalState('unsortedChoices');
  const [loading] = useProposalState('loading');
  const [totalI] = useProposalState('totalI');

  const [isVotePermission, setIsVotePermission] = useState(false);
  const [tokensLeft, setTokensLeft] = useState(0);
  const [initBalance, setInitBalance] = useState(0);
  const [infResults, setInfResults] = useState([]);
  const {
    id,
    isOpenVote,
    hasMyVotesVoted,
    payload,
    votes,
    isProposalVisible,
    results,
    isDraft,
    hackathonView
  } = initData;

  const { data: score, isLoading: tokensLoading } = useScoreQuery({
    address,
    currentNetwork,
    proposalHash: id,
    strategy,
    identity: strategy.isSum ? Object.values(identities).join(',') : identity
  });

  useEffect(() => {
    const _score = score ? score.split('.')[0] : '0';
    setTokensLeft(_score ?? 0);
    setInitBalance(_score ?? 0);
  }, [tab, identity, score]);

  const { data: voterIds, isFetching: voterIdsFetching } = useGetVotersNftIdentities(
    address,
    votes,
    isOpenVote,
    currentNetwork,
    strategy
  );

  useEffect(() => {
    if (!voterIds) return;
    setIdentities(voterIds);
    setIdentity(voterIds[Object.keys(voterIds)[0]]);
  }, [voterIds]);

  const getInfluenceResults = (results) => {
    if (tab === 'vote') return results?.influenceResult;
    if (identity) {
      const regex = new RegExp(`\\b${identity}\\b`);
      const key =
        results?.myInfluenceResultPerIdentity &&
        Object.keys(results.myInfluenceResultPerIdentity).find((identities) =>
          identities.match(regex)
        );
      return results?.myInfluenceResultPerIdentity && results.myInfluenceResultPerIdentity[key];
    }
    return results?.myInfluenceResult;
  };

  const { data: choicesData, isFetching: choicesDataFetching } = useLoadChoicesData(
    infResults,
    payload,
    tab
  );

  useEffect(() => {
    if (!choicesData || loading) return;
    const [unsortedChoices, sortedChoices] = choicesData;
    setMyChoices(sortedChoices);
    setUnsortedChoices(unsortedChoices);
    setChoices(sortedChoices);
  }, [choicesData]);

  const { data: identitySelectableOptions, isFetching: identitySelectableOptionsFetching } =
    useGetIdentitiesDropdownOptions(identities, strategy, false, styles.option, address);

  useEffect(() => setInfResults(getInfluenceResults(results)), [results, identity, tab]);

  useEffect(() => {
    if (identity && address) {
      const hasMyVotesVoted = Object.values(votes).find((vote) =>
        parseInt(vote.msg.payload.network) === parseInt(currentNetwork) &&
        vote.address === address &&
        Array.isArray(vote.msg.payload.voterId)
          ? vote.msg.payload.voterId.includes(identity)
          : vote.msg.payload.voterId === identity
      );
      setInitData({ ...initData, hasMyVotesVoted: !!hasMyVotesVoted });
    } else {
      setInitData({ ...initData, hasMyVotesVoted: false });
    }
  }, [identity, votes, strategy, currentNetwork, tab, infResults]);

  useEffect(() => {
    const votePermission = !isDraft
      ? checkIfIdentityOnLists(currentNetwork, payload, identity)
      : false;
    setIsVotePermission(!!votePermission);
  }, [identity, address, score, payload.length]);

  const { data: myInfluenceProps, isFetching: myInfluencePropsFetching } = useGetMyInfluenceProps(
    tab,
    isProposalVisible,
    address,
    totalI,
    strategy,
    choices
  );

  const {
    data: voteComponentProps,
    isFetching: voteComponentPropsFetching,
    isSuccess: voteComponentPropsLoaded
  } = useGetVoteComponentProps(address, strategy, setUnsortedChoices, setTokensLeft, tab, choices);

  const VotingComponent = strategy?.binary ? Binary : QuadraticVote;

  const isMyInfluenceLoading =
    identitySelectableOptionsFetching ||
    choicesDataFetching ||
    voterIdsFetching ||
    myInfluencePropsFetching;

  const isVotesLoading = !voteComponentPropsLoaded && voteComponentPropsFetching;

  return (
    <>
      <If condition={tab === 'myVote'}>
        <div className={styles.voteData}>
          <MyInfluence
            hackathonView={hackathonView}
            isLoading={isMyInfluenceLoading}
            identitySelectableOptions={identitySelectableOptions}
            isOpenVote={isOpenVote}
            hasVoted={hasMyVotesVoted}
            items={unsortedChoices}
            setIdentity={setIdentity}
            selectedIdentity={identity}
            secondData={`${Math.floor(initBalance)} ${strategy?.params?.symbol || ''}`}
            {...myInfluenceProps}
          />
        </div>
        <div className={classNames(styles.mobilepadding, styles.ballot)}>
          <If condition={strategy?.isSum && address}>
            <MultipleVotingNotification
              isLoading={voterIdsFetching}
              ids={hasMyVotesVoted ? null : identities}
            />
          </If>
          <VotingComponent
            hackathonView={hackathonView}
            isLoading={isVotesLoading}
            isMyVotesTab
            isOpenVote={isOpenVote}
            hasVoted={hasMyVotesVoted}
            choices={myChoices}
            items={unsortedChoices}
            tokensLeft={Math.floor(tokensLeft)}
            setChoices={setMyChoices}
            isVotePermission={isVotePermission}
            tokensLoading={tokensLoading}
            tokensAvailable={initBalance}
            {...voteComponentProps}
          />
        </div>
      </If>
      <If condition={tab === 'vote' && choices.length && isProposalVisible}>
        <div className={styles.voteData}>
          <MyInfluence
            isLoading={isMyInfluenceLoading}
            identitySelectableOptions={identitySelectableOptions}
            isOpenVote={isOpenVote}
            items={choices}
            secondData={isProposalVisible && Object.keys(votes).length}
            {...myInfluenceProps}
          />
        </div>
        <div className={classNames(styles.mobilepadding, styles.ballot)}>
          <QuadraticVote
            isLoading={isVotesLoading}
            isOpenVote={isOpenVote}
            choices={choices}
            items={choices}
            tokensLeft={Math.floor(tokensLeft)}
            setChoices={setChoices}
            tokensAvailable={initBalance}
            hackathonView={hackathonView}
            {...voteComponentProps}
          />
        </div>
      </If>
    </>
  );
};

export default memo(NftVoteComponent, (prev, next) => {
  return JSON.stringify(prev) === JSON.stringify(next);
});
