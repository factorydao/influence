import { validateMediaUrl } from 'helpers/richMedia';
import dynamic from 'next/dynamic';
import { TwitterTweetEmbed } from 'react-twitter-embed';
import classes from './RichMedia.module.scss';

const Youtube = dynamic(() => import('react-youtube'));
const Vimeo = dynamic(() => import('@u-wave/react-vimeo'));

const getMediaComponent = (type, link, options, setRichMediaReady) => {
  switch (type) {
    case 'youtube':
      return (
        <Youtube
          style={{ width: '100%', position: 'relative', paddingBottom: '56.25%' }}
          videoId={link}
          opts={{
            ...options
          }}
          iframeClassName={classes.video}
          onReady={() => setRichMediaReady(true)}
        />
      );
    case 'twitter':
    case 'x.com':
      return (
        <TwitterTweetEmbed
          tweetId={link}
          onLoad={() => {
            setRichMediaReady(true);
          }}
        />
      );
    case 'vimeo':
      return <Vimeo video={link} width={640} onLoaded={() => setRichMediaReady(true)} />;
    case 'tiktok':
      return (
        <iframe
          title={link}
          src={link}
          allow="fullscreen"
          style={{
            height: '770px',
            width: '325px',
            border: 'none'
          }}></iframe>
      );
    case 'image':
      return (
        // eslint-disable-next-line @next/next/no-img-element
        <img
          alt="media-img"
          src={`https://factorydao.infura-ipfs.io/ipfs/${link}`}
          style={{ height: 'auto', width: 'auto', maxWidth: '100%' }}
        />
      );

    case 'anchor':
      return (
        <iframe
          onLoad={() => setRichMediaReady(true)}
          title="anchor"
          src={link}
          height="102px"
          width="100%"
          style={{ maxWidth: '750px' }}
          frameBorder="0"
          scrolling="no"
        />
      );
    default:
      return <></>;
  }
};

const RichMedia = ({ type, link, setRichMediaReady }) => {
  const isClientSide = typeof window !== 'undefined';
  const opts = {
    height: '390',
    width: '640',
    playerVars: {
      autoplay: 0
    }
  };
  link = validateMediaUrl(type, link);

  return <>{link && isClientSide && getMediaComponent(type, link, opts, setRichMediaReady)}</>;
};

export default RichMedia;
