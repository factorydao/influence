import axios from 'axios';
import WalletButton from 'components/Ui/WalletButton';
import { ethInstance, toChecksumAddress } from 'evm-chain-scripts';
import { useGlobalState } from 'globalStateStore';
import { useEffect, useState } from 'react';
import { useCookies } from 'react-cookie';
import { SiweMessage } from 'siwe';
import styles from './Siwe.module.scss';

const SignInButton = () => {
  const [isLoading, setIsLoading] = useState(true);
  const [address] = useGlobalState('address');
  const [chainId] = useGlobalState('currentNetwork');

  const [cookies, setCookie] = useCookies();

  const signIn = async () => {
    try {
      const response = await axios.get(`/api/siwe/nonce?address=${address}`);

      const { tempToken, nonce } = response.data;

      if (!address || !chainId) return;

      const expDate = new Date();

      expDate.setHours(expDate.getHours() + 24);

      const message = new SiweMessage({
        domain: window.location.host,
        address: toChecksumAddress(address),
        statement: 'Sign in with Ethereum to the app.',
        uri: window.location.origin,
        version: '1',
        chainId,
        nonce: nonce,
        expirationTime: expDate.toISOString()
      });

      const signature = await ethInstance.signPersonalMessage(message.prepareMessage(), address);

      const body = {
        message,
        signature,
        tempToken,
        address: address?.toLowerCase()
      };

      setCookie(`walletAddress`, address);
      setCookie(`session_${address}`, tempToken);

      const verifyRes = await axios.post(`api/siwe/verify`, body);
      const token = verifyRes?.data?.token;

      if (verifyRes.status === 200 && token) {
        setCookie(`session_${address}`, token, {
          expires: expDate
        });
      }
    } catch (error) {
      console.log(error);
      await signIn();
    }
  };

  useEffect(() => {
    (async () => {
      try {
        if (address) {
          setIsLoading(true);

          const session = cookies?.[`session_${address}`];

          if (!session) {
            await signIn();
          }

          setCookie(`walletAddress`, address);

          const res = await axios.get(`/api/siwe/check?address=${address}`);

          if (res.status !== 200) {
            await signIn();
          }
        }
      } catch (_error) {
        console.error(_error);
      } finally {
        setIsLoading(false);
      }
    })();
  }, [address]);

  return <div className={styles.siweButton}>{isLoading ? 'Loading ....' : <WalletButton />}</div>;
};

export default SignInButton;
