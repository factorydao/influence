import classNames from 'classnames';
import LogoLoader from 'components/Ui/LogoLoader';
import { useProposalState } from 'globalStateStore';
import Button from '../../../../Ui/Button/Button.component';
import Progress from '../../../../Ui/Progress/Progress.component';
import styles from './Binary.module.scss';

/**
 *
 * Semantic ballot component.
 * Used for vote and display results after voting.
 *
 * @param {Array} items - choices array
 * @param {Boolean} isOpenVote - is open vote
 * @param {Boolean} isConnected - is wallet connected
 * @param {Number} tokensAvailable - all available tokens, not changed
 * @param {Function} setUnsortedChoices - action to set unsorted choices
 * @returns Binary Vote component
 */
const BinaryVote = ({
  items,
  choices,
  isOpenVote,
  isConnected,
  tokensAvailable,
  hasVoted,
  setUnsortedChoices,
  setChoices,
  isMyVotesTab,
  isVotePermission,
  isLoading
}) => {
  const [, setOpenVoteDialog] = useProposalState('voteDialogOpen');
  const onSetUnsortedChoices = (value) => setUnsortedChoices(value);
  const onSetChoices = (value) => setChoices(value);

  const [proposalData] = useProposalState('initData');
  const hasItems = items && items.length > 0;
  // max for tokens for progress to calculate percentages
  const max =
    !isMyVotesTab || hasVoted || !isOpenVote
      ? items.map((x) => Math.abs(x.progress)).reduce((a, b) => a + Math.abs(b), 0)
      : Math.ceil(Math.sqrt(tokensAvailable));
  /**
   *
   * Checks if proposal is before start
   *
   * @param {Object} proposalData - proposal data
   * @returns true / false
   */
  const isAfterStart = (proposalData) => {
    const now = new Date().getTime();
    return proposalData.proposal.msg.payload.start <= now;
  };

  const onWeightChange = (id, value) => {
    let item = items.find((c) => c.choice.id === id);
    let index = items.findIndex((c) => c.choice.id === id);

    if (item) {
      let editChoice = {
        ...item,
        value: value === '-' ? value : +value,
        isNegative: false
      };

      const choicesCopy = items.map((item) => {
        item.value = 0;
        return item;
      });
      choicesCopy.splice(index, 1, editChoice);

      if (isOpenVote && !hasVoted && isMyVotesTab) {
        onSetUnsortedChoices(choicesCopy);
      } else {
        onSetChoices(choicesCopy);
      }
    }
  };

  const chooseAnswer = (id) => {
    if (!isConnected) return;
    onWeightChange(id, Math.round(tokensAvailable));
    setOpenVoteDialog(true);
  };

  const getVoteInputValue = (item) => {
    if (!isMyVotesTab || !isOpenVote || hasVoted) {
      return { value: item.progress };
    }
    if (isOpenVote && !hasVoted && isConnected && item.value) return { value: item.value };

    return { value: '' };
  };

  const getProgressBgColor = (index) => {
    return index % 2 ? 'bg-progress-influence-even' : 'bg-progress-influence-odd';
  };

  const votesData = isOpenVote && !hasVoted && isMyVotesTab ? items : choices;

  return isLoading ? (
    <LogoLoader classes="logoLoader" />
  ) : (
    <div className={styles.container}>
      {isOpenVote && hasVoted && (
        <div className={styles.success}>
          <strong>VOTE SUBMITTED</strong>
        </div>
      )}

      {!isOpenVote && isAfterStart(proposalData) && !isMyVotesTab && (
        <div className={styles.winner}>
          <div className={styles.label}>Result:</div>
          <div className={styles.result}>
            <div className={styles.text}>{items[0].choice.value}</div>
            {items[0].choice.tag ?? ''}

            <div className={styles.value}>
              {`${items[0].progress} Votes (${parseFloat(
                (max > 0 ? 100 / max : 0) * items[0].progress
              ).toFixed(2)}%)`}
            </div>
          </div>
        </div>
      )}

      {isOpenVote && !hasVoted && isMyVotesTab && (
        <div
          className={classNames(
            styles.progressbar,
            isConnected ? styles.progressConnected : styles.progressNotConnected
          )}>
          <div className={styles.label}>
            {isConnected ? (
              <div>
                {tokensAvailable > 0 ? 'Select your choice' : "You're not eligible to vote"}
              </div>
            ) : (
              <div>Wallet not connected</div>
            )}
          </div>
        </div>
      )}

      <div className={styles.ballot}>
        {hasItems ? (
          votesData.map((item, index) => {
            return (
              <div
                key={`choice${item.choice._id}`}
                className={classNames(styles.item, { [styles.empty]: item.progress === 0 })}>
                <div className={styles.volume} key={`choice-progress${item.choice.id}`}>
                  {(isConnected || !isOpenVote) && (
                    <Progress
                      value={!isMyVotesTab || hasVoted || !isOpenVote ? item.progress : item.value}
                      max={1}
                      isNegative={false}
                      bgColor={getProgressBgColor(index)}
                    />
                  )}
                </div>
                <div className={styles.info}>
                  <div className={styles.data}>
                    <div className={styles.text}>{item.choice.value}</div>
                    {item.choice.tag ? <div className={styles.tag}>{item.choice.tag}</div> : null}
                  </div>
                  <div className={styles.box}>
                    {hasVoted ? (
                      <fieldset
                        className={
                          !isConnected || hasVoted || !isMyVotesTab
                            ? styles.disabled
                            : styles.editable
                        }>
                        <legend
                          className={
                            !isConnected || hasVoted || !isMyVotesTab
                              ? styles.disabled
                              : styles.editable
                          }>
                          $Vote
                        </legend>
                        <input
                          className={styles.editable}
                          disabled
                          type="number"
                          placeholder="0"
                          onWheel={(event) => event.currentTarget.blur()}
                          {...getVoteInputValue(item)}
                        />
                      </fieldset>
                    ) : (
                      <Button
                        className={styles.binary}
                        onClick={() => chooseAnswer(item.choice.id)}
                        disabled={!tokensAvailable || tokensAvailable < 1 || !isVotePermission}>
                        Vote
                      </Button>
                    )}
                  </div>
                </div>
              </div>
            );
          })
        ) : (
          <p className={styles.emptyList}>List is empty</p>
        )}
      </div>
    </div>
  );
};

export default BinaryVote;
