import axios from 'axios';
import If from 'components/If';
import moment from 'moment';
import { explorer, getTimeZone, ipfsUrl, n } from 'helpers/utils';
import Block from 'components/Ui/Block';
import Icon from 'components/Ui/Icon';
import styles from './Detail.module.scss';
import { memo } from 'react';
import Countdown from 'react-countdown';
import CountdownContent from 'components/Ui/CountdownContent';
import LogoLoader from 'components/Ui/LogoLoader';

const dateFormat = 'DD/MM/YYYY HH:mm';

/**
 *
 * Detail block component.
 * Used on the right side in proposal detail
 *
 * @param {Object} payload - data object with start and end date
 * @param {Object} initData - init proposal data
 * @returns Detail block component.
 */
const DetailsBlock = ({ space, initData, currentNetwork, loading }) => {
  const { votes, proposal, isProposalVisible, payload, isOpenVote, hideVotes } = initData;
  const startTimezone = getTimeZone(payload.start);
  const endTimezone = getTimeZone(payload.end);
  const isDifferentTimezones = startTimezone !== endTimezone;

  const generateJSONResult = () => {
    const obj = Object.values(votes).map((x) => {
      return {
        address: x.address,
        authorIpfsHash: x.authorIpfsHash,
        relayerIpfsHash: x.relayerIpfsHash,
        balance: x.balance,
        space: x.msg.space,
        timestamp: x.msg.timestamp,
        proposal: x.msg.payload.proposal,
        voterId: x.msg.payload.voterId,
        choices: x.msg.payload.choice
      };
    });

    var x = window.open();
    x.document.open();
    x.document.write(
      '<html><body><pre>' + JSON.stringify(obj, null, '\t') + '</pre></body></html>'
    );
    x.document.close();
  };

  return (
    <Block title="Details">
      <div className={styles.container}>
        {loading ? (
          <LogoLoader classes="logoLoader" />
        ) : (
          <>
            <div className={styles.item}>
              <span className={styles.label}>IPFS:</span>
              <span className={styles.value}>
                <a
                  href={`${ipfsUrl(proposal.ipfsHash)}`}
                  className={styles.value}
                  target="_blank"
                  rel="noopener noreferrer">
                  {`#${proposal.ipfsHash?.slice(0, 7)} `}
                  <Icon name="external-link" />
                </a>
              </span>
            </div>
            <div className={styles.item}>
              <span className={styles.label}>Strategy:</span>
              <span className={styles.value}>
                <span>{proposal.strategy?.name}</span>
              </span>
            </div>
            <div className={styles.item}>
              <span className={styles.label}>Start Date:</span>
              <span className={styles.value}>
                <span>
                  {moment(payload.start).format(dateFormat) +
                    (isDifferentTimezones ? ` (${startTimezone})` : '')}
                </span>
              </span>
            </div>
            <div className={styles.item}>
              <span className={styles.label}>End Date:</span>
              <span className={styles.value}>
                <span>
                  {moment(payload.end).format(dateFormat) +
                    (isDifferentTimezones ? ` (${endTimezone})` : '')}
                </span>
              </span>
            </div>
            <div className={styles.item}>
              <span className={styles.label}>{isOpenVote ? 'Time to end:' : 'Time to start:'}</span>
              <span className={styles.value}>
                <Countdown
                  date={isOpenVote ? payload.end : payload.start}
                  renderer={CountdownContent}
                />
              </span>
            </div>
            <If condition={proposal.strategy?.key === 'semantic-vote-power'}>
              <div className={styles.item}>
                <span className={styles.label}>Snapshot:</span>
                <a
                  href={explorer(currentNetwork, payload.snapshot?.[currentNetwork], 'block')}
                  target="_blank"
                  className={styles.value}
                  rel="noreferrer">
                  {`${n(payload.snapshot?.[currentNetwork], '0,0')} `}
                  <Icon name="external-link" />
                </a>
              </div>
            </If>
            <If condition={isProposalVisible && !hideVotes}>
              <div className={styles.item}>
                <span className={styles.label}>Results:</span>
                <span className={styles.value}>
                  <button className={styles.results} onClick={generateJSONResult}>
                    JSON <Icon name="external-link" />
                  </button>
                  /
                  <a
                    className={styles.results}
                    href={`${axios.defaults.baseURL}/api/spaces/${space.name}/proposal/${initData.proposal.ipfsHash}/csv`}
                    target="_blank"
                    rel="noreferrer">
                    CSV <Icon name="external-link" />
                  </a>
                </span>
              </div>
            </If>
          </>
        )}
      </div>
    </Block>
  );
};

export default memo(DetailsBlock);
