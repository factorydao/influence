import { ArcElement, Chart as ChartJS, Legend, Tooltip } from 'chart.js';
import classNames from 'classnames';
import If from 'components/If';
import NftImage from 'components/NftImage/NftImage';
import LogoLoader from 'components/Ui/LogoLoader';
import { useGlobalState } from 'globalStateStore';
import { colorsArray } from 'helpers/colorsArray';
import { shortenWalletAddress } from 'helpers/influence';
import { memo, useEffect, useState } from 'react';
import { Pie } from 'react-chartjs-2';
import Dropdown from 'react-dropdown';
import 'react-dropdown/style.css';
import styles from './MyInfluence.module.scss';

ChartJS.register(ArcElement, Tooltip, Legend);
/**
 *
 * My Influence component.
 * Used in left section of the proposal detail
 * Chenged base on voting status.
 *
 * @param {Array} items - choices
 * @param {Object} firstData - first data object - changed base on voting status
 * @param {Object} secondData - second data object - changed base on voting status
 * @param {string} symbol - space symbol
 * @param {Boolean} hasVoted - connected wallet voted
 * @param {Boolean} isOpenVote - is open vote
 * @param {Boolean} isMyVotesTab - flag inform that there is MY VOTES tab
 * @returns Component with detail of the voting
 */

const MyInfluence = ({
  symbol,
  items,
  firstData,
  secondData,
  thirdData,
  hasVoted = false,
  isOpenVote = false,
  isMyVotesTab = false,
  nftAddresses,
  hideBalanceAndChart = false,
  selectedIdentity,
  setIdentity,
  identitySelectableOptions,
  isOs1155,
  isCombinedChains,
  nftId,
  isSum = false,
  isLoading,
  hackathonView = false
}) => {
  const [currentNetwork] = useGlobalState('currentNetwork');
  const [pieChartBgColor, setPieChartBgColor] = useState([]);

  // only for combined-chains strategy - find network for nft
  const getNetworkForNft = () => {
    for (const key in nftAddresses) {
      const exist = nftAddresses[key].find(
        (contractAddress) => contractAddress === selectedIdentity
      );
      if (!exist) continue;
      return key;
    }
  };

  const labRes = items.map((choiceItem) => {
    const { tag, value } = choiceItem.choice;
    return tag?.length ? `#${tag.toUpperCase()}` : value;
  });

  const generateRandomColors = (items) => {
    let colors = [];
    for (let index = 0; index < items.length; index++) {
      colors.push(colorsArray[index]);
    }
    return colors;
  };

  useEffect(() => {
    setPieChartBgColor(generateRandomColors(items));
  }, [items]);

  const borderWidth = new Array(labRes.length).fill(0);

  const state = {
    labels: labRes,
    datasets: [
      {
        backgroundColor: pieChartBgColor,
        data: items.map((choice) => {
          return !hasVoted && isOpenVote && isMyVotesTab
            ? Math.abs(choice.value)
            : Math.abs(choice.progress);
        }),
        borderWidth
      }
    ]
  };

  const onSelectedChange = (identity) => {
    if (identity.value !== selectedIdentity) setIdentity(identity.value);
  };

  const getNftImageContractAddress = () => {
    if (!nftAddresses) {
      return '';
    }
    if (isCombinedChains) {
      return selectedIdentity;
    }
    return nftAddresses[currentNetwork];
  };

  return (
    <div className={styles.container}>
      {!hackathonView ? (
        <div className={styles.header}>
          <div className={styles.label}>{isMyVotesTab ? `My influence` : `Vote stats`}</div>
        </div>
      ) : null}
      {isLoading ? (
        <LogoLoader classes="logoLoader" />
      ) : (
        <>
          <If condition={!hackathonView}>
            {isMyVotesTab ? (
              <div key={`item-myVote-address`} className={styles.item}>
                <div className={styles.label}>address</div>
                <div className={styles.value}>{shortenWalletAddress(firstData)}</div>
              </div>
            ) : (
              <div key="item-firstData" className={styles.item}>
                <div className={styles.label}>{`Total ${symbol}`}</div>
                <div className={styles.value}>{firstData}</div>
              </div>
            )}
          </If>
          {identitySelectableOptions?.length > 0 && isMyVotesTab && (
            <div
              key="item-firstData"
              className={classNames(styles.item, hackathonView ? styles.dividerNone : '')}>
              <NftImage
                nftId={isCombinedChains ? nftId : selectedIdentity}
                contractAddress={getNftImageContractAddress()}
                center={true}
                isOs1155={isOs1155}
                {...(isCombinedChains && { chainId: getNetworkForNft() })}
              />
              <If condition={!isSum}>
                <>
                  <div className={styles.label} style={{ marginTop: '15px' }}>
                    Select an identity
                  </div>
                  <div className={styles.value}>
                    <Dropdown
                      className={styles.dropdown}
                      controlClassName={styles.control}
                      placeholderClassName={styles.placeholder}
                      menuClassName={styles.menu}
                      options={identitySelectableOptions}
                      value={selectedIdentity}
                      onChange={onSelectedChange}
                      arrowClosed={<span className={styles.arrowClosed} />}
                      arrowOpen={<span className={styles.arrowOpen} />}
                    />
                  </div>
                </>
              </If>
              {/* <>
              <div className={styles.label} style={{ marginTop: '15px' }}>
                Voting as
              </div>
              <div className={styles.value}>
                {identitySelectableOptions.map((identity) => identity.label).join(', ')}
              </div>
            </> */}
            </div>
          )}
          {((isOpenVote && !hideBalanceAndChart) || !isMyVotesTab) && !hackathonView ? (
            <div key="item-secondData" className={styles.item}>
              <div className={styles.label}>{isMyVotesTab ? `balance` : `no. of voters`}</div>
              <div className={styles.value}>{secondData ?? '-'}</div>
            </div>
          ) : null}
          {isOpenVote && !hideBalanceAndChart && isMyVotesTab && thirdData && (
            <div key="item-thirdData" className={styles.item}>
              <div className={styles.label}>{thirdData.label}</div>
              <div className={styles.value}>{`${thirdData.userPosition ?? '0'} out of ${
                thirdData.maxLevel ?? '0'
              }`}</div>
            </div>
          )}
          {(!hideBalanceAndChart || !isMyVotesTab) && (
            <div key="item-allocation" className={styles.item}>
              <div className={styles.label}>Allocation</div>
              <div className={classNames(styles.value, styles.chartContainer)}>
                <Pie
                  data={state}
                  height={300}
                  options={{
                    plugins: {
                      title: {
                        display: false
                      },
                      legend: {
                        display: false
                      },
                      tooltip: {
                        titleFont: { size: 18, style: 'normal' },
                        titleColor: '#dfe8e8',
                        backgroundColor: 'rgba(14, 39, 59, 0.65)',
                        bodyfont: { size: 18 },
                        cornerRadius: 0,
                        caretSize: 0,
                        displayColors: false,
                        callbacks: {
                          title(tooltipItems) {
                            return tooltipItems.length && tooltipItems[0].label;
                          },
                          label(tooltipItem) {
                            if (!Array.isArray(tooltipItem)) {
                              const {
                                parsed,
                                dataset: { data }
                              } = tooltipItem;
                              const total = data.reduce(function (previousValue, currentValue) {
                                return previousValue + currentValue;
                              });
                              const currentValue = parsed;
                              const percentage = ((currentValue / total) * 100).toFixed(0);
                              return total > 0
                                ? currentValue + ` votes | ` + percentage + '%'
                                : null;
                            }
                            return null;
                          }
                        }
                      }
                    }
                  }}
                />
              </div>
            </div>
          )}
        </>
      )}
    </div>
  );
};

export default memo(MyInfluence);
