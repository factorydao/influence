import AvatarImage from 'components/AvatarImage/AvatarImage';
import { shortenWalletAddress } from 'helpers/influence';
import { explorer, shorten } from 'helpers/utils';
import moment from 'moment';
import { useEffect, useState } from 'react';
import styles from './Opinions.module.scss';

/**
 *
 * Generates the visible opinions array
 *
 * @param {Array} opinions - array of opinions
 * @param {Number} pageNumber - page number to paggination
 * @param {Number} itemsPerPage - number of items per page
 * @returns opinions array
 */
const generateVisibleOpinions = (opinions, pageNumber, itemsPerPage) => {
  return opinions.slice(0, pageNumber * itemsPerPage);
};

/**
 *
 * Influence opinions component.
 * Displays the list of opinions with avatars.
 *
 * @param {Object} space - current space data
 * @param {Array} items - array of opinions
 * @param {Object} strategy - strategy data
 * @returns list of opinions
 */
const InfluenceOpinions = ({ items, strategy }) => {
  const [visibleOpinions, setVisibleOpinions] = useState(items);
  const [pageNumber, setPageNumber] = useState(1);
  const itemsPerPage = 12;

  useEffect(() => {
    if (items.length > 0) {
      setVisibleOpinions(generateVisibleOpinions(items, pageNumber, itemsPerPage));
    }
  }, [items, pageNumber]);

  const handlePageChage = () => {
    setPageNumber(pageNumber + 1);
  };

  const getExplorerLinkLabel = ({ key, params }, { address }, voterIds) => {
    if (key === 'os-1155') {
      return shortenWalletAddress(address);
    }
    if (params?.type !== 'nft') {
      return shorten(address);
    }
    return `Voter ID ${voterIds}`;
  };

  return (
    <div className={styles.container}>
      {items.length > 0 &&
        visibleOpinions.map((item, index) => {
          const voterId = Array.isArray(item.voterId) ? item.voterId[0] : item.voterId;
          const voterIds = Array.isArray(item.voterId) ? item.voterId.join(', ') : item.voterId;
          return (
            <div className={styles.item} key={`opinion-item-${index}-${voterId}`}>
              <AvatarImage
                nftAddress={
                  strategy?.params.nftAddresses ? strategy.params.nftAddresses[item.networkId] : ''
                }
                style={{
                  maxWidth: '50px',
                  maxHeight: '50px'
                }}
                voterAddress={item.address}
                voter={voterId}
                isOs1155={strategy.key === 'os-1155'}
                voterNetwork={item.networkId}
              />
              <div className={styles.opinion}>
                <div className={styles.header}>
                  <span>
                    <a
                      href={explorer(item.networkId || '1', item.address)}
                      target="_blank"
                      rel="noopener noreferrer">
                      <button className={styles.profile}>
                        <span>{getExplorerLinkLabel(strategy, item, voterIds)}</span>
                      </button>
                    </a>
                    {` / ${moment(item.time).fromNow()}`}
                  </span>
                </div>
                <div className={styles.value}>
                  <div className={styles.tag}>{item.tag ? `#${item.tag}` : item.choiceValue}</div>
                  <div className={styles.coin}>{`${
                    strategy.params?.type !== 'nft' && strategy.params?.type !== 'sim-multi'
                      ? `${Math.pow(item.value, 2)} ${strategy.params.symbol}`
                      : `${item.value} Votes`
                  }`}</div>
                </div>
                <div className={styles.text}>{item.opinion}</div>
              </div>
            </div>
          );
        })}
      {items.length === 0 && (
        <p className={styles.empty}>There aren&apos;t any opinions here yet!</p>
      )}
      {items.length > 12 && items.length !== visibleOpinions.length && (
        <button className={styles.more} onClick={handlePageChage}>
          load more opinions
        </button>
      )}
    </div>
  );
};

export default InfluenceOpinions;
