import placeholder from 'assets/placeholder.png';
import classNames from 'classnames';
import If from 'components/If';
import ReadMore from 'components/Modal/ReadMore';
import Button from 'components/Ui/Button';
import LogoLoader from 'components/Ui/LogoLoader';
import Progress from 'components/Ui/Progress';
import SearchBox from 'components/Ui/SearchBox';
import { toBN } from 'evm-chain-scripts';
import Fuse from 'fuse.js';
import { useProposalState } from 'globalStateStore';
import { ipfsUrl } from 'helpers/utils';
import Image from 'next/image';
import numeral from 'numeral';
import { memo, useEffect, useState } from 'react';
import ReactTooltip from 'react-tooltip';
import styles from './QuadraticVote.module.scss';

const format = '-0';
const minVotesToSearch = 15;

const STATUSES = {
  NO_VOTES: 'no_votes',
  TIE: 'tie',
  WINNER: 'winner',
  VOTED: 'voted',
  READY: 'ready_to_vote'
};

/**
 *
 * Semantic ballot component.
 * Used for vote and display results after voting.
 *
 * @param {Array} items - choices array
 * @param {string} symbol - space symbol
 * @param {Boolean} isOpenVote - is open vote
 * @param {Boolean} isConnected - is wallet connected
 * @param {Number} tokensLeft - remaining tokens for voting
 * @param {Number} tokensAvailable - all available tokens, not changed
 * @param {Boolean} hasVoted - user vote or not
 * @param {Boolean} negativeVote - enable/disable negative vote
 * @param {Function} onSetChoices - action to set choices
 * @param {Function} setUnsortedChoices - action to set unsorted choices
 * @param {Function} onSetTokensLeft - action to set tokens left
 * @returns Quadratic Vote Ballot
 */
const QuadraticVote = ({
  items,
  choices,
  symbol,
  isOpenVote,
  isConnected,
  tokensLeft,
  tokensAvailable,
  hasVoted = false,
  negativeVote = true,
  setChoices,
  setUnsortedChoices,
  setTokensLeft,
  isMyVotesTab,
  isVotePermission,
  tokensLoading = true,
  isLoading,
  hackathonView = false
}) => {
  const [, setOpenVoteDialog] = useProposalState('voteDialogOpen');
  const [proposalData] = useProposalState('initData');
  const [status, setStatus] = useState();
  const onSetChoices = (value) => setChoices(value);
  const onSetUnsortedChoices = (value) => setUnsortedChoices(value);
  const onSetTokensLeft = (score) => setTokensLeft(score);
  const [descriptionPopupVisible, setDescriptionPopupVisible] = useState(false);
  const [descriptionPopupData, setDescriptionPopupData] = useState({});
  const [searchValue, setSearchValue] = useState('');

  const openDescriptionPopupAndHydrate = (choice) => {
    if (!choice.description && !choice.richMedia) return;
    setDescriptionPopupData(choice);
    setDescriptionPopupVisible(true);
  };

  const closeDescriptionPopupAndClearState = () => {
    setDescriptionPopupVisible(false);
    setDescriptionPopupData({});
  };
  // action after change value for choice
  const onWeightChange = (id, value) => {
    let item = items.find((c) => c.choice._id === id);
    let index = items.findIndex((c) => c.choice._id === id);

    if (item) {
      let editChoice = {
        ...item,
        value: value === '-' ? value : +value,
        isNegative: negativeVote ? value < 0 : false
      };

      const choicesCopy = [...items];
      choicesCopy.splice(index, 1);
      choicesCopy.splice(index, 0, editChoice);
      if (isOpenVote && !hasVoted && isMyVotesTab) {
        onSetUnsortedChoices(choicesCopy);
      } else {
        onSetChoices(choicesCopy);
      }

      let calc = toBN(0);

      for (const choice of choicesCopy) {
        const value = choice.value === '-' ? toBN(0) : toBN(choice.value || 0);
        calc = calc.add(value.mul(value));
      }

      const tokensLeft = toBN(tokensAvailable).sub(calc);
      const toksLeft = tokensLeft.toString();
      onSetTokensLeft(toksLeft);
    }
  };

  const onChange = (id, evt) => {
    const { value } = evt.target;
    evt.target.placeholder = value ? '0' : '';
    const weight = formatValue(value);
    onWeightChange(id, weight);
  };

  const hasItems = items && items.length > 0;
  const proggressLeftBgColor = isConnected ? 'bg-progress-left' : 'bg-strawberry';

  // max for tokens for progress to calculate percentages
  const max =
    !isMyVotesTab || hasVoted || !isOpenVote
      ? items.reduce((sum, item) => sum + Math.abs(item.progress), 0)
      : items.reduce((sum, item) => sum + Math.abs(item.value), 0) || 1;

  /**
   *
   * Formats input value
   *
   * @param {*} value - value
   * @returns formatted value
   */
  const formatValue = (value) => {
    const formattedValue = value === '-' ? value : +numeral(value).format(format);
    return formattedValue;
  };

  const handleKeyPress = (event) => {
    if (event.charCode === 45) event.preventDefault();
  };

  const getVoteInputValue = (item) => {
    if (!isMyVotesTab || !isOpenVote || hasVoted) {
      return { value: item.progress };
    }
    if (isOpenVote && !hasVoted && isConnected && item.value) return { value: item.value };

    return { value: '' };
  };

  const getProgressBgColor = (index) => {
    return index % 2 ? 'bg-progress-influence-even' : 'bg-progress-influence-odd';
  };

  /**
   *
   * Checks if proposal is before start
   *
   * @param {Object} proposalData - proposal data
   * @returns true / false
   */
  const isAfterStart = (proposalData) => {
    const now = new Date().getTime();
    return proposalData.proposal.msg?.payload.start <= now;
  };
  const getStatus = () => {
    // finished statuses
    if (!isOpenVote && isAfterStart(proposalData) && !isMyVotesTab) {
      if (!max) {
        return setStatus(STATUSES.NO_VOTES);
      } else if (Math.abs(items[0].progress) === Math.abs(items[1].progress)) {
        return setStatus(STATUSES.TIE);
      }
      return setStatus(STATUSES.WINNER);
    }
    // vote open statuses
    if (isOpenVote && hasVoted) return setStatus(STATUSES.VOTED);
    if (isOpenVote && !hasVoted && isMyVotesTab) return setStatus(STATUSES.READY);
  };
  const tiedValues =
    status === STATUSES.TIE
      ? items.filter((i) => Math.abs(i.progress) === Math.abs(items[0].progress))
      : null;

  useEffect(() => {
    getStatus();
  }, [items, isOpenVote, hasVoted, isMyVotesTab]);

  const votes = isOpenVote && !hasVoted && isMyVotesTab ? items : choices;

  const fuzzySearch = new Fuse(votes, {
    keys: ['choice.value']
  });
  const searchedVotesData = fuzzySearch.search(searchValue);

  const votesData = searchValue.length ? searchedVotesData.map((votes) => votes.item) : votes;

  const onChangeSearchValue = (value) => {
    setSearchValue(value);
  };

  return isLoading ? (
    <LogoLoader classes="logoLoader" />
  ) : (
    <div className={styles.container}>
      <If condition={status === STATUSES.VOTED}>
        <div className={styles.success}>
          <strong>VOTE SUBMITTED</strong>
        </div>
      </If>
      <If condition={descriptionPopupData}>
        <ReadMore
          isOpen={descriptionPopupVisible}
          data={descriptionPopupData}
          onClose={closeDescriptionPopupAndClearState}
          type={proposalData.hackathonView ? 'hackathonView' : 'description'}
        />
      </If>
      <If condition={status === STATUSES.NO_VOTES}>
        <div className={styles.emptyResultInfo}>
          <p className={styles.text}>SORRY, NOBODY VOTED &nbsp; ¯\_(ツ)_/¯</p>
        </div>
      </If>
      <If condition={status === STATUSES.TIE}>
        <div className={styles.winner}>
          <div className={styles.label}>{tiedValues?.length} way tie:</div>
          <div className={styles.result}>
            <div className={styles.text}>
              {tiedValues?.map((winner) => `${winner.choice.value} `)}
            </div>
            <div className={styles.tag}>
              {tiedValues?.map((winner) => `${winner.choice.tag ?? ''} `)}
            </div>
            <div className={styles.value}>
              {`${items?.[0]?.progress} Votes (${parseFloat(
                (max > 0 ? 100 / max : 0) * items?.[0]?.progress
              ).toFixed(2)}%)`}
            </div>
          </div>
        </div>
      </If>

      <If condition={status === STATUSES.WINNER}>
        <div className={styles.winner}>
          <div className={styles.label}>Most Important Issue:</div>
          <div className={styles.result}>
            <div className={styles.text}>{items?.[0]?.choice.value}</div>
            <div className={styles.tag}>{items?.[0]?.choice.tag}</div>

            <div className={styles.value}>
              {`${items?.[0]?.progress} Votes (${parseFloat(
                (max > 0 ? 100 / max : 0) * items?.[0]?.progress
              ).toFixed(2)}%)`}
            </div>
          </div>
        </div>
      </If>
      <If condition={status === STATUSES.READY}>
        <div
          className={classNames(
            styles.progressbar,
            isConnected ? styles.progressConnected : styles.progressNotConnected
          )}>
          <div className={styles.label}>
            {isConnected ? (
              <div>
                {tokensLoading ? (
                  'Loading...'
                ) : (
                  <>
                    {tokensLeft} {symbol} LEFT
                  </>
                )}
              </div>
            ) : (
              <div>Wallet not connected</div>
            )}
          </div>
          <Progress
            value={tokensLeft}
            max={tokensAvailable}
            bgColor={proggressLeftBgColor}
            isTokensLeft={tokensLeft < 0}
          />
        </div>
      </If>
      <If condition={votes.length > minVotesToSearch}>
        <div className={styles.search}>
          <SearchBox searchValue={searchValue} onChangeSearchValue={onChangeSearchValue} />
        </div>
      </If>
      <div className={styles.ballot}>
        {hasItems ? (
          <>
            {votesData.map((item, index) => {
              const formattedValue = item.value === '-' ? 0 : item.value;
              const isVotePopupDisabled = !item.choice.description && !item.choice.richMedia;

              return (
                <div
                  key={`choice${item.choice._id ?? index}`} //DONE:  FIX same key, react issue
                  className={classNames(
                    styles.item,
                    { [styles.empty]: item.progress === 0 },
                    !isVotePopupDisabled && styles.stripDescription
                  )}>
                  <div className={styles.volume} key={`choice-progress${item.choice._id ?? index}`}>
                    <Progress
                      value={
                        !isMyVotesTab || hasVoted || !isOpenVote ? item.progress : formattedValue
                      }
                      max={max}
                      isNegative={negativeVote && (item.progress < 0 || item.value < 0)}
                      bgColor={getProgressBgColor(index)}
                    />
                  </div>
                  <div className={styles.info}>
                    <ReactTooltip place="top" type="info" effect="solid" />

                    <button
                      data-tip-disable={isVotePopupDisabled}
                      data-tip="click for details"
                      className={classNames(styles.data, !isVotePopupDisabled && styles.clickable)}
                      onClick={() => openDescriptionPopupAndHydrate(item.choice)}>
                      {hackathonView ? (
                        <Image
                          src={
                            item.choice.imageLink?.length
                              ? ipfsUrl(item.choice.imageLink)
                              : placeholder
                          }
                          width={52}
                          height={52}
                          alt="choice image"
                          className={styles.choiceImage}
                        />
                      ) : null}
                      <div className={styles.text}>{item.choice.value}</div>
                      {item.choice.tag ? <div className={styles.tag}>{item.choice.tag}</div> : null}
                    </button>

                    <div className={styles.box}>
                      <fieldset
                        className={
                          !isConnected || hasVoted || !isMyVotesTab
                            ? styles.disabled
                            : styles.editable
                        }>
                        <legend
                          className={
                            !isConnected || hasVoted || !isMyVotesTab
                              ? styles.disabled
                              : styles.editable
                          }>
                          $Vote
                        </legend>
                        <input
                          className={styles.editable}
                          disabled={
                            hasVoted ||
                            !isConnected ||
                            !isOpenVote ||
                            !isMyVotesTab ||
                            tokensAvailable === 0
                          }
                          type="number"
                          placeholder="0"
                          {...(!negativeVote && { min: 0, onKeyPress: handleKeyPress })}
                          onChange={(evt) => onChange(item.choice._id, evt)}
                          onFocus={(e) => (e.target.placeholder = '')}
                          onBlur={(e) => (e.target.placeholder = '0')}
                          onWheel={(event) => event.currentTarget.blur()}
                          {...getVoteInputValue(item)}
                        />
                      </fieldset>
                    </div>
                  </div>
                </div>
              );
            })}
          </>
        ) : (
          <p className={styles.emptyList}>List is empty</p>
        )}
      </div>

      <div className={styles.footer}>
        <If condition={status === STATUSES.READY}>
          <Button
            disabled={
              tokensLeft < 0 ||
              !isConnected ||
              tokensLeft == Math.floor(tokensAvailable) ||
              !isVotePermission
            }
            onClick={() => setOpenVoteDialog(true)}
            className={styles.votebutton}>
            Vote
          </Button>
        </If>
        <If condition={isOpenVote && tokensLeft < 0}>
          <div className={styles.danger}>
            Over allocated, please adjust your votes to get your $I left as close to 0 as possible
          </div>
        </If>
      </div>
    </div>
  );
};

export default memo(QuadraticVote);
