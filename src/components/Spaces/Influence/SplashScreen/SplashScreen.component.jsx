import LogoLoader from 'components/Ui/LogoLoader';
import main from 'images/spaces/influence/main_light.svg';
import Image from 'next/image';
import styles from './SplashScreen.module.scss';
/**
 *
 * Splash screen component
 * Show only when user visit site
 *
 * @returns Splash screen
 */
const SplashScreen = () => {
  return (
    <div className={styles['splash-screen']}>
      <Image alt="influence-logo" src={main} className={styles['main-logo']} />
      <LogoLoader classes={styles.loader} />
    </div>
  );
};
export default SplashScreen;
