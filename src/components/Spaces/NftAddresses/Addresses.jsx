import Button from 'components/Ui/Button';
import Icon from 'components/Ui/Icon';
import uniqueId from 'lodash/uniqueId';
import styles from './Addresses.module.scss';

const Addresses = ({ name, chainValues, setFieldValue, errors, isNewStrategy, buttonLabel }) => {
  const getChainItem = (itemId) => {
    const chain = chainValues.find((chain) => chain.itemId === itemId);
    const index = chainValues.indexOf(chain);
    return { chain, index };
  };

  const editChainItem = (editedChain, index) => {
    const newItems = [...chainValues];
    newItems.splice(index, 1);
    newItems.splice(index, 0, editedChain);
    saveParam(newItems);
    return newItems;
  };
  const saveParam = (items) => {
    setFieldValue(name, [...items]);
  };

  const handleAddChain = () => {
    const items = chainValues.concat({
      itemId: parseInt(uniqueId()),
      chainId: '',
      address: '',
      minNftAmount: '',
      errors: {}
    });

    saveParam(items);
  };

  const handleUpdateChain = (itemId, event) => {
    const { chain, index } = getChainItem(itemId);
    const name = event.target.name;
    const value = event.target.value;

    let editedChainItems = [];
    if (chain) {
      let editedChain = {
        ...chain,
        [name]: value
      };

      editedChainItems = editChainItem(editedChain, index);
    }

    saveParam(editedChainItems);
  };

  const handleDeleteChain = (itemId) => {
    const { index } = getChainItem(itemId);

    if (index >= 0) {
      const newItems = [...chainValues];
      newItems.splice(index, 1);

      saveParam(newItems);
    }
  };
  return (
    <>
      <div className={styles.container}>
        <div className={styles.right}>
          <div className={styles.list}>
            <div className={styles.chainRow}>
              <div className={styles.item}>Chain ID</div>
              <div className={styles.item}>Contract Address</div>
              {isNewStrategy ? null : <div className={styles.item}>Min NFT Amount</div>}
              <div className={styles.item}>Action</div>
            </div>

            {chainValues
              ? chainValues.map(({ itemId, address, chainId, minNftAmount }, index) => {
                  return (
                    <div key={index}>
                      <div className={styles.chainRow}>
                        <div className={styles.item}>
                          <input
                            placeholder="Chain id"
                            value={chainId}
                            name="chainId"
                            type="text"
                            className="input"
                            onChange={(event) => handleUpdateChain(itemId, event)}
                          />
                          {errors?.[index]?.chainId && (
                            <p style={{ color: 'red', fontSize: 16 }}>{errors?.[index]?.chainId}</p>
                          )}
                        </div>
                        <div className={styles.item}>
                          <input
                            placeholder="Contract Address"
                            value={address}
                            type="text"
                            name="address"
                            className="input"
                            onChange={(event) => handleUpdateChain(itemId, event)}
                          />
                          {errors?.[index]?.address && (
                            <p style={{ color: 'red', fontSize: 16 }}>{errors?.[index]?.address}</p>
                          )}
                        </div>
                        {isNewStrategy ? null : (
                          <div className={styles.item}>
                            <input
                              placeholder="Min NFT Amount"
                              value={minNftAmount}
                              type="text"
                              name="minNftAmount"
                              className="input"
                              onChange={(event) => handleUpdateChain(itemId, event)}
                            />
                            {errors?.[index]?.minNftAmount && (
                              <p style={{ color: 'red', fontSize: 16 }}>
                                {errors?.[index]?.minNftAmount}
                              </p>
                            )}
                          </div>
                        )}

                        <Icon
                          className={chainValues.length <= 1 ? styles.disabledIcon : styles.icon}
                          name="close"
                          size="22"
                          onClick={chainValues.length <= 1 ? null : () => handleDeleteChain(itemId)}
                        />
                      </div>
                    </div>
                  );
                })
              : null}

            <Button className={styles.button} onClick={handleAddChain}>
              {buttonLabel}
            </Button>
          </div>
        </div>
      </div>
    </>
  );
};

export default Addresses;
