import classNames from 'classnames';
import Icon from 'components/Ui/Icon';
import { Field, FieldArray } from 'formik';
import styles from './MetadataParams.module.scss';

export const METADATA_TYPES = {
  RANGE: 'range',
  SINGLE_VALUE: 'singleValue',
  BOOLEAN: 'boolean'
};

const metadataTypes = [
  { value: METADATA_TYPES.SINGLE_VALUE, label: 'Single Value' },
  { value: METADATA_TYPES.BOOLEAN, label: 'Boolean' },
  { value: METADATA_TYPES.RANGE, label: 'Range' }
];

const MetadataParams = ({ metadata, errors }) => {
  return (
    <div className={styles.container}>
      <FieldArray name="metadata">
        {({ remove, push }) => (
          <div>
            {metadata.length > 0 &&
              metadata.map((meta, index) => (
                <div className={styles.row} key={index}>
                  <div className={styles.col}>
                    <div className={styles.label}>Key</div>
                    <Field
                      name={`metadata.${index}.key`}
                      placeholder="Key"
                      type="text"
                      className={styles.input}
                    />
                    {errors?.[index]?.key && (
                      <p style={{ color: 'red', fontSize: 16 }}>{errors?.[index]?.key}</p>
                    )}
                  </div>
                  <div className={styles.col}>
                    <div className={styles.label}>Type</div>
                    <Field
                      component="select"
                      name={`metadata.${index}.type`}
                      placeholder="Metadata type">
                      {metadataTypes.map((metaType, index) => {
                        return (
                          <option key={index} value={metaType.value} className={styles.option}>
                            {metaType.label}
                          </option>
                        );
                      })}
                    </Field>
                    {errors?.[index]?.type && (
                      <p style={{ color: 'red', fontSize: 16 }}>{errors?.[index]?.type}</p>
                    )}
                  </div>

                  {meta.type === METADATA_TYPES.BOOLEAN ? (
                    <div className={styles.col}>
                      <div className={styles.label}>Bool value</div>
                      <div className={classNames(styles.toggle, styles.isBoolToggle)}>
                        <Field
                          name={`metadata.${index}.boolValue`}
                          type="checkbox"
                          className={styles.switch}
                        />
                        <span className={styles.slider}></span>
                      </div>
                      {errors?.[index]?.boolValue && (
                        <p style={{ color: 'red', fontSize: 16 }}>{errors?.[index]?.boolValue}</p>
                      )}
                    </div>
                  ) : null}

                  {meta.type === METADATA_TYPES.SINGLE_VALUE ? (
                    <div className={styles.col}>
                      <div className={styles.label}>Single value</div>
                      <Field
                        name={`metadata.${index}.value`}
                        placeholder="Value"
                        type="text"
                        className={styles.input}
                      />
                      {errors?.[index]?.value && (
                        <p style={{ color: 'red', fontSize: 16 }}>{errors?.[index]?.value}</p>
                      )}
                    </div>
                  ) : null}

                  {meta.type === METADATA_TYPES.RANGE ? (
                    <>
                      <div className={styles.col}>
                        <div className={styles.label}>Min value</div>
                        <Field
                          name={`metadata.${index}.min`}
                          placeholder="min"
                          type="number"
                          className={styles.input}
                        />
                        {errors?.[index]?.min && (
                          <p style={{ color: 'red', fontSize: 16 }}>{errors?.[index]?.min}</p>
                        )}
                      </div>
                      <div className={styles.col}>
                        <div className={styles.label}>Max value</div>
                        <Field
                          placeholder="max"
                          type="number"
                          className={styles.input}
                          name={`metadata.${index}.max`}
                        />
                        {errors?.[index]?.max && (
                          <p style={{ color: 'red', fontSize: 16 }}>{errors?.[index]?.max}</p>
                        )}
                      </div>
                    </>
                  ) : null}

                  <div className={classNames(styles.col, styles.colPosition)}>
                    <Icon
                      name="close"
                      size="22"
                      onClick={() => {
                        remove(index);
                      }}
                      className={styles.closeIcon}
                    />
                  </div>
                </div>
              ))}
            <button
              type="button"
              className={classNames(styles.button, styles.paramButton)}
              onClick={() =>
                push({
                  key: '',
                  value: '',
                  type: METADATA_TYPES.SINGLE_VALUE,
                  boolValue: false,
                  min: '',
                  max: ''
                })
              }>
              Add Param
            </button>
          </div>
        )}
      </FieldArray>
    </div>
  );
};

export default MetadataParams;
