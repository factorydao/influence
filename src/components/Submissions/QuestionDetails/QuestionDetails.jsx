import classNames from 'classnames';
import AddSubmissionForm from 'components/Modal/Submissions';
import Button from 'components/Ui/Button';
import Markdown from 'components/Ui/Markdown';
import State from 'components/Ui/State';
import { useGlobalState } from 'globalStateStore';
import { useState } from 'react';
import styles from './QuestionDetails.module.scss';

const QuestionDetails = ({
  data: {
    questionHash,
    tokenHolder,
    idHolder,
    allowRichMedia,
    maxLength,
    type,
    startDate,
    endDate,
    upDownEndDate,
    title,
    body,
    isOpenTask
  },
  isEligible,
  identities,
  onReload
}) => {
  const [address] = useGlobalState('address');
  const [walletRef] = useGlobalState('walletRef');
  const isNotWalletConnected = address.length <= 0;
  const [isAddSubmissionModalOpen, setIsAddSubmissionModalOpen] = useState(false);

  const onAddSubmissionClick = () => {
    setIsAddSubmissionModalOpen(true);
  };

  const onWalletConnectClick = () => {
    walletRef?.current?.firstChild.click();
  };

  const EligibilityButton = () => {
    if (isNotWalletConnected) {
      return (
        <Button className={styles.button} onClick={onWalletConnectClick}>
          connect wallet to contribute
        </Button>
      );
    }
    if (isEligible) {
      return (
        <Button
          className={classNames(styles.button, styles.connected)}
          onClick={onAddSubmissionClick}>
          create a submission
        </Button>
      );
    }
    return <Button className={styles.button}>You&apos;re not eligible</Button>;
  };

  return (
    <div className={styles.description}>
      <AddSubmissionForm
        isOpen={isAddSubmissionModalOpen}
        onClose={() => setIsAddSubmissionModalOpen(false)}
        reloadData={onReload}
        questionHash={questionHash}
        identities={identities}
        tokenHolder={tokenHolder}
        idHolder={idHolder}
        allowRichMedia={allowRichMedia}
        maxSubmLength={maxLength}
        type={type}
      />
      <State
        isProposal={false}
        startDate={startDate}
        endDate={endDate}
        upDownEndDate={upDownEndDate}
        textVisible
      />
      <div className={styles.title}>{title}</div>
      <Markdown body={body} className={styles.markdown} />

      {isOpenTask ? <EligibilityButton /> : null}
    </div>
  );
};

export default QuestionDetails;
