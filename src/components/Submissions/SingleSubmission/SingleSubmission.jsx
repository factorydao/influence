import axios from 'axios';
import AvatarImage from 'components/AvatarImage/AvatarImage';
import RichMedia from 'components/RichMediaComponent';
import Arrow from 'components/Ui/Arrow';
import Button from 'components/Ui/Button';
import { toChecksumAddress } from 'evm-chain-scripts';
import { useGlobalState } from 'globalStateStore';
import { getExplorerLinkLabel } from 'helpers/profile';
import { STATUSES } from 'helpers/statuses';
import { explorer } from 'helpers/utils';
import twitterIcon from 'images/x-icon.png';
import moment from 'moment';
import Image from 'next/image';
import { useRouter } from 'next/router';
import { forwardRef, useEffect, useState } from 'react';
import styles from './SingleSubmission.module.scss';

const VOTE_COLORS = {
  downVote: '#ff1d25',
  upVote: '#7de213'
};

const INITIAL_VARIANTS = {
  downVote: 'white',
  upVote: 'white'
};

const SingleSubmission = forwardRef(
  (
    {
      item,
      idHolder,
      tokenHolder,
      index,
      onReadMoreModalOpen,
      refetchTaskData,
      status,
      setRichMediaReady,
      setTwitterSharing
    },
    ref
  ) => {
    const {
      score,
      richMedia,
      upVotes,
      downVotes,
      submissionHash,
      body,
      contractAddress,
      voterAddress,
      userId,
      isOs1155,
      networkId,
      timestamp,
      title
    } = item;

    const [address] = useGlobalState('address');
    const [space] = useGlobalState('space');
    const isOnlyTokenHolder = tokenHolder && !idHolder;
    const [arrowVariant, setArrowVariant] = useState(INITIAL_VARIANTS);
    const voteEqualToAddress = (vote) => vote === toChecksumAddress(address);
    const router = useRouter();

    const isClient = typeof window !== 'undefined';

    const onVoteClick = async (type) => {
      try {
        const payload = {
          type
        };

        const msg = JSON.stringify(payload);

        const requestBody = {
          msg,
          address
        };

        await axios.patch(`/api/tasks/${space.key}/vote/${submissionHash}`, {
          data: requestBody,
          address
        });
      } catch (error) {
        console.error(error);
      } finally {
        refetchTaskData();
      }
    };

    const setClassOnVoteButton = () => {
      const upVoted = upVotes.find(voteEqualToAddress);
      if (upVoted) return setArrowVariant({ ...INITIAL_VARIANTS, upVote: VOTE_COLORS['upVote'] });
      const downVoted = downVotes.find(voteEqualToAddress);
      if (downVoted)
        return setArrowVariant({ ...INITIAL_VARIANTS, downVote: VOTE_COLORS['downVote'] });
      return setArrowVariant(INITIAL_VARIANTS);
    };

    useEffect(() => {
      if (address) setClassOnVoteButton();
    }, [item.score]);

    const getScoreClassName = (score) => {
      if (score === 0) return styles.neutral;
      return score > 0 ? styles.green : styles.red;
    };

    const upDownDisabled = status === STATUSES.AWAITING || status === STATUSES.CLOSED || !address;

    const requestedId = router.query.id;

    const subRef = (el, identifier) => {
      if (item?._id === requestedId) {
        ref.current[identifier] = el;
      }
    };

    const handleTwitterShare = async () => {
      try {
        setTwitterSharing(true);

        const { data: success } = await axios.post(`/api/submissionimages/${item?._id}`);

        if (success) {
          const url = `https://twitter.com/intent/tweet?url=${
            location.origin + location.pathname
          }/${item?._id}`;

          const shareOnXurl = encodeURI(url);

          return window.open(shareOnXurl, '_blank', 'noopener,noreferrer');
        }
      } catch (error) {
        console.log('Error while upload twitter file', error);
      } finally {
        setTwitterSharing(false);
      }
    };

    return (
      <div className={styles.container}>
        <div ref={(el) => subRef(el, 'backgroundRef')}></div>
        <div className={styles.item} key={`opinion-item-${index}`}>
          <div className={styles.voteButtonsContainer}>
            <button
              className={styles.arrowUp}
              disabled={upDownDisabled}
              onClick={() => onVoteClick('upVotes')}>
              <Arrow fill={arrowVariant['upVote']} />
            </button>
            <span className={getScoreClassName(score)}>{score}</span>
            <button
              className={styles.arrowDown}
              disabled={upDownDisabled}
              onClick={() => onVoteClick('downVotes')}>
              <Arrow fill={arrowVariant['downVote']} />
            </button>
          </div>
          <div className={styles.image}>
            <AvatarImage
              nftAddress={contractAddress}
              style={{ maxWidth: '80px', width: '80px' }}
              voterAddress={voterAddress}
              voter={isOnlyTokenHolder ? null : userId}
              isOs1155={isOs1155}
              voterNetwork={networkId}
            />
          </div>
          <div className={styles.opinion}>
            <div className={styles.header}>
              <span>
                <a
                  href={explorer(networkId || '1', voterAddress)}
                  target="_blank"
                  rel="noopener noreferrer">
                  <button className={styles.profile}>
                    {getExplorerLinkLabel(isOnlyTokenHolder, isOs1155, userId, voterAddress)}
                  </button>
                </a>
                {` / ${moment(timestamp).fromNow()}`}
              </span>
              <form onClick={handleTwitterShare} target="_blank">
                <Button className={styles.twitterButton}>
                  <div className={styles.twitterIcon}>
                    <Image src={twitterIcon} alt="Twitter" />
                  </div>
                </Button>
              </form>
            </div>
            <div className={styles.value}>
              <div className={styles.tag}>{title}</div>
            </div>
            <div className={styles.body}>
              {body.length > 200 ? (
                <div className={styles.text}>
                  <span>{body.slice(0, 200)}...</span>
                  <button onClick={() => onReadMoreModalOpen(item)} className={styles.readMore}>
                    Read more
                  </button>
                </div>
              ) : (
                body
              )}
            </div>
          </div>
        </div>
        {richMedia?.type && isClient && (
          <div className={styles.item}>
            <RichMedia
              setRichMediaReady={setRichMediaReady}
              type={richMedia.type}
              link={richMedia.link}
            />
          </div>
        )}
      </div>
    );
  }
);

SingleSubmission.displayName = 'SingleSubmission';
export default SingleSubmission;
