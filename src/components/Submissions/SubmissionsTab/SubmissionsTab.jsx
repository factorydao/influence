import ReadMore from 'components/Modal/ReadMore';
import debounce from 'lodash.debounce';
import { useCallback, useEffect, useRef, useState } from 'react';
import Dropdown from 'react-dropdown';
import 'react-dropdown/style.css';
import SingleSubmission from '../SingleSubmission';
import styles from './SubmissionsTab.module.scss';
const dropDownOptions = [
  {
    value: 'newest',
    label: 'Newest',
    className: styles.option
  },
  {
    value: 'score',
    label: 'Score',
    className: styles.option
  },
  {
    value: 'oldest',
    label: 'Oldest',
    className: styles.option
  }
];

const SubmissionsTab = ({ data, idHolder, tokenHolder, refetchTaskData, status, setTwitterSharing }) => {
  const [selectedSort, setSelectedSort] = useState({ value: 'newest', label: 'Newest' });
  const [filteredSubmissions, setFilteredSubmissions] = useState(null);
  const [isReadMoreModalOpen, setIsReadMoreModalOpen] = useState(false);
  const [readMoreData, setReadMoreData] = useState({});
  const [richMediaReady, setRichMediaReady] = useState(false);

  const submissionRef = useRef({});

  useEffect(() => {
    filterOpinions();
  }, [selectedSort.value, data]);

  const handleOpinionFilter = (filter) => {
    setSelectedSort(filter);
  };

  const submissionScroll = () => {
    submissionRef.current['backgroundRef'].scrollIntoView({ behavior: 'smooth' });
    submissionRef.current['backgroundRef'].classList.add(styles.submissionHighlight);
  };

  const debouncedScroll = useCallback(
    debounce(() => {
      submissionScroll();
    }, 500),
    []
  );

  useEffect(() => {
    if (submissionRef.current['backgroundRef']) debouncedScroll();
    return () => {
      debouncedScroll.cancel();
    };
  }, [richMediaReady, filteredSubmissions, debouncedScroll, submissionRef]);

  const filterOpinions = () => {
    let newOne = [...data];

    if (newOne.length > 0) {
      switch (selectedSort.value) {
        case 'newest':
          newOne.sort((a, b) => b.timestamp - a.timestamp);
          break;
        case 'oldest':
          newOne.sort((a, b) => a.timestamp - b.timestamp);
          break;
        case 'score':
          newOne.sort((a, b) => b.score - a.score);
          break;
        default:
          newOne.sort((a, b) => b.timestamp - a.timestamp);
          break;
      }
    }

    setFilteredSubmissions(newOne);
  };

  const onReadMoreModalOpen = (data) => {
    setReadMoreData(data);
    setIsReadMoreModalOpen(true);
  };

  return (
    <div className={styles.container}>
      {Object.keys(readMoreData).length > 0 && (
        <ReadMore
          isOpen={isReadMoreModalOpen}
          onClose={() => setIsReadMoreModalOpen(false)}
          data={readMoreData}
          type={'submission'}
        />
      )}
      <div className={styles.opinions}>
        {filteredSubmissions && (
          <>
            {filteredSubmissions.length > 0 ? (
              filteredSubmissions.map((item, index) => (
                <SingleSubmission
                  setRichMediaReady={setRichMediaReady}
                  ref={submissionRef}
                  idHolder={idHolder}
                  tokenHolder={tokenHolder}
                  key={`submission-${index}`}
                  item={item}
                  index={index}
                  onReadMoreModalOpen={onReadMoreModalOpen}
                  refetchTaskData={refetchTaskData}
                  status={status}
                  setTwitterSharing={setTwitterSharing}
                />
              ))
            ) : (
              <p className={styles.empty}>There aren&apos;t any submissions here yet!</p>
            )}
          </>
        )}
      </div>
      {data.length > 0 && (
        <div className={styles.filters}>
          <div className={styles.sort}>
            <div className={styles.label}>SORT BY</div>
            <div className={styles.dropdownBtn}>
              <Dropdown
                className={styles.dropdown}
                controlClassName={styles.control}
                placeholderClassName={styles.placeholder}
                menuClassName={styles.menu}
                options={dropDownOptions}
                value={selectedSort}
                onChange={handleOpinionFilter}
                arrowClosed={<span className={styles.arrowClosed} />}
                arrowOpen={<span className={styles.arrowClosed} />}
              />
            </div>
          </div>
        </div>
      )}
    </div>
  );
};

export default SubmissionsTab;
