import classNames from 'classnames';
import Counter from 'components/Ui/Counter';
import { useState, useEffect } from 'react';
import styles from './TabBar.module.scss';

const tabItems = [
  {
    id: 1,
    text: 'Submissions',
    tab: 'submissions',
    counter: false
  },
  {
    id: 2,
    text: 'My Submissions',
    tab: 'mySubmissions',
    counter: false
  },
  {
    id: 3,
    text: 'Contributors',
    tab: 'contributors',
    counter: true
  }
];

const TabBar = ({
  onItemClick,
  className = '',
  counter = 0,
  selectedTab,
  isMySubmissionTabDisabled
}) => {
  let tabs = [...tabItems];
  const [selectedData, setSelectedData] = useState(tabs.find((x) => x.tab === selectedTab));

  if (isMySubmissionTabDisabled) {
    tabs.splice(1, 1);
  }

  useEffect(() => {
    handleItemClick(tabs[0]);
  }, []);

  const handleItemClick = (item) => {
    setSelectedData(item);
    onItemClick(item.tab);
  };

  return (
    <nav className={classNames(styles.container, className)}>
      {tabs.map((item, index) => {
        return (
          <div key={index} className={styles.item}>
            <div
              onClick={() => handleItemClick(item)}
              onKeyDown={() => handleItemClick(item)}
              role="button"
              tabIndex="0"
              title={item.text}
              className={classNames(styles.text, {
                [styles.selected]: item.id === selectedData?.id
              })}>
              <div>{item.text}</div>
            </div>
            {item.counter && (
              <div className={styles.counter}>
                <Counter counter={counter} />
              </div>
            )}
          </div>
        );
      })}
    </nav>
  );
};

export default TabBar;
