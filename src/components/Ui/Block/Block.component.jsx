import style from './Block.module.scss';

/**
 *
 * Block component.
 * Small container with header.
 * Used in proposal detail.
 *
 * @param {string} title - title of the block
 * @returns Block component
 */
const Block = ({ title, children }) => {
  return (
    <div>
      {title && <div className={style.title}>{title}</div>}
      {children}
    </div>
  );
};

export default Block;
