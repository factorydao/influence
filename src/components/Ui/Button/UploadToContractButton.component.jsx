import { toast } from 'react-toastify';
import axios from 'axios';
import { ethInstance } from 'evm-chain-scripts';
import styles from './Button.module.scss';
import Button from './Button.component';
// import voteRegistryContract from '../../../contracts/.json';
import React from 'react';

//TODO: ADD VOTE REGISTRY CONTRACT IMPORT
const voteRegistryContract = {
  abi: null
};

const UploadToContractButton = ({ setLoading, choicesItems, space, id }) => {
  const getStoreVoteResults = async () => {
    try {
      setLoading(true);
      const { data: resultsHash } = await axios.get(`/api/${space}/proposal/${id}/results`);
      const contract = await ethInstance.getContract('write', voteRegistryContract);
      await contract.addProposal(resultsHash, choicesItems[0].choice.id, space);
      toast.success(<span>Succesfully uploaded vote results.</span>, { position: 'top-center' });
    } catch (e) {
      toast.error(<span>{e}</span>, { position: 'top-center' });
      console.error(e);
    } finally {
      setLoading(false);
    }
  };
  return (
    <Button className={styles.uploadResultsButton} onClick={getStoreVoteResults}>
      UPLOAD RESULTS
    </Button>
  );
};

export default React.memo(UploadToContractButton);
