import styles from './Container.module.scss';

/**
 * Main container
 */
const Container = ({ children }) => {
  return <div className={styles.container}>{children}</div>;
};

export default Container;
