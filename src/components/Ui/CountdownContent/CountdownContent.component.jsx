const CountdownContent = ({ days, hours, minutes, seconds }) => {
  const dayLabel = 'day' + (days > 1 ? 's' : '');
  const hourLabel = 'hour' + (hours > 1 ? 's' : '');
  return <span>{`${days} ${dayLabel} ${hours} ${hourLabel} ${minutes} min. ${seconds} sec.`}</span>;
};

export default CountdownContent;
