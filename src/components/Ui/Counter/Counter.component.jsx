import classNames from 'classnames';
import styles from './Counter.module.scss';

/**
 *
 * Counter component.
 * Used in underline nav bar. It is such as Badge component
 *
 * @param {number} counter - value
 * @returns Counter component
 */
const Counter = ({ counter, className }) => {
  return <span className={classNames(styles.counter, className)}>{counter}</span>;
};

export default Counter;
