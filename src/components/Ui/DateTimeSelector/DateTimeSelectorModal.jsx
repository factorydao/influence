import { useState } from 'react';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import { toast } from 'react-toastify';
import Button from '../Button';
import Dialog from '../Dialog';
import styles from './DateTimeSelector.module.scss';

/**
 *
 * Date time component.
 * Used in the creation of the new proposal
 *
 * @param {string} defaultText - default text for button opener date time modal
 * @returns Date time component
 */
const DateTimeSelectorModal = ({
  onSelected,
  defaultText,
  minDate,
  dateValue,
  dataTest = '',
  isRankingDate = false
}) => {
  const [isOpen, setIsOpen] = useState(false);
  const [buttonText, setButtonText] = useState(
    dateValue
      ? dateValue.toLocaleString('en-US', {
          month: 'short',
          year: 'numeric',
          day: 'numeric',
          hour: 'numeric',
          minute: 'numeric',
          hour12: true
        })
      : defaultText
  );
  const [, setSelectedDate] = useState(defaultText);
  const [date, setDate] = useState(new Date());
  const [header, setHeader] = useState(defaultText); // dialog header text
  const [mode, setMode] = useState('date'); // dialog mode - date or time select
  const [hours, setHours] = useState('12');
  const [minutes, setMinutes] = useState('00');

  const isValidDate = () => {
    return date != null;
  };

  const isValidTime = () => {
    return /^\d{1,2}$/.test(hours) && /^\d{1,2}$/.test(minutes);
  };

  const checkDateAheadOfMinDate = () => {
    if (!minDate) return true;
    if (isRankingDate) return date >= minDate;
    return date > minDate;
  };

  const clear = () => {
    setDate(new Date());
    setIsOpen(false);
    setMode('date');
    setMinutes('00');
    setHours('12');
  };

  const onOpen = () => {
    if (!dateValue) {
      setDate(minDate ? new Date(minDate) : new Date());
    } else {
      setDate(dateValue);
      setMinutes(dateValue.getMinutes());
      setHours(dateValue.getHours());
    }
    setIsOpen(true);
  };

  const Timezone = ({ date }) => {
    const sign = date.getTimezoneOffset() > 0 ? '-' : '+';
    const offset = Math.abs(date.getTimezoneOffset() / 60);
    return <div>timezone: UTC {`${sign}${offset}`}</div>;
  };

  return (
    <div id="modal" data-test={dataTest}>
      <Button onClick={() => onOpen()} className={styles.trigger}>
        {buttonText}
      </Button>

      <Dialog handleClose={clear} isOpen={isOpen}>
        <div className={styles.container}>
          <div className={styles.header}>{header}</div>
          <div className={styles.body}>
            {mode === 'date' ? (
              <DatePicker
                selected={date}
                onChange={(date) => setDate(date)}
                inline={true}
                dateFormat="Pp"
                minDate={minDate}
                calendarClassName={styles.calendar}
              />
            ) : (
              <div className={styles.time}>
                <input
                  placeholder="hh"
                  className="input"
                  onChange={(e) => {
                    let h = parseInt(e.target.value);
                    if ((/^\d{1,2}|^$$/.test(e.target.value) && !h) || (h > 0 && h <= 24)) {
                      setHours(e.target.value);
                    }
                  }}
                  value={hours}
                />
                <div>:</div>
                <input
                  placeholder="mm"
                  className="input"
                  onChange={(e) => {
                    let m = parseInt(e.target.value);
                    if ((/^\d{1,2}|^$$/.test(e.target.value) && !m) || (m >= 0 && m <= 59)) {
                      setMinutes(e.target.value);
                    }
                  }}
                  value={minutes}
                />
              </div>
            )}
            <Timezone date={date} />
            <div className={styles.footer}>
              <Button
                onClick={() => {
                  clear();
                }}
                className={styles.button}>
                Cancel
              </Button>

              {mode === 'date' ? (
                <Button
                  className={styles.button}
                  disabled={!isValidDate()}
                  onClick={() => {
                    setHeader(defaultText.replace('date', 'time'));
                    setMode('time');
                  }}>
                  Next
                </Button>
              ) : (
                <Button
                  className={styles.button}
                  disabled={!isValidTime()}
                  onClick={() => {
                    date.setHours(hours);
                    date.setMinutes(minutes);
                    date.setSeconds(0);
                    const minHours = new Date(minDate).getHours();
                    const minMinutes = String(new Date(minDate).getMinutes()).padStart(2, '0');
                    if (checkDateAheadOfMinDate()) {
                      setSelectedDate(date);
                      setButtonText(
                        date.toLocaleString('en-US', {
                          month: 'short',
                          year: 'numeric',
                          day: 'numeric',
                          hour: 'numeric',
                          minute: 'numeric',
                          hour12: true
                        })
                      );
                      if (onSelected) {
                        onSelected(date);
                      }
                      clear();
                    } else if (isRankingDate && date < minDate) {
                      toast.error(
                        `Ranking end time should be past or equal ${minHours}:${minMinutes}`
                      );
                    } else {
                      toast.error(`End date time should be past ${minHours}:${minMinutes}`);
                    }
                  }}>
                  Select
                </Button>
              )}
            </div>
          </div>
        </div>
      </Dialog>
    </div>
  );
};

export default DateTimeSelectorModal;
