import styles from './Dialog.module.scss';

const Dialog = ({ isOpen, handleClose, children, type = 'button', form = '' }) => {
  return (
    <>
      {isOpen && <div className={styles.backdrop} />}
      <div className={styles.dialog}>
        {isOpen && (
          <div className={styles.modal}>
            <button className={styles.close} onClick={handleClose} type={type} form={form} />
            {children}
          </div>
        )}
      </div>
    </>
  );
};
export default Dialog;
