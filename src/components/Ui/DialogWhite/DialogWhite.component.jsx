import React from 'react';
import styles from './DialogWhite.module.scss';

const DialogWhite = ({ isOpen, handleClose, children }) => {
  return (
    <>
      {isOpen && <div className={styles.backdrop} />}
      <div className={styles.dialog}>
        {isOpen && (
          <div className={styles.modal}>
            <button className={styles.close} onClick={handleClose} />
            {children}
          </div>
        )}
      </div>
    </>
  );
};
export default DialogWhite;
