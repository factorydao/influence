import classNames from 'classnames';
import closeIcon from 'images/close.svg';
import Image from 'next/image';
import styles from './DialogWhite.module.scss';

const HackathonDialogWhite = ({ isOpen, handleClose, children, className }) => {
  if (!isOpen) return null;
  return (
    <>
      <div className={styles.backdrop} />
      <div className={classNames(className, styles.dialog)}>
        <div className={styles.modal}>
          <Image
            src={closeIcon}
            onClick={handleClose}
            className={styles.close}
            alt="close-dialog-icon"
          />
          {children}
        </div>
      </div>
    </>
  );
};
export default HackathonDialogWhite;
