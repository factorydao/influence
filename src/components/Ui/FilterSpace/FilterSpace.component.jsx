import { setGlobalState, useGlobalState } from 'globalStateStore';
import { STATUSES } from 'helpers/statuses';
import CloseBtn from 'images/spaces/influence/closeBtn.svg';
import Image from 'next/image';
import { useState } from 'react';
import styles from './FilterSpace.module.scss';
import Status from './Status/Status.component';
import Tag from './Tag/Tag.component';

const STATUS_OPTIONS = {
  proposal: [STATUSES.OPEN, STATUSES.DRAFT, STATUSES.CLOSED, STATUSES.AWAITING],
  task: [STATUSES.OPEN, STATUSES.RANKING, STATUSES.CLOSED, STATUSES.AWAITING]
};

const FilterSpace = ({ listFilterIndicator, tagItems, onClose }) => {
  const [listFilters, setListFilters] = useGlobalState(`${listFilterIndicator}ListFilters`);
  const [tags, setTags] = useState(listFilters.sign ?? []);
  const [statuses, setStatuses] = useState(listFilters.status);

  const onChangeHandler = ({ target }, value, type) => {
    const { checked } = target;
    let changeFn;
    switch (type) {
      case 'status':
        changeFn = setStatuses;
        break;
      case 'tag':
        changeFn = setTags;
        break;
    }
    return changeFn((prev) => {
      return checked ? [...prev, value] : prev.filter((el) => el !== value);
    });
  };

  const filterHandler = () => {
    setGlobalState(`${listFilterIndicator}ListActivePage`, 1);
    setListFilters((prevState) => {
      return {
        ...prevState,
        status: statuses,
        sign: tags
      };
    });
    onClose();
  };
  const clearFilterHandler = () => {
    setGlobalState(`${listFilterIndicator}ListActivePage`, 1);
    setListFilters({ ...listFilters, status: [], sign: [] });
    onClose();
  };

  return (
    <div className={styles.filterContainer}>
      <div className={styles.filterBox}>
        <div className={styles.filterHeader}>
          <span>filter results</span>
          <button className={styles.closeBtn} onClick={onClose}>
            <Image src={CloseBtn} alt="close-button" />
          </button>
        </div>
        <div className={styles.innerContainer}>
          <span>status</span>
          <Status
            statuses={statuses}
            listFilters={listFilters}
            options={STATUS_OPTIONS[listFilterIndicator]}
            onChangeHandler={onChangeHandler}
          />
        </div>
        <div className={styles.innerContainer}>
          <span>TAG</span>
          <Tag tags={tags} tagItems={tagItems} onChangeHandler={onChangeHandler} />
        </div>
        <div className={styles.filterBtns}>
          <button className={styles.clearBtn} onClick={clearFilterHandler}>
            clear
          </button>
          <button className={styles.applyBtn} onClick={filterHandler}>
            apply
          </button>
        </div>
      </div>
    </div>
  );
};

export default FilterSpace;
