import styles from './Tag.module.scss';

const Tag = ({ tagItems, tags, onChangeHandler }) => {
  return (
    <div className={styles.tagContainer}>
      {tagItems.map((option, index) => {
        const isChecked = tags.find((sign) => sign === option.id);
        return (
          <label key={index}>
            <input
              type="checkbox"
              id={option.id}
              name={option.text}
              value={option.text}
              checked={isChecked}
              onChange={(event) => onChangeHandler(event, option.id, 'tag')}
            />
            {option.text}
          </label>
        );
      })}
    </div>
  );
};

export default Tag;
