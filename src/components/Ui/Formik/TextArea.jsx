import styles from './FormikFields.module.scss';
import { useField } from 'formik';

const TextArea = ({ label, ...props }) => {
  const [field, meta] = useField(props);
  return (
    <>
      <label className={styles.label} htmlFor={props.id || props.name}>
        {label}
      </label>
      <textarea rows="5" cols="33" className="textarea" {...field} {...props}></textarea>
      {meta.touched && meta.error ? <div className={styles.error}>{meta.error}</div> : null}
    </>
  );
};

export default TextArea;
