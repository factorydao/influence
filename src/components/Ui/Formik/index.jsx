import TextArea from './TextArea';
import TextInput from './TextInput';

export default {
  TextArea,
  TextInput
};
