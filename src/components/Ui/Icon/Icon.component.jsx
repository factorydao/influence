/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
import classNames from 'classnames';
import styles from './Icon.module.scss';
/**
 *
 * Icon component
 * Generates an icon with the given params
 *
 * @param {string} name - icon name
 * @param {string} size - icon size
 * @param {Function} onClick - icon on click action
 * @returns
 */
const Icon = ({ name, size, className, onClick }) => {
  const classResult = className ? className : ' ';

  return (
    <i
      onClick={onClick}
      style={{ fontSize: `${size}px`, lineHeight: `${size}px` }}
      className={classNames(styles.item, 'iconfont', classResult, `icon${name}`)}></i>
  );
};

export default Icon;
