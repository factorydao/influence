import Dropdown from 'react-dropdown';
import 'react-dropdown/style.css';
import styles from './ItemsPerPageSelect.module.scss';

const dropDownOptionsItemsPerPage = [
  {
    value: '5',
    label: '5',
    className: styles.option
  },
  {
    value: '10',
    label: '10',
    className: styles.option
  },
  {
    value: '25',
    label: '25',
    className: styles.option
  }
];

const ItemsPerPageSelect = ({ value, onChange }) => {
  return (
    <Dropdown
      className={styles.dropdown}
      controlClassName={styles.control}
      placeholderClassName={styles.placeholder}
      menuClassName={styles.menu}
      options={dropDownOptionsItemsPerPage}
      value={value}
      onChange={onChange}
      arrowClosed={<span className={styles.arrowClosed} />}
      arrowOpen={<span className={styles.arrowOpen} />}
    />
  );
};

export default ItemsPerPageSelect;
