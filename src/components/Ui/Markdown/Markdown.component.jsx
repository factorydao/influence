import classNames from 'classnames';
import DomPurify from 'isomorphic-dompurify';
import { Remarkable } from 'remarkable';
import { linkify } from 'remarkable/linkify';
import styles from './Markdown.module.scss';

DomPurify.addHook('beforeSanitizeElements', function (current) {
  if (current.href) {
    current.setAttribute('rel', 'noopener noreferrer');
  }
  return current;
});

function sanitizer(str) {
  return DomPurify.sanitize(str, {
    ADD_ATTR: ['target']
  });
}

const remarkable = new Remarkable({
  html: false,
  breaks: true,
  typographer: false,
  linkTarget: '_blank'
}).use(linkify);

const Markdown = ({ body, className }) => {
  const render = remarkable.render(body);
  const sanitized = sanitizer(render);

  return (
    <div
      className={classNames(styles.markdown, className)}
      dangerouslySetInnerHTML={{ __html: sanitized }}
    />
  );
};

export default Markdown;
