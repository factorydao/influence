import styles from './Progress.module.scss';

/**
 *
 * Progress bar component.
 * Used in semantic ballot component.
 *
 * @param {*} value - obect or array with value
 * @param {number} max - max value for progress to calculate percents
 * @param {string} bgColor - background color for progress
 * @param {Boolean} isTokensLeft - is token left for balance bar
 * @param {Boolean} isNegative - is negative value to chenge background and justifyContent(negative values start from right)
 * @returns Progress bar component
 */
const Progress = ({ value, max, bgColor, isTokensLeft = false, isNegative = false }) => {
  return (
    <span
      className={styles.progress}
      style={{
        justifyContent: isNegative ? 'flex-end' : 'flex-start'
      }}>
      <span
        className={isTokensLeft ? 'bg-strawberry' : bgColor}
        style={{
          width: `${parseFloat((100 / max) * Math.abs(value)).toFixed(3)}%`,
          backgroundColor: isNegative ? '#ffe3e4' : ``
        }}></span>
    </span>
  );
};

export default Progress;
