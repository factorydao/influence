import classNames from 'classnames';
import Icon from '../Icon';
import styles from './SearchBox.module.scss';
/**
 *
 * SearchBox component
 *
 * @param {string} searchValue - value from search input
 * @param {Function} onChangeSearchValue - on click action
 * @returns SearchBox component
 */
const SearchBox = ({ searchValue, onChangeSearchValue, placeholder = 'Search', maxLength }) => {
  return (
    <div className={styles.searchBox}>
      <label className={styles.searchLabel}>
        <input
          placeholder={placeholder}
          value={searchValue}
          className={classNames(styles.searchInput, 'input')}
          onChange={(event) => onChangeSearchValue(event.target.value)}
          maxLength={maxLength}
        />
        {searchValue.length > 0 ? (
          <Icon name="close" onClick={() => onChangeSearchValue('')} className={styles.icon} />
        ) : null}
        <Icon name="search" size="24" className={styles.icon} />
      </label>
    </div>
  );
};

export default SearchBox;
