import Multiselect from 'multiselect-react-dropdown';
import styles from './Select.module.scss';

const Select = ({
  name,
  label,
  placeholder,
  onChange,
  initialValue,
  optionsArray,
  displayValue,
  error = null,
  touched = false,
  onBlur = () => {}
}) => {
  return (
    <div className={styles.multiSelect} onBlur={onBlur}>
      <label className={styles.multiSelectLabel} htmlFor="multiselectContainerReact">
        {label}
      </label>
      <Multiselect
        name={name}
        options={optionsArray} // Options to display in the dropdown
        selectedValues={initialValue} // Preselected value to persist in dropdown
        onSelect={onChange} // Function will trigger on select event
        onRemove={onChange} // Function will trigger on remove event
        displayValue={displayValue} // Property name to display in the dropdown options
        placeholder={placeholder}
        style={{
          multiselectContainer: {
            width: '100%',
            minHeight: '2.5rem'
          },
          inputField: {
            color: 'white',
            margin: 'auto 0',
            fontSize: '20px'
          },
          searchBox: {
            height: '100%',
            borderRadius: '0',
            border: '1px solid var(--light-gray-blue)',
            display: 'flex',
            flexWrap: 'wrap'
          },
          chips: {
            fontSize: '20px',
            textWrap: 'wrap',
            wordBreak: 'break-all'
          },
          optionContainer: {
            background: 'var(--black)',
            border: '1px solid var(--light-gray-blue)',
            borderRadius: '0',
            fontSize: '20px'
          }
        }}
      />
      {error && touched ? <span className={styles.multiSelectError}>{error}</span> : null}
    </div>
  );
};

export default Select;
