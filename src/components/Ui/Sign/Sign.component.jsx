import classNames from 'classnames';
import { calcHexBrightness } from 'helpers/utils';
import styles from './Sign.module.scss';

/**
 *
 * Sign component
 * Show information about proposal sign (YLD, MKT, GOV, LDN21, etc..)
 * base on data from config file
 *
 * @param {string} color - bg color
 * @param {string} text - sign text
 * @returns component with information about proposal state
 */
const Sign = ({ color, className, children }) => {
  const backgroundBrightness = calcHexBrightness(color);

  const style = {
    backgroundColor: color,
    color: backgroundBrightness > 127 ? '#000000' : '#ffffff'
  };

  return (
    <div className={classNames(styles.sign, className)} style={style}>
      {children}
    </div>
  );
};

export default Sign;
