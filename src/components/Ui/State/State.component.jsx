import classNames from 'classnames';
import { getProposalStatus, getTaskStatus, STATUSES } from 'helpers/statuses';
import styles from './State.module.scss';

//TODO: add PREVOTE status in styles
const getClassNameForStatus = (status) => {
  switch (status) {
    case STATUSES.DRAFT:
      return 'draft';
    case STATUSES.OPEN:
      return 'open';
    case STATUSES.CLOSED:
      return 'closed';
    case STATUSES.AWAITING:
      return 'awaiting';
    case STATUSES.PREVOTE:
      return 'prevote';
    case STATUSES.RANKING:
      return 'ranking';
  }
};

/**
 *
 * State component
 * Show information about proposal state (open or closed) base on timestamp and adjust styles for them
 *
 * @param {Number} start - proposal start timestamp
 * @param {Number} end- proposal end timestamp
 * @param {Boolean} textVisible - show/hide text
 * @param {Boolean} isDraft - is proposal in draft mode
 * @returns component with information about proposal state
 */
const State = ({
  isProposal = true,
  textVisible = false,
  startDate,
  endDate,
  isDraft,
  isPrevote,
  upDownEndDate
}) => {
  const status = isProposal
    ? getProposalStatus(startDate, endDate, isDraft, isPrevote)
    : getTaskStatus(startDate, endDate, upDownEndDate);

  const iconClassName = getClassNameForStatus(status);

  return (
    <div className={classNames(styles.state, styles[iconClassName])}>
      {status === STATUSES.RANKING ? (
        <div className={styles.rankingIconContent}>
          <div className={classNames(styles.arrow, styles.arrowUp)}></div>
          <div className={classNames(styles.arrow, styles.arrowDown)}></div>
        </div>
      ) : (
        <div className={styles.icon} />
      )}
      {textVisible && <div className={styles.text}>{iconClassName}</div>}
    </div>
  );
};

export default State;
