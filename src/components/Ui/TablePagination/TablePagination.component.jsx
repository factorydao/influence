import If from 'components/If';
import 'react-dropdown/style.css';
import ReactPaginate from 'react-paginate';
import styles from './TablePagination.module.scss';

const TablePagination = ({ forcePage, onPageChange, pageCount, totalRows }) => {
  return (
    <>
      <If condition={totalRows > 0}>
        <div className={styles.footer}>
          <ReactPaginate
            forcePage={forcePage}
            pageCount={pageCount}
            pageRangeDisplayed={3}
            marginPagesDisplayed={1}
            onPageChange={onPageChange}
            previousLabel="<"
            nextLabel=">"
            containerClassName={styles.paginationContainer}
            activeClassName={styles.paginationActivePage}
            pageLinkClassName={styles.paginationLink}
          />
        </div>
      </If>
    </>
  );
};

export default TablePagination;
