import classNames from 'classnames';
import styles from './Tag.module.scss';

/**
 *
 * Tag component.
 * Used in opinions list.
 *
 * @param {string} color - background color
 * @param {string} text - text
 * @returns Tag component
 */
const Tag = ({ color, className, text }) => {
  return (
    <div className={classNames(styles.tag, className)} style={{ backgroundColor: color }}>
      {text}
    </div>
  );
};

export default Tag;
