import { ConnectWallet, ethInstance } from 'evm-chain-scripts';
import { setGlobalState } from 'globalStateStore';
import { useEffect, useRef } from 'react';
import styles from './WalletButton.module.scss';

const WalletButton = () => {
  const walletRef = useRef(null);

  useEffect(() => {
    setGlobalState('walletRef', walletRef);
    //on changing metamask address
    window.ethereum?.on('accountsChanged', ethInstance.handleAccountsChanged.bind(ethInstance));
  }, []);

  const handleLoad = async () => {
    try {
      const ethAccount = await ethInstance.getEthAccount(false);
      setGlobalState('address', ethAccount);
    } catch (e) {
      console.log(e);
    }
  };

  return (
    <div className={styles.button} ref={walletRef}>
      <ConnectWallet addressContainerClassName={styles.address} onConnect={handleLoad} />
    </div>
  );
};

export default WalletButton;
