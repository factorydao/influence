export const hosts = {
  ethPrague: 'https://vote.ethprague.com/',
  ethPraguePath: '/ethprague',
  ethBrno: 'https://vote.ethbrno.cz/',
  ethBrnoPath: '/ethbrno'
};
