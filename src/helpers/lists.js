import axios from 'axios';

export const getSignOptions = async (spaceKey, isForm, className, signal) => {
  try {
    const { data } = await axios.get(`/api/spaces/${spaceKey}/sign-options`, {
      signal
    });
    if (isForm) {
      return data?.map((sign) => ({ value: sign.label, label: sign.label, className })) ?? [];
    }
    const result = data.map(({ _id, label, color }) => ({
      id: _id,
      text: label,
      tab: label,
      counter: false,
      color: color
    }));
    return result;
  } catch (error) {
    throw ('Sign options loading error', error);
  }
};
