import { ethInstance } from 'evm-chain-scripts';
const zeroHash = '0x0000000000000000000000000000000000000000000000000000000000000000';

function createMerkleProof(tree, targetAddress) {
  // TODO: this is linear, probably should make it a hash map to make it constant
  const leaf = tree.nodes.find((x) => x.data.address.toLowerCase() == targetAddress.toLowerCase());
  let x = { ...leaf };
  const proof = [];
  while (x.parentIndex > -1) {
    const parent = tree.nodes[x.parentIndex];
    const flip = x.index === parent.leftChildIndex;
    if (flip) {
      proof.push(tree.nodes[parent.rightChildIndex].hash);
    } else {
      proof.push(tree.nodes[parent.leftChildIndex].hash);
    }
    x = parent;
  }
  // console.log({ leaf, proof });
  return proof;
}

function checkMerkleProof(proof, root, targetAddress, uri) {
  const web3 = ethInstance.readWeb3();
  let a = web3.utils.sha3(
    web3.eth.abi.encodeParameters(['address', 'string'], [targetAddress, uri])
  );
  for (let i = 0; i < proof.length; i++) {
    let b = proof[i];
    // console.log('proof step', { a, b });
    let p = parentHash(a, b);
    a = p[0];
  }
  const correct = a === root;
  // console.log({ correct, a, root });
  return correct;
}

function getLeafHash(obj, hashTypes, hashKeys) {
  const web3 = ethInstance.readWeb3();
  return web3.utils.sha3(
    web3.eth.abi.encodeParameters(
      hashTypes,
      hashKeys.map((x) => obj[x])
    )
  );
}

function createMerkleTree(leaves, hashTypes, hashKeys) {
  let numNodes = 0;
  let allNodes = leaves.map((x) => {
    const hash = getLeafHash(x, hashTypes, hashKeys);

    return {
      index: numNodes++,
      data: x,
      hash: hash,
      parentIndex: -1,
      leftChildIndex: -1,
      rightChildIndex: -1
    };
  });
  let currentRow = allNodes.map((x) => x.index);
  let parentRow = [];
  let allRows = [];
  while (currentRow.length > 1) {
    console.log({ currentRow });
    // handle case where the tree is not binary by adding zero hash
    if (currentRow.length % 2) {
      allNodes.push({
        index: numNodes++,
        hash: zeroHash,
        data: null,
        parentIndex: -1,
        leftChildIndex: -1,
        rightChildIndex: -1
      });
      currentRow.push(numNodes - 1);
    }
    // loop over current row
    for (let i = 0; i < currentRow.length; i += 2) {
      const [pHash, flip] = parentHash(
        allNodes[currentRow[i]].hash,
        allNodes[currentRow[i + 1]].hash
      );
      const newNode = {
        index: numNodes++,
        hash: pHash,
        data: null,
        parentIndex: -1,
        leftChildIndex: flip ? allNodes[currentRow[i + 1]].index : allNodes[currentRow[i]].index,
        rightChildIndex: flip ? allNodes[currentRow[i]].index : allNodes[currentRow[i + 1]].index
      };
      allNodes[currentRow[i]].parentIndex = numNodes - 1;
      allNodes[currentRow[i + 1]].parentIndex = numNodes - 1;
      allNodes.push(newNode);
      parentRow.push(newNode.index);
    }
    // keep track of rows
    allRows.push(currentRow);
    // move pointer to next row
    currentRow = parentRow;
    // zero out parent
    parentRow = [];
  }
  allRows.push(currentRow);
  return { rows: allRows, nodes: allNodes, root: allNodes[currentRow] };
}

function parentHash(a, b) {
  const web3 = ethInstance.readWeb3();
  if (a < b) {
    return [web3.utils.sha3(a + b.slice(2)), false];
  } else {
    return [web3.utils.sha3(b + a.slice(2)), true];
  }
}

function generateRandomData(numLeaves) {
  const web3 = ethInstance.readWeb3();
  const leaves = [];
  for (let i = 0; i < numLeaves; i++) {
    leaves.push({ uri: 'ipfs://' + web3.utils.randomHex(20), address: web3.utils.randomHex(20) });
  }
  return leaves;
}

export { createMerkleProof, checkMerkleProof, createMerkleTree, generateRandomData };
