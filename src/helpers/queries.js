import {
  fetchStrategies,
  generateChoices,
  getMultiChainPowerFromBackend,
  getPowerFromBackend,
  getSpaceData,
  getUserRank
} from 'helpers/utils';
import { useQuery } from 'react-query';
import {
  calcVotesAndOpinions,
  calculateResults,
  fetchNftImage,
  fetchProposalVotesData,
  getIdentities,
  getIdentitiesDropdownOptions,
  getMultiChainIdentitiesDropdownOptions,
  getMultiChainVoterNftIdentities,
  getMyInfluenceProps,
  getProposalData,
  getVoteComponentProps,
  getVotersNftIdentities,
  sortVotesList
} from './proposal';
import { getTaskData } from './tasks';

const QUERIES = {
  SCORE: 'score',
  MULTI_CHAIN_SCORE: 'multi-chain-score',
  MULTI_CHAIN_IDENTITIES: 'MULTI_CHAIN_IDENTITIES',
  MULTI_CHAIN_IDENTITIES_DROPDOWN: 'MULTI_CHAIN_IDENTITIES_DROPDOWN',
  IDENTITIES: 'IDENTITIES',
  NFTS: 'NFT_IDENTITIES',
  VOTESOPINIONS: 'VOTES_OPINIONS',
  IDENTITIESDROPDOWN: 'IDENTITIES_DROPDOWN',
  NFTIMAGE: 'NFT_IMAGE',
  VOTEPROPS: 'VOTE_PROPS',
  TASKDATA: 'TASK_DATA',
  GET_PROPOSAL_DATA: 'PROPOSAL_DATA',
  FETCHED_VOTES_DATA: 'PROPOSAL_VOTES_DATA',
  SORTED_VOTES_LIST: 'SORTED_VOTES_LIST',
  PROPOSAL_RESULTS: 'PROPOSAL_RESULTS',
  RANK: 'RANK',
  SPACE_DATA: 'SPACE_DATA'
};

export const useScoreQuery = ({ address, currentNetwork, proposalHash, strategy, identity }) =>
  useQuery(
    [QUERIES.SCORE, proposalHash, address, currentNetwork, identity],
    async () => getPowerFromBackend({ address, currentNetwork, proposalHash, identity }),
    {
      enabled: !!(
        address &&
        currentNetwork &&
        proposalHash &&
        (strategy?.params.type !== 'nft' || identity)
      )
    }
  );

export const useMultiChainScoreQuery = ({ address, proposalHash }) =>
  useQuery(
    [QUERIES.MULTI_CHAIN_SCORE, address, proposalHash],
    async () => getMultiChainPowerFromBackend({ address, proposalHash }),
    {
      enabled: !!(address && proposalHash)
    }
  );

export const useUserRank = ({ address, proposalHash, currentNetwork, strategyKey }) =>
  useQuery(
    [QUERIES.RANK, address, proposalHash],
    async () => getUserRank({ address, proposalHash, currentNetwork }),
    {
      enabled: !!(
        address &&
        proposalHash &&
        currentNetwork &&
        ['deposit-rank', 'deposit-balance'].includes(strategyKey)
      )
    }
  );

export const useGetUserMultiChainIdentities = (votes, strategy, address, acceptableChains) =>
  useQuery(
    [QUERIES.MULTI_CHAIN_IDENTITIES, votes, strategy, address, acceptableChains],
    async () => getMultiChainVoterNftIdentities(votes, strategy, address, acceptableChains),
    {
      enabled: !!(address || (votes && strategy)),
      refetchOnWindowFocus: false
    }
  );

export const useGetUserIdentities = (votes, strategy, isOpenVote, address, currentNetwork) =>
  useQuery(
    [QUERIES.IDENTITIES, votes, strategy, isOpenVote, address, currentNetwork],
    async () => getIdentities(votes, strategy, isOpenVote, address, currentNetwork),
    {
      enabled: !!(address && votes && currentNetwork && strategy),
      refetchOnWindowFocus: false
    }
  );

export const useGetVotersNftIdentities = (address, votes, isOpenVote, currentNetwork, strategy) =>
  useQuery(
    [QUERIES.NFTS, address, votes, isOpenVote, currentNetwork, strategy],
    async () => getVotersNftIdentities(address, votes, isOpenVote, currentNetwork, strategy),
    {
      enabled: !!(address && votes && currentNetwork && strategy),
      refetchOnWindowFocus: false
    }
  );

export const useGetMyInfluenceProps = (
  tab,
  isProposalVisible,
  address,
  totalI,
  strategy,
  choices
) =>
  useQuery(
    [QUERIES.NFTS, tab, isProposalVisible, address, totalI, strategy],
    () => getMyInfluenceProps(tab, isProposalVisible, address, totalI, strategy),
    {
      enabled: !!(tab && totalI >= 0 && strategy && choices)
    }
  );

export const useCalcVotesAndOpinions = (votes, choices, id, isBinary) =>
  useQuery(
    [QUERIES.VOTESOPINIONS, Object.keys(votes), choices, id],
    async () => calcVotesAndOpinions(votes, choices, isBinary),
    {
      enabled: !!(votes && choices, id)
    }
  );

export const useGetMultiChainIdentitiesDropdownOptions = (identities, className) =>
  useQuery(
    [QUERIES.MULTI_CHAIN_IDENTITIES_DROPDOWN, identities, className],
    async () => getMultiChainIdentitiesDropdownOptions(identities, className),
    {
      enabled: !!identities
    }
  );

export const useGetIdentitiesDropdownOptions = (
  identities,
  strategy,
  shortLabel,
  className,
  address
) =>
  useQuery(
    [QUERIES.IDENTITIESDROPDOWN, Object.keys(identities), strategy, shortLabel, className, address],
    async () => getIdentitiesDropdownOptions(identities, strategy, shortLabel, className),
    {
      enabled: !!(address && strategy)
    }
  );

export const useFetchNftImage = (nftId, contractAddress, chainId, isOs1155, isGalleryVote) =>
  useQuery(
    [QUERIES.NFTIMAGE, nftId, contractAddress, chainId, isOs1155],
    async () => fetchNftImage(nftId, contractAddress, chainId, isOs1155, isGalleryVote),
    {
      enabled: !!(nftId && chainId),
      refetchOnWindowFocus: false
    }
  );

export const useGetVoteComponentProps = (
  address,
  strategy,
  setUnsortedChoices,
  setTokensLeft,
  tab,
  choices
) =>
  useQuery(
    [QUERIES.VOTEPROPS, address, strategy, tab],
    () => getVoteComponentProps(address, strategy, setUnsortedChoices, setTokensLeft, choices),
    {
      enabled: !!(address && strategy && choices.length)
    }
  );

export const useLoadChoicesData = (results, payload, tab) =>
  useQuery(['loadData', payload, results, tab], () => generateChoices(payload?.choices, results), {
    enabled: !!(payload && tab),
    refetchOnWindowFocus: false
  });

export const useGetTaskData = (taskHash, spaceKey, address) =>
  useQuery(
    [QUERIES.TASKDATA, taskHash, spaceKey, address],
    async () => getTaskData(taskHash, spaceKey, address),
    {
      enabled: !!(taskHash && spaceKey),
      refetchOnWindowFocus: false,
      initialData: {
        submissionsData: [],
        mySubmissionsData: [],
        contributorsData: [],
        identitiesForRealmByUser: {},
        isEligible: false,
        questionData: {}
      }
    }
  );

export const useFetchProposalVotesData = (
  proposal,
  address,
  spaceKey,
  id,
  signWindowOpenRef,
  currentNetwork
) =>
  useQuery(
    [
      QUERIES.FETCHED_VOTES_DATA,
      spaceKey,
      address,
      id,
      currentNetwork,
      proposal && Object.keys(proposal)
    ],
    async () =>
      fetchProposalVotesData(proposal, address, spaceKey, id, signWindowOpenRef, currentNetwork),
    {
      enabled: !!(spaceKey && id && currentNetwork && proposal),
      refetchOnWindowFocus: false,
      initialData: {
        votes: {},
        isProposalVisible: false,
        isCombinedChainsVoting: false
      }
    }
  );

export const useSortVotesList = (votes) =>
  useQuery([QUERIES.SORTED_VOTES_LIST, Object.keys(votes)], async () => sortVotesList(votes), {
    enabled: !!(votes && Object.keys(votes)),
    refetchOnWindowFocus: false
  });

export const useCalculateResults = (
  votes,
  currentNetwork,
  proposal,
  address,
  isCombinedChainsVoting = false
) =>
  useQuery(
    [
      QUERIES.PROPOSAL_RESULTS,
      Object.keys(votes),
      currentNetwork,
      proposal,
      address,
      isCombinedChainsVoting
    ],
    async () => calculateResults(votes, currentNetwork, proposal, address, isCombinedChainsVoting),
    {
      enabled: !!(votes && currentNetwork && proposal && proposal?.strategy?.key),
      refetchOnWindowFocus: false,
      initialData: {
        influenceResult: [],
        myInfluenceResult: [],
        myInfluenceResultPerIdentity: []
      }
    }
  );

export const useGetProposalData = (id) =>
  useQuery([QUERIES.GET_PROPOSAL_DATA, id], async () => getProposalData(id), {
    enabled: !!id,
    refetchOnWindowFocus: false
  });

export const useGetStrategies = () =>
  useQuery(['strategies'], async () => fetchStrategies(), {
    enabled: true,
    refetchOnWindowFocus: false
  });

export const useGetSpaceData = (spaceName) =>
  useQuery([QUERIES.SPACE_DATA, spaceName], async () => getSpaceData(spaceName), {
    enabled: !!spaceName,
    refetchOnWindowFocus: false
  });
