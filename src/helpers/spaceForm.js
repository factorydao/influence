
export const REALM_RESTRICTION_TYPE = {
  STANDARD: 'standard',
  NFT: 'nft'
};

export const defaultNftAddresses = [
  {
    itemId: 0,
    chainId: '',
    address: '',
    minNftAmount: '',
    errors: {}
  }
];
