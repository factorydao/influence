import axios from 'axios';
import networks from 'configs/networks.json';
import { ethInstance } from 'evm-chain-scripts';
import numeral from 'numeral';
import { Cookies } from 'react-cookie';
import { getBlockNumberPerNetwork } from './influence';

export function jsonParse(input, fallback) {
  if (typeof input !== 'string') {
    return fallback || {};
  }
  try {
    return JSON.parse(input);
  } catch (err) {
    return fallback || {};
  }
}

export function formatProposals(proposals) {
  return Object.fromEntries(
    Object.entries(proposals).map((proposal) => [proposal[0], formatProposal(proposal[1])])
  );
}

export function formatProposal(proposal) {
  proposal.msg = jsonParse(proposal.msg, proposal.msg);
  return proposal;
}

export function ipfsUrl(ipfsHash) {
  return `https://factorydao.infura-ipfs.io/ipfs/${ipfsHash}`;
}

export function shorten(str, key) {
  if (!str) return str;
  let limit;
  if (typeof key === 'number') limit = key;
  if (key === 'symbol') limit = 6;
  if (key === 'name') limit = 64;
  if (key === 'choice') limit = 12;
  if (limit) return str.length > limit ? `${str.slice(0, limit).trim()}...` : str;
  return `${str.slice(0, 6)}...${str.slice(str.length - 4)}`;
}

export function generateRandomBackground(id) {
  let hash = 0;
  if (id === 0) return hash;
  for (let i = 0; i < id.length; i++) {
    hash = id.charCodeAt(i) + ((hash << 5) - hash);
    hash = hash & hash;
  }

  var color = '#';
  for (let i = 0; i < 3; i++) {
    var value = (hash >> (i * 8)) & 255;
    color += ('00' + value.toString(16)).substr(-2);
  }

  return color;
}

export function explorer(network, str, type = 'address') {
  return `${networks[network]?.explorer}/${type}/${str}`;
}

export function n(number, format = '(0.[00]a)') {
  return numeral(number).format(format);
}

export function shuffle(array) {
  var currentIndex = array.length,
    randomIndex;

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {
    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex--;

    // And swap it with the current element.
    [array[currentIndex], array[randomIndex]] = [array[randomIndex], array[currentIndex]];
  }

  return array;
}

export async function getMultiChainPowerFromBackend({ address, proposalHash }) {
  try {
    const {
      data: { totalScore }
    } = await axios.get(`api/proposals/${proposalHash}/vote-power/${address}`);
    return totalScore;
  } catch (error) {
    console.log('GET_MULTI_POWER_FAILURE', error);
  }
}

export async function getPowerFromBackend({ address, currentNetwork, proposalHash, identity }) {
  try {
    const {
      data: { totalScore }
    } = await axios.get(
      `/api/proposals/${proposalHash}/vote-power/${address}?identity=${identity}&network=${currentNetwork}`
    );
    return totalScore;
  } catch (e) {
    console.log('GET_POWER_FAILURE', e);
  }
}

export async function getUserRank({ address, currentNetwork, proposalHash }) {
  try {
    const {
      data: { userPosition, maxLevel }
    } = await axios.get(`/api/proposals/${proposalHash}/rank/${address}?network=${currentNetwork}`);
    return { userPosition, maxLevel };
  } catch (e) {
    console.log('GET_USER_RANK', e);
    return { userPosition: 0, maxLevel: 0 };
  }
}

/**
 * @param {string} hex - Color in hexadecimal format, for instance "#000000" with "#" included
 *
 * @returns {number} - Retruns brightness in range of 0 - 255
 */
export function calcHexBrightness(hex) {
  if (!hex) return null;

  const hexInt = parseInt(hex.substr(1), 16);
  const r = (hexInt >> 16) & 255;
  const g = (hexInt >> 8) & 255;
  const b = hexInt & 255;

  const brightness = Math.round((r * 299 + g * 587 + b * 114) / 1000);

  return brightness;
}

export const generateChoices = (choices, results) => {
  const parsedChoices = choices.map((choice, i) => {
    const { submission } = choice;
    const choiceElem = submission
      ? {
          _id: choice._id,
          id: choice.id,
          value: submission.title,
          description: submission.body,
          richMedia: submission.richMedia
        }
      : choice;
    return {
      i,
      choice: choiceElem,
      progress: results?.length > 0 ? Math.round(results[i]) : 0,
      value: 0,
      opinion: choice.opinion
    };
  });
  return [shuffle(parsedChoices), [...parsedChoices].sort((a, b) => b.progress - a.progress)];
};

export const getSnapshotPerNetwork = async (networks) => {
  let spaceNetworkSnapshots = {};
  for (const network of networks) {
    spaceNetworkSnapshots[network] = await getBlockNumberPerNetwork(network);
  }
  return spaceNetworkSnapshots;
};

export const getParsedUserId = (userId) => {
  if (!userId) return {};

  const splitId = userId.split('-');
  const [chainId, contractAddress, nftName, nftId, os1155] = splitId;

  if (splitId.length > 1) {
    return {
      chainId,
      contractAddress,
      nftName,
      nftId,
      os1155
    };
  } else {
    return { contractAddress: userId };
  }
};

export const toBase64 = (file) =>
  new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = (error) => reject(error);
  });

export const getSessionToken = async (address) => {
  try {
    const cookies = new Cookies();
    if (cookies.get(`session_${address}`)) {
      return;
    }
    const msg = { timestamp: Date.now() };
    const signature = await ethInstance.signPersonalMessage(JSON.stringify(msg), address);
    const {
      data: { token }
    } = await axios.post('/api/auth', { address, signature, msg });

    const date = new Date();
    date.setHours(date.getHours() + 24);
    cookies.set(`session_${address}`, token, { expires: date });
  } catch (err) {
    console.error('Failed to get signature', err);
  }
};

export const validateAcceptableChains = (acceptableChains, contractsObject) => {
  return Object.keys(contractsObject)
    .filter((chainId) => acceptableChains.includes(parseInt(chainId)))
    .reduce((chainData, chainId) => {
      chainData[chainId] = contractsObject[chainId];
      return chainData;
    }, {});
};

export const getTimeZone = (date) => {
  const offset = new Date(date).getTimezoneOffset() / -60;
  return 'UTC' + (offset >= 0 ? '+' : '-') + offset;
};

export const fetchStrategies = async () => {
  try {
    const { data: strategies } = await axios.get('/api/strategies');
    return strategies;
  } catch (err) {
    console.log('Failed to fetch strategies.', err);
  }
};

export const getSpaceData = async (spaceName) => {
  try {
    const space = await axios.get(`/api/spaces/${spaceName}`)
    return space;
  } catch (err) {
    console.log('Failed to fetch space.', err);
  }
};
