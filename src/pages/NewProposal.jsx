import axios from 'axios';
import classNames from 'classnames';
import NewTagModal from 'components/Modal/ProposalTag/ProposalTag';
import StrategyParamsModal from 'components/Modal/StrategyParams';
import VoterListModal from 'components/Modal/Whitelist';
import Block from 'components/Ui/Block';
import Button from 'components/Ui/Button';
import Container from 'components/Ui/Container';
import DateTimeSelectorModal from 'components/Ui/DateTimeSelector/DateTimeSelectorModal';
import Icon from 'components/Ui/Icon';
import Markdown from 'components/Ui/Markdown';
import PageLoading from 'components/Ui/PageLoading';
import networksJSON from 'configs/networks.json';
import { parseFile, setElements } from 'evm-chain-scripts';
import { useGlobalState } from 'globalStateStore';
import { getSignOptions } from 'helpers/lists';
import { useGetTaskData } from 'helpers/queries';
import { getSnapshotPerNetwork } from 'helpers/utils';
import uniqueId from 'lodash/uniqueId';
import { useRouter } from 'next/router';
import Papa from 'papaparse';
import { useEffect, useState } from 'react';
import Dropdown from 'react-dropdown';
import styles from './NewProposal.module.scss';

/**
 *
 * Validate form
 *
 * @param {Object} values - object to validate
 * @returns errors object
 */

const findDuplicated = (value, index, array) => {
  return array.indexOf(value) !== array.lastIndexOf(value);
};

const validate = (values, choices, tabTag, strategy, acceptableChains, hackathonView) => {
  let errors = {};
  if (!values.question || values.question.trim() === '') {
    errors.question = 'Question is required';
  } else if (values.question.length > 128) {
    errors.question = 'Question is too long';
  }
  if (!values.answer || values.answer.trim() === '') {
    errors.answer = 'Answer is required';
  } else if (values.answer.length > 4e4) {
    errors.answer = 'Answer is too long, must be less than 40 000 characters';
  }
  if (!values.from) {
    errors.from = 'Start date is required';
  }
  if (!values.to) {
    errors.to = 'End date is required';
  }
  if (values.to && values.to <= Date.now()) {
    errors.to = 'End date cannot be set in the past';
  }
  if (values.from && values.to) {
    if (Date.parse(values.from) >= Date.parse(values.to)) {
      errors.to = 'End date should be later than start date';
    }
  }
  if (choices?.items) {
    if (choices?.items.length < 2) {
      errors.choiceCounter = 'Minimum 2 choices are required';
    }
    const { descriptionsArray, valuesArray } = choices.items.reduce(
      (result, current) => {
        result.valuesArray.push(current.value);
        current.description ? result.descriptionsArray.push(current.description) : null;
        return result;
      },
      { valuesArray: [], descriptionsArray: [] }
    );
    if (!values.basedOnTask) {
      const duplicatedDescriptions = descriptionsArray.some(findDuplicated);
      const duplicatedChoices = valuesArray.some(findDuplicated);
      if (duplicatedDescriptions || duplicatedChoices) {
        errors.duplicated = 'Description or values are duplicated';
      }
    }
    if (choices.items.find((item) => item.value.trim() === '')) {
      errors.choiceValues = 'Please input choice value';
    }
    if (descriptionsArray.some((i) => i.trim().length < 1)) {
      errors.choiceTags = 'Some of descriptions are blank values.';
    }
    if (!hackathonView && choices.items.find((choice) => choice.value.trim().length > 32)) {
      errors.choiceValues = 'Wrong length of choice value. Max 32 characters.';
    }
    if (!hackathonView && !values.basedOnTask) {
      if (descriptionsArray.some((desc) => desc.trim().length > 400)) {
        errors.choiceTags = 'Maximum length per description is 400 characters.';
      }
    }
  }
  if (!tabTag) {
    errors.tabTag = 'Choose tag';
  }
  if (!strategy) {
    errors.strategy = 'Choose strategy';
  }
  if (!acceptableChains.length) {
    errors.acceptableChains = 'You must select minimum one chain.';
  }
  // if (values.badge && values.badge.size > 2097152) {
  //   errors.badge = 'File is too big (max 2MB)';
  // }

  return errors;
};

const prepareObjToArray = (list) => {
  const listCopy = Object.assign({}, list);
  for (const netw in listCopy) {
    if (listCopy[netw].length) {
      listCopy[netw] = listCopy[netw].map((i) => i.value);
    }
  }
  return listCopy;
};

const prepareDynamicStrategyParam = (dynamicParams) => {
  let paramsToSave = {};
  let paramData;
  for (const param in dynamicParams) {
    if (param === 'addresses' || param === 'depositPools' || param === 'nftAddresses') {
      const dynamicDataObject = dynamicParams[param].dynamicParamData.reduce(
        (prevValue, currValue) => ({ ...prevValue, [currValue.chainId]: currValue.value }),
        {}
      );
      const originDataObject = dynamicParams[param].originParamData.reduce(
        (prevValue, currValue) => ({ ...prevValue, [currValue.chainId]: currValue.value }),
        {}
      );
      paramData = {
        ...dynamicParams[param],
        dynamicParamData: dynamicDataObject,
        originParamData: originDataObject
      };
    } else {
      paramData = dynamicParams[param];
    }
    const isEqual =
      JSON.stringify(paramData.dynamicParamData) === JSON.stringify(paramData.originParamData);

    if (!paramData.defaultValue && !isEqual) {
      paramsToSave[param] = paramData.dynamicParamData;
    }
  }

  return paramsToSave;
};

/**
 *
 * New proposal page
 *
 * @returns The form for create a new proposal
 */
const NewProposalPage = () => {
  const [space] = useGlobalState('space');
  const [strategies] = useGlobalState('strategies');
  const [address] = useGlobalState('address');
  const [isMember] = useGlobalState('isMember');
  const router = useRouter();
  const { taskHash, submissionsAmount } = router.query;

  const {
    data: { submissionsData, questionData }
  } = useGetTaskData(taskHash, space?.key, address);
  const [data, setData] = useState({
    // badge: null,
    question: questionData.title ?? '',
    answer: questionData.body ?? '',
    from: null,
    to: null,
    choiceCounter: 1,
    basedOnTask: !!submissionsData?.length
  });

  const mapSubmissionsToChoices = (submissions) => {
    const sortedSubmissions = submissions.sort((a, b) => b.score - a.score);
    const maxIndexOfElements = submissionsAmount - 1;
    let items = [];
    for (let i = 0; i < sortedSubmissions.length; i++) {
      const submission = sortedSubmissions[i];
      if (i < maxIndexOfElements) {
        items.push(submission);
        continue;
      }
      if (i === maxIndexOfElements) {
        const tempArr = sortedSubmissions.slice(i, sortedSubmissions.length).filter((subm) => {
          const submResult = subm.upVotes.length - subm.downVotes.length;
          const bRes = submission.upVotes.length - submission.downVotes.length;
          return submResult === bRes;
        });
        items = items.concat(tempArr);
        break;
      }
      break;
    }
    return {
      items: items.map((submission, index) => {
        return {
          submission: submission._id,
          value: submission.title,
          description: submission.body,
          id: index + 1
        };
      })
    };
  };

  const [includeHeaders, setIncludeHeaders] = useState(false);
  const [overwriteChoices, setOverwriteChoices] = useState(true);
  const [choiceCounter, setChoiceCounter] = useState({ counter: 1 }); // choices counter
  const [choices, setChoices] = useState(
    submissionsData.length
      ? mapSubmissionsToChoices(submissionsData)
      : {
          items: [{ id: choiceCounter.counter, value: '', description: '' }]
        }
  );

  const [networksSnapshots, setNetworkSnapshots] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [isSnapshotLoading, setIsSnapshotLoading] = useState(false);
  const [errors, setErrors] = useState({});
  const [tabTag, setTabTag] = useState(
    questionData.sign
      ? {
          value: questionData.sign.label,
          label: questionData.sign.label
        }
      : null
  );
  const [strategy, setStrategy] = useState();
  const [tagOptions, setTagOptions] = useState([]);
  // const [badgeFileName, setBadgeFileName] = useState(null);
  const [isNewProposalTagModalOpen, setIsNewProposalTagModalOpen] = useState(false);
  const [isStrategyParamsModalOpen, setIsStrategyParamsModalOpen] = useState(false);
  const [strategyInfo, setStrategyInfo] = useState({});
  const [winnerBox, setWinnerBox] = useState(false);
  const [hideVotes, setHideVotes] = useState(false);
  const [isDraft, setIsDraft] = useState(true);
  const [hackathonView, setHackatonView] = useState(false);
  const [whitelist, setWhitelist] = useState({});
  const [blacklist, setBlacklist] = useState({});
  const [acceptableChains, setAcceptableChains] = useState(
    space.networks ? [...space.networks] : []
  );
  const [dynamicStrategyParams, setDynamicStrategyParams] = useState({});
  const [isVoterListModalOpened, setIsVoterListModalOpened] = useState(false);
  const [nftUsersOnly, setNftUsersOnly] = useState(false);
  const strategiesDropOptions =
    strategies
      .filter((x) => space.strategies?.includes(x._id))
      .map((x) => {
        return {
          value: x._id,
          label: x.name,
          className: classNames(styles.open, styles.option)
        };
      }) ?? [];

  const availableChains = space.networks ?? [];

  useEffect(() => {
    setElements([
      {
        name: 'choices',
        headers: {
          name: {
            index: 0,
            validate: () => {}
          },
          description: {
            index: 1,
            validate: () => {}
          }
        }
      }
    ]);

    const controller = new AbortController();
    (async () => {
      try {
        setIsLoading(true);
        const signOpts = await getSignOptions(space.key, true, styles.option, controller.signal);
        setTagOptions(signOpts ?? []);
      } catch (e) {
        console.log(e);
      }
    })();
    setIsLoading(false);
    return () => controller.abort();
  }, []);

  const calculateSnapshots = async (networks) => {
    const networksSnapshots = await getSnapshotPerNetwork(networks);
    setNetworkSnapshots(networksSnapshots);
  };

  useEffect(() => {
    if (!space.key) return;
    if (space && (!acceptableChains || !acceptableChains.length)) {
      setAcceptableChains(space.networks);
    }
    if (!isMember) {
      //router.push(`/${space.key}`);
    } else {
      // get and set current snapshot and networks snapshots
      (async () => {
        try {
          setIsSnapshotLoading(true);
          let networks = acceptableChains || [];
          await calculateSnapshots(networks);
        } catch (error) {
          console.log('Snapshots loading error', error);
        } finally {
          setIsSnapshotLoading(false);
        }
      })();
    }
  }, [isMember, space]);

  const handleChange = (name, value) => {
    setData({ ...data, [name]: value });
  };

  const handleAddChoice = () => {
    let { counter } = choiceCounter;
    counter++;
    setChoiceCounter({ counter });
    let items = choices.items.concat({
      id: counter,
      value: '',
      description: ''
    });
    setChoices({ items });
  };

  /**
   *
   * Updates the choice name
   *
   * @param {Number} id - id of changed choice
   * @param {Object} ev - input event
   */
  const updateChoice = (id, ev, fieldName) => {
    let choice = choices.items.find((c) => c.id === id);
    let index = choices.items.findIndex((c) => c.id === id);
    if (choice) {
      let editChoice = {
        ...choice
      };
      editChoice[fieldName] = ev.target.value;
      choices.items.splice(index, 1);
      choices.items.splice(index, 0, editChoice);
      setChoices({ items: choices.items });
      if (choices.items.length > 1 && choices.items.every((x) => x.value)) {
        setData({ ...data, choiceCounter: choiceCounter });
      } else {
        setData({
          ...data,
          choiceCounter: choices.items.filter((x) => x.value).length
        });
      }
    }
  };

  /**
   *
   * Deletes the choice
   *
   * @param {Number} id - id of choice
   */
  const deleteChoice = (id) => {
    let index = choices.items.findIndex((c) => c.id === id);
    if (index >= 0) {
      choices.items.splice(index, 1);
      choices.items.map((c) => (c.id = c.id > id ? --c.id : c.id));
      setChoices({ items: choices.items });
      let counter = choiceCounter.counter;
      counter--;
      setChoiceCounter({ counter: counter });
      setData({ ...data, choiceCounter: counter });
    }
  };

  /**
   *
   * Set selected tab tag to proposal
   *
   * @param {Object} item - selected tab tag data
   */
  const tagHandle = (item) => {
    setTabTag(item);
  };

  /**
   *
   * @param {Object} item - selected strategy data
   */
  const strategyHandle = (item) => {
    if (
      ['Binary voting', 'TW Binary Voting', 'Aggregated Identity Binary Voting'].includes(
        item.label
      )
    ) {
      setChoiceCounter({ counter: 2 }); // choices counter
      setChoices({
        items: [
          { id: 1, value: 'Yes', description: '' },
          { id: 2, value: 'No', description: '' }
        ]
      });
      setData({ ...data, choiceCounter: 2 });
    }

    if (strategy?.label === 'Binary voting' && strategy?.label !== item.label) {
      setData({ ...data, choiceCounter: 1 });
      setChoiceCounter({ counter: 1 });
      setChoices({
        items: [{ id: 1, value: '', description: '' }]
      });
    }

    const { name, binary, params, key } = strategies
      .filter((x) => space.strategies?.includes(x._id))
      .find((strategy) => strategy._id === item.value);

    setStrategyInfo({ name, binary, key });

    let dynamicParamObject = {};

    for (const param in params) {
      let dynamicParamData;
      if (param === 'addresses' || param === 'depositPools' || param === 'nftAddresses') {
        let items = [];
        for (const chainId in params[param]) {
          items.push({
            itemId: parseInt(uniqueId()),
            chainId: parseInt(chainId),
            value: params[param][chainId]
          });
        }
        dynamicParamData = [...items];
      } else {
        dynamicParamData = params[param];
      }
      dynamicParamObject[param] = {
        defaultValue: true,
        dynamicParamData: dynamicParamData,
        originParamData: dynamicParamData
      };
    }

    setDynamicStrategyParams(dynamicParamObject);
    setStrategy(item);
  };

  // /**
  //  *
  //  * @param {File} file - selected badge image
  //  */
  // const badgeHandle = (file) => {
  //   if (file?.name) {
  //     setBadgeFileName(file.name);
  //   } else {
  //     setBadgeFileName(null);
  //   }
  //   handleChange('badge', file);
  // };

  /**
   *
   * If user selected start date is
   * in the past, returns minimum possible
   * end date set in the future
   *
   * @param {Date} [minStartDate] - User selected minimum startDate
   */
  const getMinimumEndDate = (minStartDate) => Math.max(minStartDate, Date.now());

  const formatPrizeTracks = (prizeTracks) => {
    return prizeTracks?.split(',').map((prizeTrack) => prizeTrack.replace('&comma;', ','));
  };

  const saveChoice = async () => {
    try {
      const errors = validate(data, choices, tabTag, strategy, acceptableChains, hackathonView);
      if (Object.keys(errors).length) {
        return setErrors(errors);
      } else {
        setIsLoading(true);
        const formData = new FormData();

        const model = {
          start: data.from.getTime(),
          end: data.to.getTime(),
          choices: choices.items
            .filter((obj) => Object.keys(obj.value).length !== 0)
            .map((item) => ({
              ...item,
              value: !hackathonView ? `#${item.value}` : item.value,
              prizeTracks: item.prizeTracks ? formatPrizeTracks(item.prizeTracks) : null,
              links: item.links ? item.links?.split(',') : null,
              members: item.members ? item.members?.split(',') : null
            })),
          name: data.question,
          body: data.answer,
          metadata: {},
          snapshot: networksSnapshots,
          sign: { ...tabTag, type: 'proposal' },
          winnerBox,
          whitelist: prepareObjToArray(whitelist),
          blacklist: prepareObjToArray(blacklist),
          nftUsersOnly,
          acceptableChains,
          dynamicStrategyParams: prepareDynamicStrategyParam(dynamicStrategyParams),
          basedOnTask: data.basedOnTask
        };

        const msgString = JSON.stringify({
          payload: model,
          space: space.key,
          type: 'proposal',
          timestamp: Date.now(),
          strategy: strategy.value
        });

        const requestData = {
          msg: msgString,
          address,
          isDraft,
          hideVotes,
          hackathonView
        };

        formData.append('data', JSON.stringify(requestData));

        await axios.post(`/api/proposals?address=${address}`, formData, {
          headers: { 'Content-Type': 'multipart/form-data' }
        });

        router.push(`/${space.key}`);
      }
    } catch (error) {
      console.error('Create new proposal error: ' + error);
    } finally {
      setIsLoading(false);
    }
  };

  const openNewProposalTagModal = () => setIsNewProposalTagModalOpen(true);
  const openVoterWhiteBlacklist = (modalName) => {
    setIsVoterListModalOpened(modalName);
  };
  const onNewProposalTagSave = ({ name, color }) => {
    const newProposalTag = {
      label: name.toUpperCase(),
      color: color,
      className: styles.option
    };
    tagOptions.push(newProposalTag);
    setTabTag(newProposalTag);
    setIsNewProposalTagModalOpen(false);
  };

  const openStrategyParamsModal = () => {
    setIsStrategyParamsModalOpen(true);
  };

  const handleWhitelistSave = (values) => setWhitelist(values);

  const handleBlacklistSave = (values) => setBlacklist(values);

  const onWhiteBlackListClick = (listKind) => {
    const whiteBlackListErrors = {};
    if (strategy) {
      whiteBlackListErrors.whiteBlackList = '';
      openVoterWhiteBlacklist(listKind);
    } else {
      whiteBlackListErrors.whiteBlackList = 'Choose strategy first';
    }
    setErrors({ ...errors, ...whiteBlackListErrors });
  };

  const isVotingRestrictionAdded = (list) =>
    Object.values(list).length &&
    Object.values(list)
      .map((item) => item.length)
      .reduce((prev, current) => prev + current);

  const networksJSONArray = Object.values(networksJSON);

  const handleAcceptableChains = async (event) => {
    const chainId = parseInt(event.target.value);
    let currentAcceptableChains = [...acceptableChains];
    const indexOfChain = currentAcceptableChains.indexOf(chainId);

    if (indexOfChain === -1) currentAcceptableChains = [...currentAcceptableChains, chainId];
    else currentAcceptableChains.splice(indexOfChain, 1);

    setAcceptableChains(currentAcceptableChains);
    await calculateSnapshots(currentAcceptableChains);
  };

  const generateNetworkCheckerContent = (chainId) => {
    return (
      <div className={styles.chainContainer} key={`chain-checkbox-${chainId}`}>
        <div className={styles.chain}>
          <input
            value={chainId}
            className={classNames(styles.label)}
            type="checkbox"
            defaultChecked={true}
            onChange={handleAcceptableChains}
          />
          {networksJSONArray.find((chain) => chain.chainId === parseInt(chainId)).name}
        </div>
        <div className={styles.availSnapshot}>
          <span>Snapshot: </span>
          {networksSnapshots[chainId] ?? '-'}
        </div>
      </div>
    );
  };

  const handleFileUpload = (event) => {
    if (!hackathonView) {
      const callback = ({ parsedData, error }) => {
        if (error) {
          setErrors({ ...errors, csv: error });
        } else {
          let counter = overwriteChoices ? 0 : choiceCounter.counter;
          let newItems = [];

          for (let choice of parsedData) {
            const { name, description } = choice;

            counter++;
            newItems.push({
              id: counter,
              value: name,
              description
            });
          }

          if (newItems.length) {
            const items = overwriteChoices ? newItems : choices.items.concat(newItems);
            setChoiceCounter({ counter });
            setChoices({ items });
          }
        }
      };

      parseFile(event.target, 'choices', 'CSV', callback, includeHeaders, false);
    } else {
      Papa.parse(event.target.files?.[0], {
        skipEmptyLines: true,
        complete: function (result) {
          const projects = result.data;
          let parsedChoices = [];
          const startIndex = includeHeaders ? 1 : 0;
          let counter = overwriteChoices ? 0 : choiceCounter.counter;
          try {
            for (let i = startIndex; i < projects.length; i++) {
              const [
                projectName,
                imageLink,
                tagline,
                description,
                links,
                members,
                url,
                prizeTracks
              ] = projects[i];

              counter++;
              parsedChoices.push({
                id: counter,
                value: projectName,
                info: tagline,
                description,
                links,
                imageLink,
                members,
                url,
                prizeTracks
              });
            }
            if (parsedChoices.length) {
              const items = overwriteChoices ? parsedChoices : choices.items.concat(parsedChoices);

              setChoiceCounter({ counter });
              setChoices({ items });
            }
          } catch (error) {
            console.log('Error while parsing CSV file.', error);
          }
        }
      });
    }
    event.target.value = null;
  };

  return (
    <>
      <StrategyParamsModal
        strategyInfo={strategyInfo}
        dynamicStrategyParams={dynamicStrategyParams}
        isOpen={isStrategyParamsModalOpen}
        acceptableChains={acceptableChains}
        onClose={() => setIsStrategyParamsModalOpen(false)}
        setDynamicStrategyParams={setDynamicStrategyParams}
      />
      <NewTagModal
        isOpen={isNewProposalTagModalOpen}
        tagOptions={tagOptions}
        onSave={onNewProposalTagSave}
        onClose={() => setIsNewProposalTagModalOpen(false)}
      />
      <VoterListModal
        networks={acceptableChains}
        option={isVoterListModalOpened}
        isOpen={isVoterListModalOpened !== false}
        blacklist={blacklist}
        whitelist={whitelist}
        blacklistSave={handleBlacklistSave}
        whitelistSave={handleWhitelistSave}
        onClose={() => setIsVoterListModalOpened(false)}
        isTokenWeighted={strategy?.label === 'Token weighted'}
      />
      {isLoading || isSnapshotLoading ? (
        <PageLoading />
      ) : (
        <Container>
          <div className={styles.container}>
            <div className={styles.row}>
              <div className={styles.description}>
                <div className={styles.box}>
                  <input
                    name="question"
                    maxLength={128}
                    className={classNames(styles.input, styles.question)}
                    placeholder="Question"
                    onChange={(e) => handleChange(e.target.name, e.target.value)}
                    value={data.question}
                  />
                  {errors.question && (
                    <p style={{ color: 'red', fontSize: 20 }}>{errors.question}</p>
                  )}
                  <textarea
                    name="answer"
                    className={styles.input}
                    placeholder="What is your proposal?"
                    onChange={(e) => handleChange(e.target.name, e.target.value)}
                    value={data.answer}
                  />
                  {errors.answer && <p style={{ color: 'red', fontSize: 20 }}>{errors.answer}</p>}

                  {data.answer.length > 0 && (
                    <div>
                      <h4 className={styles.preview}>Preview</h4>
                      <div className={classNames(styles.markdown, 'markdown-body', 'break-word')}>
                        <Markdown className="mb-6" body={data.answer} />
                      </div>
                    </div>
                  )}
                </div>
              </div>

              <div className={styles.actions}>
                <Block title="Actions">
                  <div className={styles.detail}>
                    {/* <div className={styles.item}>
                      <span className={styles.label}>Badge:</span>
                      <span className={styles.info}>
                        <span className="tooltipped tooltipped-n">
                          <label className={styles.fileInput}>
                            <p className={styles.fileInputLabel}>
                              {badgeFileName ?? 'Select badge image'}
                            </p>
                            <input
                              name="image"
                              className={styles.input}
                              onChange={(e) => badgeHandle(e.target.files[0])}
                              type="file"
                            />
                          </label>
                          {errors.badge && (
                            <p style={{ color: 'red', fontSize: 18 }}>{errors.badge}</p>
                          )}
                        </span>
                      </span>
                    </div> */}
                    <div className={styles.item}>
                      <span className={styles.label}>Start Date:</span>
                      <span className={styles.info}>
                        <span className="tooltipped tooltipped-n">
                          <DateTimeSelectorModal
                            dataTest="proposalStartDateSelector"
                            onSelected={(date) => {
                              handleChange('from', date);
                            }}
                            defaultText="Select start date"
                            dateValue={data.from}
                          />
                          {errors.from && (
                            <p style={{ color: 'red', fontSize: 18 }}>{errors.from}</p>
                          )}
                        </span>
                      </span>
                    </div>
                    <div className={styles.item}>
                      <span className={styles.label}>End Date:</span>
                      <span className={styles.info}>
                        <span className="tooltipped tooltipped-n">
                          <DateTimeSelectorModal
                            dataTest="proposalEndDateSelector"
                            onSelected={(date) => {
                              handleChange('to', date);
                            }}
                            defaultText="Select end date"
                            minDate={getMinimumEndDate(data.from)}
                            dateValue={data.to}
                          />
                          {errors.to && <p style={{ color: 'red', fontSize: 18 }}>{errors.to}</p>}
                        </span>
                      </span>
                    </div>
                    <div className={styles.item}>
                      <span className={styles.label}>Strategy:</span>
                      <span className={styles.info} data-test="proposalStrategyDropdown">
                        <Dropdown
                          className={styles.dropdown}
                          controlClassName={styles.control}
                          placeholderClassName={styles.placeholder}
                          menuClassName={styles.menu}
                          options={strategiesDropOptions}
                          placeholder="Strategy"
                          value={strategy}
                          onChange={strategyHandle}
                          arrowClosed={<span className={styles.arrowClosed} />}
                          arrowOpen={<span className={styles.arrowOpen} />}
                        />
                        {errors.strategy ? (
                          <p style={{ color: 'red', fontSize: 18 }}>{errors.strategy}</p>
                        ) : null}
                      </span>
                    </div>
                    {strategy && strategyInfo?.key !== 'combined-chains' && (
                      <div className={styles.item}>
                        <span className={styles.label}>Strategy params:</span>
                        <span className={styles.info} data-test="proposalStrategyParamsButton">
                          <Button
                            className={classNames(styles.button, styles.buttonAdded)}
                            onClick={openStrategyParamsModal}>
                            Advanced options
                          </Button>
                        </span>
                      </div>
                    )}
                    <div className={styles.item}>
                      <span className={styles.label} style={{ marginBottom: 21 }}>
                        Tag:
                      </span>
                      <span className={styles.info} data-test="proposalTagDropdown">
                        <Dropdown
                          className={styles.dropdown}
                          controlClassName={styles.control}
                          placeholderClassName={styles.placeholder}
                          menuClassName={styles.menu}
                          options={tagOptions}
                          placeholder="Tag"
                          value={tabTag?.label}
                          onChange={tagHandle}
                          arrowClosed={<span className={styles.arrowClosed} />}
                          arrowOpen={<span className={styles.arrowOpen} />}
                        />
                        {errors.tabTag ? (
                          <p style={{ color: 'red', fontSize: 18 }}>{errors.tabTag}</p>
                        ) : null}
                        <button onClick={openNewProposalTagModal} className={styles.newTagButton}>
                          Add new tag
                        </button>
                      </span>
                    </div>
                    {/* <div className={styles.item}>
                      <span className={styles.label}>Snapshot:</span>
                      <span className={styles.info}>
                        <span>{networksSnapshots[acceptableChains]}</span>
                      </span>
                    </div> */}
                    <div className={styles.item}>
                      <span className={styles.label}>Acceptable chains:</span>
                      <div className={styles.info}>
                        {availableChains.map((chainId) => {
                          return generateNetworkCheckerContent(chainId);
                        })}
                        {errors.acceptableChains ? (
                          <p style={{ color: 'red', fontSize: 18 }}>{errors.acceptableChains}</p>
                        ) : null}
                      </div>
                    </div>
                    <div className={styles.toggle}>
                      Winnerbox?
                      <input
                        className={styles.switch}
                        name="winnerbox"
                        type="checkbox"
                        label="winnerbox?"
                        defaultChecked={winnerBox}
                        onChange={() => setWinnerBox(!winnerBox)}
                      />
                      <span className={styles.slider}></span>
                    </div>
                    <div className={styles.toggle}>
                      Viewing restriction
                      <input
                        className={styles.switch}
                        name="nftUsersOnly"
                        type="checkbox"
                        label="Viewing restriction"
                        defaultChecked={nftUsersOnly}
                        onChange={() => setNftUsersOnly(!nftUsersOnly)}
                      />
                      <span className={styles.slider}></span>
                    </div>
                    {strategyInfo?.key !== 'combined-chains' && (
                      <div className={styles.item}>
                        <span className={styles.votersLabel}>Voting restrictions:</span>
                        <div className={styles.votersList}>
                          <div className={styles.listContainer}>
                            <Button
                              className={
                                isVotingRestrictionAdded(whitelist)
                                  ? classNames(styles.button, styles.buttonAdded)
                                  : styles.button
                              }
                              data-value="whitelist"
                              onClick={() => onWhiteBlackListClick('whitelist')}>
                              Whitelist
                            </Button>
                            <Button
                              className={
                                isVotingRestrictionAdded(blacklist)
                                  ? classNames(styles.button, styles.buttonAdded)
                                  : styles.button
                              }
                              data-value="blacklist"
                              onClick={() => onWhiteBlackListClick('blacklist')}>
                              Blacklist
                            </Button>
                          </div>
                          {errors.whiteBlackList ? (
                            <p style={{ color: 'red', fontSize: 18 }}>{errors.whiteBlackList}</p>
                          ) : null}
                        </div>
                      </div>
                    )}
                    <div className={styles.toggle}>
                      Hide votes?
                      <input
                        className={styles.switch}
                        name="votes"
                        type="checkbox"
                        label="Hide votes?"
                        defaultChecked={hideVotes}
                        onChange={() => setHideVotes(!hideVotes)}
                      />
                      <span className={styles.slider}></span>
                    </div>
                    <div className={styles.toggle}>
                      Draft?
                      <input
                        className={styles.switch}
                        name="Draft"
                        type="checkbox"
                        label="Draft?"
                        defaultChecked={isDraft}
                        onChange={() => setIsDraft(!isDraft)}
                      />
                      <span className={styles.slider}></span>
                    </div>
                    <div className={styles.toggle}>
                      Hackaton view
                      <input
                        className={styles.switch}
                        name="hackatonView"
                        type="checkbox"
                        label="Hackaton view"
                        defaultChecked={hackathonView}
                        onChange={() => setHackatonView(!hackathonView)}
                      />
                      <span className={styles.slider}></span>
                    </div>
                    <div className={styles.item}>
                      <Button
                        dataTest="proposalPublishButton"
                        onClick={() => saveChoice()}
                        className={styles.button}>
                        Publish
                      </Button>
                    </div>
                  </div>
                </Block>
              </div>
            </div>
            {!['Binary voting', 'TW Binary Voting', 'Aggregated Identity Binary Voting'].includes(
              strategy?.label
            ) ? (
              <div className={styles.row}>
                <div className={styles.choices}>
                  <Block title="Choices">
                    <div className={styles.list}>
                      {choices.items.map((c, i) => {
                        return (
                          <div className={styles.choiceRow} key={i}>
                            <div className={styles.indexColumn}>{c.id}</div>
                            <div className={styles.inputColumn}>
                              <div className={styles.default}>
                                <div className={classNames(styles.item, styles.name)}>
                                  <span className={styles.hash}>#</span>
                                  <input
                                    placeholder="Name"
                                    value={c.value}
                                    className="input"
                                    onChange={(evt) => updateChoice(c.id, evt, 'value')}
                                    disabled={submissionsData?.length}
                                  />
                                </div>
                                <div className={styles.item}>
                                  <textarea
                                    placeholder="Description"
                                    value={c.description}
                                    type="text"
                                    data-role="descriptioninput"
                                    onChange={(evt) => updateChoice(c.id, evt, 'description')}
                                    className="input"
                                    disabled={submissionsData?.length}
                                  />
                                </div>
                              </div>
                              {hackathonView ? (
                                <>
                                  <div className={styles.item}>
                                    <textarea
                                      placeholder="Info"
                                      value={c.info}
                                      type="text"
                                      data-role="infoinput"
                                      onChange={(evt) => updateChoice(c.id, evt, 'info')}
                                      className="input"
                                    />
                                  </div>
                                  <div className={styles.item}>
                                    <textarea
                                      placeholder="Links separated by commas"
                                      value={c.links}
                                      type="text"
                                      data-role="linksinput"
                                      onChange={(evt) => updateChoice(c.id, evt, 'links')}
                                      className="input"
                                    />
                                  </div>
                                  <div className={styles.item}>
                                    <textarea
                                      placeholder="Image Link"
                                      value={c.imageLink}
                                      type="text"
                                      data-role="imageLinkinput"
                                      onChange={(evt) => updateChoice(c.id, evt, 'imageLink')}
                                      className="input"
                                    />
                                  </div>
                                  <div className={styles.item}>
                                    <textarea
                                      placeholder="Members separated by commas"
                                      value={c.members}
                                      type="text"
                                      data-role="membersinput"
                                      onChange={(evt) => updateChoice(c.id, evt, 'members')}
                                      className="input"
                                    />
                                  </div>
                                  <div className={styles.item}>
                                    <textarea
                                      placeholder="URL"
                                      value={c.url}
                                      type="text"
                                      data-role="urlinput"
                                      onChange={(evt) => updateChoice(c.id, evt, 'url')}
                                      className="input"
                                    />
                                  </div>
                                  <div className={styles.item}>
                                    <textarea
                                      placeholder="Prize Tracks separated by commas"
                                      value={c.prizeTracks}
                                      type="text"
                                      data-role="prizeTracksinput"
                                      onChange={(evt) => updateChoice(c.id, evt, 'prizeTracks')}
                                      className="input"
                                    />
                                  </div>
                                </>
                              ) : null}
                            </div>
                            <div className={styles.removeColumn}>
                              {!submissionsData?.length ? (
                                <div className={styles.item}>
                                  <Icon name="close" size="22" onClick={() => deleteChoice(c.id)} />
                                </div>
                              ) : null}
                            </div>
                          </div>
                        );
                      })}
                      {errors.choiceValues && !errors.choiceCounter ? (
                        <p style={{ color: 'red', fontSize: 18 }}>{errors.choiceValues}</p>
                      ) : null}
                      {errors.choiceTags && !errors.choiceCounter ? (
                        <p style={{ color: 'red', fontSize: 18 }}>{errors.choiceTags}</p>
                      ) : null}
                      {errors.duplicated && !errors.choiceCounter ? (
                        <p style={{ color: 'red', fontSize: 18 }}>{errors.duplicated}</p>
                      ) : null}
                      {errors.choiceCounter ? (
                        <p style={{ color: 'red', fontSize: 18 }}>{errors.choiceCounter}</p>
                      ) : null}
                    </div>
                    {submissionsData?.length ? null : (
                      <>
                        <label className={styles.file}>
                          {`Upload CSV file: `}
                          <input
                            name="holders"
                            className=""
                            onChange={handleFileUpload}
                            type="file"
                            accept=".csv"
                            placeholder="Insert your CSV here"
                          />
                        </label>
                        <div className={styles.actionsColumn}>
                          <label className={styles.checkbox}>
                            <input
                              name="overwriteChoices"
                              type="checkbox"
                              value={overwriteChoices}
                              defaultChecked={true}
                              onChange={() => setOverwriteChoices(!overwriteChoices)}
                            />
                            Overwrite current choices with a CSV file
                          </label>
                          <label className={styles.checkbox}>
                            <input
                              name="includeHeaders"
                              type="checkbox"
                              value={includeHeaders}
                              onChange={() => setIncludeHeaders(!includeHeaders)}
                            />
                            Include headers
                          </label>
                          {includeHeaders && (
                            <p className={styles.warning}>
                              Be careful, first line of data are headers and won&apos;t be included
                            </p>
                          )}
                          {errors.csv ? (
                            <p style={{ color: 'red', fontSize: 18 }}>{errors.csv}</p>
                          ) : null}
                        </div>
                        <Button
                          className={styles.button}
                          data-test="proposalAddChoiceButton"
                          onClick={handleAddChoice}>
                          Add choice
                        </Button>
                      </>
                    )}
                  </Block>
                </div>
              </div>
            ) : null}
          </div>
        </Container>
      )}
    </>
  );
};

export default NewProposalPage;
