import ProposalDetails from 'components/Proposal/Details';
import ProposalTabs from 'components/Proposal/ProposalTabs/ProposalTabs.component';
import TabBar from 'components/Proposal/TabBar';
import DetailsBlock from 'components/Spaces/Influence/Blocks/Detail';
import Container from 'components/Ui/Container';
import Icon from 'components/Ui/Icon';
import PageLoading from 'components/Ui/PageLoading';
import commonConfig from 'configs/pages.json';
import { ethInstance } from 'evm-chain-scripts';
import {
  proposalStateCleanup,
  setGlobalState,
  useGlobalState,
  useProposalState
} from 'globalStateStore';
import {
  useCalculateResults,
  useFetchProposalVotesData,
  useGetProposalData,
  useSortVotesList
} from 'helpers/queries';
import { STATUSES, getProposalStatus } from 'helpers/statuses';
import { useRouter } from 'next/router';
import { useEffect, useRef, useState } from 'react';
import { ToastContainer } from 'react-toastify';
import styles from './Proposal.module.scss';

const {
  common: { proposalsSigns }
} = commonConfig;

const Proposal = ({ key, id }) => {
  const router = useRouter();
  const [address] = useGlobalState('address');
  const [space] = useGlobalState('space');
  const [currentNetwork] = useGlobalState('currentNetwork');
  const [initData, setInitData] = useProposalState('initData');
  const [loading, setLoading] = useProposalState('loading');
  const [tab, setTab] = useState('myVote');
  const [isPageLoading, setIsPageLoading] = useProposalState('isPageLoading');
  const { payload, signData, isDraft } = initData;
  const signWindowOpenRef = useRef();
  const showMyVoteTab = initData.hasMyVotesVoted || initData.isOpenVote;
  const tabChange = (tab) => setTab(tab);

  const { data: proposal } = useGetProposalData(id);

  const {
    data: { votes, isProposalVisible, isCombinedChainsVoting },
    isFetching: isProposalVotesDataLoading,
    refetch: refetchProposalVotesData
  } = useFetchProposalVotesData(
    proposal,
    address,
    space.key,
    id,
    signWindowOpenRef,
    currentNetwork
  );

  const { data: sortedVotes } = useSortVotesList(votes);

  const { data: results } = useCalculateResults(
    votes,
    currentNetwork,
    proposal,
    address,
    isCombinedChainsVoting
  );
  const loadingDataWithPopup = loading || isProposalVotesDataLoading || isPageLoading;

  useEffect(() => {
    if (!proposal) {
      setLoading(false);
    } else {
      if (!votes || !sortedVotes) return;
      const showChainsDropdown = proposal.strategy?.params?.type !== 'sim-multi';
      setGlobalState('showChainsDropdown', showChainsDropdown);
      setGlobalState(
        'supportedChains',
        proposal.msg.payload.acceptableChains || space?.networks || []
      );
      const signData =
        proposalsSigns.find((propSign) => propSign.sign === proposal.msg.payload.sign) ?? '';
      const proposalStatus = getProposalStatus(
        proposal.msg.payload.start,
        proposal.msg.payload.end
      );
      const isOpenVote = proposalStatus === STATUSES.OPEN;
      const connectedNotNFT =
        address &&
        (proposal.strategy.params.type !== 'nft' || proposal.strategy.key === 'combined-chains');

      setInitData((initData) => ({
        ...initData,
        key,
        id,
        proposal,
        votes,
        results,
        isProposalVisible,
        signData,
        payload: proposal.msg.payload,
        isOpenVote,
        hackathonView: proposal.hackathonView,
        hasMyVotesVoted: connectedNotNFT ? !!votes?.[address] : false,
        hideVotes: new Date().getTime() < proposal.msg.payload.end && proposal.hideVotes,
        isDraft: proposal.isDraft
      }));
      setIsPageLoading(false);
      setLoading(false);
    }
  }, [proposal, sortedVotes, results, votes, isProposalVisible]);

  const getSpaceKey = () => {
    let key = space.key;
    if (space.parentNamespace?.key) key = `${space.parentNamespace.key}-${space.key}`;
    return key;
  };

  const accountsChangedAction = () => {
    const spaceKey = getSpaceKey();
    if (location.pathname === `/${spaceKey}/proposal/${id}`) refetchProposalVotesData();
  };

  useEffect(() => {
    try {
      ethInstance.onAccountsChanged(accountsChangedAction);
    } catch (err) {
      //
    }
  }, [address]);

  const handleBackClick = () => {
    const spaceKey = getSpaceKey();
    proposalStateCleanup();
    router.push(
      window?.location.hostname === 'vote.ethbrno.cz' ||
        window?.location.hostname === 'vote.ethprague.com'
        ? '/'
        : `/${spaceKey}`
    );
  };

  useEffect(() => {
    refetchProposalVotesData();
    return () => proposalStateCleanup();
  }, []);

  return (
    <Container>
      {isPageLoading || isProposalVotesDataLoading ? (
        <PageLoading />
      ) : (
        <>
          <div className={styles.actionsContainer}>
            <button className={styles.backButton} onClick={handleBackClick}>
              <Icon name="back" className={styles.backIcon} />
              <span style={{ marginLeft: 5 }}>{space.name}</span>
            </button>
          </div>
          <div className={styles.row}>
            <ProposalDetails payload={payload} signData={signData} isDraft={isDraft} />
            <div className={styles.details}>
              <DetailsBlock
                space={space}
                initData={initData}
                currentNetwork={currentNetwork}
                loading={loadingDataWithPopup}
              />
            </div>
          </div>
          <div className={styles.row}>
            <div className={styles.navbar}>
              <TabBar
                hackathonView={proposal.hackathonView}
                isLoading={loadingDataWithPopup}
                onItemClick={tabChange}
                selectedTab={tab}
                showMyVoteTab={showMyVoteTab}
                showTabsForUserWithNft={initData.isProposalVisible}
                hideVotesTabs={initData.hideVotes}
                counter={Object.keys(initData.votes).length || 0}
              />
            </div>
          </div>
          <ProposalTabs tab={tab} space={space} refetchProposalData={refetchProposalVotesData} />
        </>
      )}
      <ToastContainer />
    </Container>
  );
};

export default Proposal;
