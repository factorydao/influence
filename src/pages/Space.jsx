import PageLoading from 'components/Ui/PageLoading';
import { useGlobalState } from 'globalStateStore';
import dynamic from 'next/dynamic';
import SpacesPage from 'pages/spaces/Spaces';
import { useEffect, useState } from 'react';

const SpaceDetails = dynamic(() => import('./spaceDetails/SpaceDetails'));

const Space = () => {
  const [space] = useGlobalState('space');
  const [isLoading, setLoading] = useState(true);

  useEffect(() => {
    if (Object.keys(space).length) {
      setLoading(false);
    }
  }, [space]);

  const Component = space.isNamespace ? SpacesPage : SpaceDetails;

  return <>{isLoading ? <PageLoading /> : <Component />}</>;
};

export default Space;
