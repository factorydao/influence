import axios from 'axios';
import Button from 'components/Ui/Button';
import LogoLoader from 'components/Ui/LogoLoader';
import GatedMerkleIdentity from 'contracts/GatedMerkleIdentity.json';
import { createMerkleTree, ethInstance } from 'evm-chain-scripts';
import { useGlobalState } from 'globalStateStore';
import multihashes from 'multihashes';
import { useEffect, useState } from 'react';
import { Navigate, Route, Routes } from 'react-router-dom';
import styles from './AddMerkleRoot.module.scss';

function buf2hex(buffer) {
  return [...new Uint8Array(buffer)].map((x) => x.toString(16).padStart(2, '0')).join('');
}
const getBytes = (ipfsHash) => {
  const bytes = '0x' + buf2hex(multihashes.fromB58String(ipfsHash)).slice(4);
  return bytes;
};

const ActionButton = ({ children, onClick, isDisabled }) => {
  return (
    <Button onClick={onClick} className={styles.button} disabled={isDisabled}>
      {children}
    </Button>
  );
};

const warning = 'Be careful, first line of data are headers and wont be included';

const parseData = (value, inputType, headers) => {
  if (inputType === 'CSV') {
    const lines = value.split(/\r\n|\n/);
    const preparedAddressData = [];
    const preparedMetaData = [];
    const start = headers ? 1 : 0;
    for (let i = start; i < lines.length; i++) {
      const [address, uri, tokenId] = lines[i].replace(' ', '').split(',');
      preparedAddressData.push({ address });
      preparedMetaData.push({ tokenId, uri });
    }
    const preparedData = {
      addressLeaves: preparedAddressData,
      metadataLeaves: preparedMetaData
    };
    return preparedData;
  } else {
    return JSON.parse(value);
  }
};

const INPUT_TYPES = {
  CSV: {
    inputType: 'CSV',
    switchButtonText: 'Switch to JSON Array',
    placeholderText: 'Input CSV values here..'
  },
  JSON: {
    inputType: 'JSONArray',
    switchButtonText: 'Switch to CSV',
    placeholderText: 'Input JSON Array here..'
  }
};

const AddMerkleRoot = () => {
  const [space] = useGlobalState('space');
  const [isGlobalAdmin] = useGlobalState('isGlobalAdmin');
  const [address] = useGlobalState('address');
  const [value, setValue] = useState();
  const [hash, setHash] = useState();
  const [metadataHash, setMetadataHash] = useState();
  const [inputs, setInputs] = useState(INPUT_TYPES.JSON);
  const [headersIncluded, setHeadersIncluded] = useState(false);
  const [ipfsHash, setIpfsHash] = useState(null);
  const [error, setError] = useState();
  const [preparedHex, setPreparedHex] = useState();
  const [nftAddress, setNftAddress] = useState();
  const [gateAddress, setGateAddress] = useState();
  const [isLoading, setIsLoading] = useState(false);
  const [btnDisabled, setBtnDisabled] = useState(true);

  useEffect(() => {
    if (value && nftAddress && gateAddress) {
      setBtnDisabled(false);
    } else {
      setBtnDisabled(true);
    }
  }, [value, nftAddress, gateAddress]);

  const handleChange = (e) => {
    setValue(e.target.value);
  };

  const handleSubmit = async () => {
    try {
      setIsLoading(true);
      setError(null);
      setIpfsHash(null);

      const data = parseData(value, inputs.inputType, headersIncluded);

      const merkleTree = createMerkleTree(data.addressLeaves, ['address'], ['address']);
      const metadataMerkleTree = createMerkleTree(
        data.metadataLeaves,
        ['uint', 'string'],
        ['tokenId', 'uri']
      );
      setHash(merkleTree.root.hash);
      setMetadataHash(metadataMerkleTree.root.hash);
      const msg = JSON.stringify(data);
      const sig = await ethInstance.signPersonalMessage(msg, address);

      const requestBody = {
        payload: { ...data },
        sig,
        nftAddress,
        gateAddress,
        merkleTree,
        metadataMerkleTree,
        address
      };
      const result = await axios.post(`/api/spaces/${space?.key}/add-merkle`, requestBody);

      const ipfsHashValue = result.data;
      setIpfsHash(ipfsHashValue);
      const ipfsHashInBytes32 = getBytes(ipfsHashValue);
      setPreparedHex(ipfsHashInBytes32);

      const senderWalletAddress = await ethInstance.getEthAccount();
      const contract = await ethInstance.getContract('write', GatedMerkleIdentity);
      const resultFromUpload = await contract.methods
        .addMerkleTree(
          merkleTree.root.hash,
          metadataMerkleTree.root.hash,
          ipfsHashInBytes32,
          nftAddress,
          gateAddress
        )
        .send({ from: senderWalletAddress });
      console.log(resultFromUpload);
    } catch (e) {
      setError(e.message);
      console.error(e);
    } finally {
      setIsLoading(false);
    }
  };

  const handleSwitch = () => {
    if (inputs.inputType === INPUT_TYPES.CSV.inputType) {
      setInputs(INPUT_TYPES.JSON);
    } else {
      setInputs(INPUT_TYPES.CSV);
    }
  };

  return isGlobalAdmin ? (
    <div className={styles.container}>
      <h1 className={styles.title}>Create merkle tree hash</h1>
      {isLoading ? (
        <LogoLoader />
      ) : (
        <>
          <form className={styles.form}>
            <label className={styles.textarea}>
              <textarea
                placeholder={inputs.placeholderText}
                value={value}
                rows={3}
                cols={50}
                onChange={handleChange}
              />
            </label>
            <div className={styles.actions}>
              <div>
                {inputs.inputType === 'CSV' ? (
                  <label className={styles.checkbox}>
                    Headers included
                    <input
                      onChange={() => setHeadersIncluded(!headersIncluded)}
                      defaultChecked={headersIncluded}
                      type="checkbox"
                    />
                  </label>
                ) : null}
                {inputs.inputType === 'CSV' && headersIncluded ? (
                  <p className={styles.warning}>{warning}</p>
                ) : null}
              </div>
              <ActionButton onClick={handleSwitch}>{inputs.switchButtonText}</ActionButton>
            </div>

            <div>
              <h3 className={styles.inputLabel}>NFT Address</h3>
              <input
                type="text"
                placeholder={'Input NFT Address here..'}
                value={nftAddress}
                className={styles.input}
                onChange={(e) => setNftAddress(e.target.value)}
              />
              <h3 className={styles.inputLabel}>Gate address</h3>
              <input
                type="text"
                placeholder={'Input gate address here..'}
                value={gateAddress}
                className={styles.input}
                onChange={(e) => setGateAddress(e.target.value)}
              />
            </div>
            <ActionButton onClick={handleSubmit} isDisabled={btnDisabled}>
              Upload to blockchain
            </ActionButton>
          </form>

          {ipfsHash && hash && metadataHash ? (
            <div className={styles.table}>
              <h3 className={styles.rowTitle}>Generated IPFS hash: </h3> <h3>{ipfsHash}</h3>
              <h3 className={styles.rowTitle}>Addresses merkle root hash: </h3> <h3>{hash}</h3>
              <h3 className={styles.rowTitle}>Metadata merkle root hash: </h3>{' '}
              <h3>{metadataHash}</h3>
              <h3 className={styles.rowTitle}>Prepared IPFS hex: </h3>
              <h3>{preparedHex}</h3>
            </div>
          ) : null}
        </>
      )}
      {error && <p className={styles.error}>{error}</p>}
    </div>
  ) : (
    <Routes>
      <Route path="/" element={<Navigate to="/" replace />} />
    </Routes>
  );
};

export default AddMerkleRoot;
