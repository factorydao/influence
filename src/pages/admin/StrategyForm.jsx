import axios from 'axios';
import If from 'components/If';
import Addresses from 'components/Spaces/NftAddresses';
import MetadataParams from 'components/Strategy';
import { METADATA_TYPES } from 'components/Strategy/MetadataParams';
import Button from 'components/Ui/Button';
import Container from 'components/Ui/Container';
import TextArea from 'components/Ui/Formik/TextArea';
import TextInput from 'components/Ui/Formik/TextInput';
import PageLoading from 'components/Ui/PageLoading';
import { toChecksumAddress } from 'evm-chain-scripts';
import { Form, Formik } from 'formik';
import {
  buildChainIdToAddressMap,
  strategiesToModify,
  strategiesToShift,
  userFriendlyStrategyLabels
} from 'helpers/strategyForm';
import { useEffect, useState } from 'react';
import Dropdown from 'react-dropdown';
import { toast } from 'react-toastify';
import * as Yup from 'yup';
import classes from './StrategyForm.module.scss';

const isNFT = (strategy) => {
  const str = strategy
    ? strategy.includes('nft') ||
      strategy.includes('united') ||
      strategy === 'os-1155' ||
      strategy === 'combined-chains' ||
      strategy === 'markets-voice-credits'
    : false;
  return str;
};

const isSimMulti = (strategyType) => {
  return (
    strategyType === 'token-weighted-chains-sum' ||
    strategyType === 'combined-chains' ||
    strategyType === 'deposit-balance' ||
    strategyType === 'deposit-rank'
  );
};

const isTokenBased = (strategyType) => {
  return !isNFT(strategyType) || isSimMulti(strategyType);
};

const nftTypes = [
  {
    value: 'nft',
    label: 'NFT',
    className: classes.option
  },
  {
    value: 'not-nft',
    label: 'not NFT',
    className: classes.option
  }
];
const simTypes = [
  {
    value: 'sim-multi',
    label: 'SimMulti',
    className: classes.option
  }
];

const StrategyForm = () => {
  const [isLoading, setIsLoading] = useState(true);
  const [isNFTStrategy, setIsNFTStrategy] = useState(false);
  const [isTokenBasedStrategy, setIsTokenBasedStrategy] = useState(false);
  const [strategyTypes, setStrategyTypes] = useState([]);
  const [types, setTypes] = useState(nftTypes);
  const [strategies, setStrategies] = useState([]);

  useEffect(() => {
    try {
      setIsLoading(true);
      axios.get(`/api/strategies/types`).then(({ data: strategyTypesArray }) => {
        const filteredStrategyTypes = strategyTypesArray.filter(
          (strategy) => !strategiesToShift.includes(strategy)
        );

        setStrategyTypes(
          filteredStrategyTypes.map((type) => {
            return {
              value: strategiesToModify[type] || type,
              //TODO check on production which types of strategies are no longer used
              label: userFriendlyStrategyLabels[type] || type.replace(/-/g, ' '),
              className: classes.option
            };
          })
        );
      });
      axios.get('/api/strategies').then(({ data }) => {
        setStrategies(data.map((strategy) => strategy.name));
      });
    } catch (err) {
      console.log(err);
    } finally {
      setIsLoading(false);
    }
  }, []);

  const validateContractAddress = (contractAddress) => {
    let isValid = null;
    try {
      toChecksumAddress(contractAddress);
      isValid = true;
    } catch (error) {
      console.log(error);
    }
    return isValid;
  };

  const inputsValidation = Yup.object({
    name: Yup.string().max(64).notOneOf(strategies, 'This name is already used').required(),
    strategyKey: Yup.string().required(),
    binary: Yup.boolean(),
    negativeVote: Yup.boolean(),
    isSum: Yup.boolean(),
    mintingLink: Yup.string(),
    addresses: Yup.array().when('strategyKey', {
      is: (strategy) => !isNFT(strategy),
      then: Yup.array().of(
        Yup.object({
          chainId: Yup.number()
            .required('Chain id is required')
            .integer('Please input integer value')
            .positive('Please input positive value')
            .typeError('Please input number value'),
          address: Yup.string()
            .required('Address is required')
            .test('is-address-valid', 'Wrong address value', (value) =>
              validateContractAddress(value)
            )
        })
      )
    }),
    nftAddresses: Yup.array().when('strategyKey', {
      is: (strategy) => isNFT(strategy),
      then: Yup.array().of(
        Yup.object({
          chainId: Yup.number()
            .required('Chain id is required')
            .integer('Please input integer value')
            .positive('Please input positive value')
            .typeError('Please input number value'),
          address: Yup.string()
            .required('NFT address is required')
            .test('is-address-valid', 'Wrong NFT address value', (value) =>
              validateContractAddress(value)
            )
        })
      )
    }),
    symbol: Yup.string()
      .matches(/^\$[A-Za-z0-9]*$/, 'Token symbol must be string and start with $')
      .max(6, 'Please input $ and max 5 chars')
      .required(),
    decimals: Yup.number().positive().required(),
    type: Yup.string().required(),
    nftName: Yup.string().when('strategyKey', {
      is: (strategy) => isNFT(strategy),
      then: (schema) => schema.required()
    }),
    collectionName: Yup.string().matches(/^[A-Za-z]*$/, 'Please input only letters'),
    maxScore: Yup.number()
      .integer('Please input integer value')
      .when(['isSum', 'strategyKey'], {
        is: (isSum, strategy) => !isSum && isNFT(strategy),
        then: (schema) => schema.positive('Please input positive value').required()
      }),
    minBalance: Yup.number().min(0),
    divider: Yup.number().min(0),
    powerPerIdentity: Yup.number().when('isSum', {
      is: true,
      then: (schema) => schema.positive('Please input positive value'),
      otherwise: (schema) => schema.min(0)
    }),
    values: Yup.array().when('strategyKey', {
      is: 'combined-chains',
      then: (schema) => schema.required()
    }),
    depositPools: Yup.object(),
    depositMaxScore: Yup.number().when('strategyKey', {
      is: 'deposit-rank',
      then: (schema) => schema.positive().required()
    }),
    metadata: Yup.array().when('strategyKey', {
      is: (strategy) => isNFT(strategy) && strategy.includes('metadata'),
      then: Yup.array().of(
        Yup.object({
          type: Yup.string().required('Type is required'),
          key: Yup.string().required('Key is required'),
          value: Yup.string().when('type', {
            is: 'singleValue',
            then: (schema) => schema.required('Value is required')
          }),
          min: Yup.number().when('type', {
            is: 'range',
            then: (schema) => schema.required('Min is required')
          }),
          max: Yup.number().when('type', {
            is: 'range',
            then: (schema) => schema.required('Max is required')
          }),
          boolValue: Yup.boolean().when('type', {
            is: 'boolean',
            then: (schema) => schema.required('Value is required')
          })
        })
      )
    })
  });

  const handleStrategyKeyChange = (strategyKey, setFieldValue) => {
    setFieldValue('strategyKey', strategyKey);
    setFieldValue('type', '');
    setIsNFTStrategy(isNFT(strategyKey));
    setIsTokenBasedStrategy(isTokenBased(strategyKey));
    if (isSimMulti(strategyKey)) {
      strategyKey === 'token-weighted-chains-sum'
        ? setTypes(
            simTypes.concat({
              value: 'not-nft',
              label: 'not NFT',
              className: classes.option
            })
          )
        : setTypes(simTypes);
    } else {
      setTypes(nftTypes);
    }
  };

  const handleSubmit = async ({
    name,
    strategyKey,
    binary,
    negativeVote,
    isSum,
    mintingLink,
    addresses,
    nftAddresses,
    symbol,
    decimals,
    type,
    nftName,
    collectionName,
    maxScore,
    minBalance,
    divider,
    powerPerIdentity,
    values,
    depositPools,
    depositMaxScore,
    hideBalanceAndChart,
    metadata,
    logicOperator
  }) => {
    try {
      setIsLoading(true);
      const metadataObject = {};

      metadata.forEach(({ key, type, value, boolValue, min, max }) => {
        const meta = { type };

        switch (type) {
          case METADATA_TYPES.SINGLE_VALUE:
            metadataObject[key] = { ...meta, value };
            break;
          case METADATA_TYPES.BOOLEAN:
            metadataObject[key] = { ...meta, value: boolValue };
            break;
          case METADATA_TYPES.RANGE:
            metadataObject[key] = { ...meta, range: { min, max } };
            break;
        }
      });

      const reqNftAddresses = buildChainIdToAddressMap(nftAddresses, true);
      const reqAddresses = buildChainIdToAddressMap(addresses, true);

      const requestBody = {
        name: name,
        key: strategyKey,
        binary: binary,
        negativeVote: negativeVote,
        isSum: isSum,
        mintingLink: mintingLink || undefined,
        params: {
          addresses: reqAddresses || undefined,
          nftAddresses: reqNftAddresses || undefined,
          symbol: symbol,
          decimals: decimals || 1,
          type: type || undefined,
          nftName: nftName || undefined,
          collectionName: collectionName || undefined,
          maxScore: maxScore || undefined,
          minBalance: minBalance || undefined,
          divider: divider || undefined,
          powerPerIdentity: powerPerIdentity || undefined,
          values: values || undefined,
          depositPools: depositPools || undefined,
          depositMaxScore: depositMaxScore || undefined,
          hideBalanceAndChart: hideBalanceAndChart,
          metadataParams: metadata.length ? metadataObject : undefined,
          logicOperator: logicOperator ? 'and' : 'or'
        }
      };

      axios
        .post(`/api/strategies`, requestBody)
        .then(() => {
          toast.success('New strategy added', { position: toast.POSITION.TOP_CENTER });
          setStrategies([...strategies, name]);
        })
        .catch((e) => {
          console.log(e);
        });
    } catch (e) {
      toast.error(e, { position: toast.POSITION.TOP_CENTER });
      console.log(e);
    } finally {
      setIsLoading(false);
    }
  };

  const defaultNftAddresses = [
    {
      itemId: 0,
      chainId: '',
      address: '',
      errors: {}
    }
  ];

  const defaultAddresses = [
    {
      itemId: 0,
      chainId: '',
      address: '',
      errors: {}
    }
  ];

  const defaultMetadata = [
    {
      key: '',
      value: '',
      type: METADATA_TYPES.SINGLE_VALUE,
      min: '',
      max: '',
      boolValue: false
    }
  ];

  const initialValues = {
    name: '',
    strategyKey: '',
    binary: false,
    negativeVote: false,
    isSum: false,
    mintingLink: '',
    addresses: defaultAddresses,
    nftAddresses: defaultNftAddresses,
    symbol: '',
    decimals: 18,
    type: '',
    nftName: '',
    collectionName: '',
    maxScore: 0,
    minBalance: 0,
    divider: 0,
    powerPerIdentity: 0,
    values: '',
    depositPools: '',
    depositMaxScore: '',
    hideBalanceAndChart: false,
    metadata: defaultMetadata,
    logicOperator: true
  };

  return !isLoading ? (
    <Container>
      <h1>Add new strategy</h1>
      <Formik
        initialValues={initialValues}
        validationSchema={inputsValidation}
        onSubmit={handleSubmit}>
        {({ values, errors, touched, setFieldValue }) => (
          <Form className={classes.box}>
            <TextInput
              className={classes.input}
              label="Name"
              name="name"
              type="text"
              placeholder="Please enter strategy name"
              error={errors.name}
            />

            <label className={classes.label} htmlFor="strategyKey">
              Strategy type
            </label>
            <Dropdown
              id="strategyKey"
              className={classes.dropdown}
              controlClassName={classes.control}
              placeholderClassName={classes.placeholder}
              menuClassName={classes.menu}
              options={strategyTypes}
              placeholder="Strategy type"
              value={values.strategyKey}
              onChange={({ value }) => {
                handleStrategyKeyChange(value, setFieldValue);
              }}
              arrowClosed={<span className={classes.arrowClosed} />}
              arrowOpen={<span className={classes.arrowOpen} />}
            />
            {touched.strategyKey && errors.strategyKey ? (
              <span style={{ color: 'red', fontSize: 20, marginLeft: '12px', paddingTop: '4px' }}>
                {errors.strategyKey}
              </span>
            ) : null}

            <div className={classes.toggle}>
              <TextInput className={classes.switch} name="binary" type="checkbox" label="Binary" />
              <span className={classes.slider}></span>
            </div>

            <If condition={!values.binary}>
              <div className={classes.toggle}>
                <TextInput
                  className={classes.switch}
                  name="negativeVote"
                  type="checkbox"
                  label="Negative vote"
                />
                <span className={classes.slider}></span>
              </div>
            </If>

            <If condition={isNFTStrategy}>
              <div className={classes.toggle}>
                <TextInput className={classes.switch} name="isSum" type="checkbox" label="Is sum" />
                <span className={classes.slider}></span>
              </div>
            </If>

            <TextInput
              className={classes.input}
              label="Minting link"
              name="mintingLink"
              type="text"
              placeholder="Please enter minting link"
              error={errors.mintingLink}
            />
            <If condition={!isNFT(values.strategyKey)}>
              <label className={classes.label} htmlFor="addresses">
                Addresses
              </label>
              <Addresses
                id="addresses"
                name="addresses"
                chainValues={values.addresses}
                setFieldValue={setFieldValue}
                errors={errors.addresses}
                touched={touched}
                isNewStrategy
                buttonLabel="Add next address"
              />
            </If>
            <If condition={isNFT(values.strategyKey)}>
              <label className={classes.label} htmlFor="nftAddresses">
                NFT Addresses
              </label>
              <Addresses
                id="nftAddresses"
                name="nftAddresses"
                chainValues={values.nftAddresses}
                setFieldValue={setFieldValue}
                errors={errors.nftAddresses}
                touched={touched}
                isNewStrategy
                buttonLabel="Add next nft address"
              />
            </If>

            <TextInput
              className={classes.input}
              label="Symbol"
              name="symbol"
              type="text"
              placeholder="Please enter symbol"
              error={errors.symbol}
            />

            <TextInput
              className={classes.input}
              label="Decimals"
              name="decimals"
              type="number"
              placeholder="Input decimals value here"
              error={errors.decimals}
            />

            <label className={classes.label} htmlFor="type">
              Type
            </label>
            <Dropdown
              id="type"
              className={classes.dropdown}
              controlClassName={classes.control}
              placeholderClassName={classes.placeholder}
              menuClassName={classes.menu}
              options={types}
              placeholder="Type"
              value={values.type}
              onChange={({ value }) => setFieldValue('type', value)}
              arrowClosed={<span className={classes.arrowClosed} />}
              arrowOpen={<span className={classes.arrowOpen} />}
            />
            {touched.type && errors.type ? (
              <span style={{ color: 'red', fontSize: 20, marginLeft: '12px', paddingTop: '4px' }}>
                {errors.type}
              </span>
            ) : null}

            <If condition={isNFTStrategy}>
              <TextInput
                className={classes.input}
                label="NFT name"
                name="nftName"
                type="text"
                placeholder="Please enter NFT name"
                error={errors.nftName}
              />
            </If>

            <If condition={isNFTStrategy}>
              <TextInput
                className={classes.input}
                label="Collection name"
                name="collectionName"
                type="text"
                placeholder="Please enter collection name"
                error={errors.collectionName}
              />
            </If>

            <If condition={isNFTStrategy && !values.isSum}>
              <TextInput
                className={classes.input}
                label="Maximum score"
                name="maxScore"
                type="number"
                placeholder="Input max score value here"
                error={errors.maxScore}
              />
            </If>

            <If condition={values.strategyKey === 'ethereum-vote-power'}>
              <TextInput
                className={classes.input}
                label="Minimal balance"
                name="minBalance"
                type="number"
                placeholder="Input min balance value here"
                error={errors.minBalance}
              />
            </If>

            <If condition={isTokenBasedStrategy}>
              <TextInput
                className={classes.input}
                label="Divider"
                name="divider"
                type="number"
                placeholder="Input divider value here"
                error={errors.divider}
              />
            </If>

            <If condition={isNFTStrategy && values.isSum}>
              <TextInput
                className={classes.input}
                label="Power per identity"
                name="powerPerIdentity"
                type="number"
                placeholder="Input power per identity value here"
                error={errors.powerPerIdentity}
              />
            </If>

            <If condition={values.strategyKey === 'combined-chains'}>
              <TextArea
                className={classes.textarea}
                label="Values"
                name="values"
                value=""
                type="text"
                placeholder="Please insert here JSON Array of values objects"
                error={errors.values}
              />
            </If>

            <If
              condition={
                values.strategyKey === 'deposit-balance' || values.strategyKey === 'deposit-rank'
              }>
              <TextArea
                className={classes.textarea}
                label="Deposit pools"
                name="depositPools"
                value=""
                type="text"
                placeholder="Please insert here JSON object of deposit pools"
                error={errors.depositPools}
              />
            </If>

            <If condition={values.strategyKey === 'deposit-rank'}>
              <TextInput
                className={classes.input}
                label="Deposit maximum score"
                name="depositMaxScore"
                type="text"
                placeholder="Input deposit max score value here"
                error={errors.depositMaxScore}
              />
            </If>
            <If condition={values.strategyKey.includes('nft-metadata')}>
              <label className={classes.label}>Metadata params</label>
              <MetadataParams metadata={values.metadata} errors={errors.metadata} />
              <div className={classes.toggle}>
                <TextInput
                  className={classes.switch}
                  name="logicOperator"
                  type="checkbox"
                  label="Required all metadata"
                />
                <span className={classes.slider}></span>
              </div>
            </If>
            <div className={classes.toggle}>
              <TextInput
                className={classes.switch}
                name="hideBalanceAndChart"
                type="checkbox"
                label="Hide balance and chart"
              />
              <span className={classes.slider}></span>
            </div>

            <Button className={classes.button} type="submit">
              Add strategy
            </Button>
          </Form>
        )}
      </Formik>
    </Container>
  ) : (
    <PageLoading />
  );
};

export default StrategyForm;
