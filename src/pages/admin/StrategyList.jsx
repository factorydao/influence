import { JsonViewer } from '@textea/json-viewer';
import axios from 'axios';
import Container from 'components/Ui/Container';
import PageLoading from 'components/Ui/PageLoading';
import { useEffect, useState } from 'react';
import {
  Accordion,
  AccordionItem,
  AccordionItemButton,
  AccordionItemHeading,
  AccordionItemPanel
} from 'react-accessible-accordion';
import classes from './StrategyList.module.scss';

const StrategyList = () => {
  const [isLoading, setIsLoading] = useState(true);
  const [strategies, setStrategies] = useState([]);

  useEffect(() => {
    axios
      .get('/api/strategies')
      .then(({ data }) => {
        setStrategies(data);
        setIsLoading(false);
      })
      .catch((e) => {
        console.log(e);
      });
  }, []);

  return !isLoading ? (
    <Container>
      <h1>Strategies</h1>
      <Accordion allowZeroExpanded className={classes.accordion}>
        {strategies.map((strategy) => (
          <AccordionItem key={strategy._id} className={classes.accordion__item}>
            <AccordionItemHeading>
              <AccordionItemButton className={classes.accordion__button}>
                {strategy.name}
              </AccordionItemButton>
            </AccordionItemHeading>
            <AccordionItemPanel className={classes.accordion__panel}>
              <JsonViewer value={strategy} />
            </AccordionItemPanel>
          </AccordionItem>
        ))}
      </Accordion>
    </Container>
  ) : (
    <PageLoading />
  );
};

export default StrategyList;
