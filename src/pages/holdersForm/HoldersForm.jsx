import axios from 'axios';
import Button from 'components/Ui/Button';
import Container from 'components/Ui/Container';
import PageLoading from 'components/Ui/PageLoading';
import { Field, Form, Formik } from 'formik';
import { useState } from 'react';
import { toast } from 'react-toastify';
import styles from './HoldersForm.module.scss';

const validate = ({ address }) => {
  const errors = {};
  if (!address) {
    errors.address = 'Address is required!';
  }
  return errors;
};

const HoldersForm = () => {
  const [holderAddresses, setHolderAddresses] = useState([]);
  const [isSubimtted, setIsSubimtted] = useState(false);
  const [isLoading, setIsLoading] = useState(false);

  const submit = async (values, { setSubmitting, resetForm }) => {
    try {
      setIsLoading(true);
      resetForm();
      const requestParams = {
        module: 'account',
        action: 'txlist',
        address: values.address,
        apikey: 'PSCTUWK4E9MPGDAEZTB87JQSQC44UYA3ZA'
      };

      const response = await axios.get('https://api.etherscan.io/api', { params: requestParams });

      if (response.data.message === 'NOTOK') {
        toast.error(response.data.result, { position: 'top-center' });
        setHolderAddresses([]);
      } else {
        const addressesList = response.data.result.map((r) => r.to);
        setHolderAddresses(addressesList);
      }
      setIsSubimtted(true);
    } catch (e) {
      console.error(e);
    } finally {
      setSubmitting(false);
      setIsLoading(false);
    }
  };

  return isLoading ? (
    <PageLoading />
  ) : (
    <Container>
      <h1 className={styles.title}>Get holders addresses</h1>
      <Formik
        initialValues={{
          address: ''
        }}
        validate={validate}
        onSubmit={submit}>
        {({ errors }) => (
          <Form className={styles.form}>
            <label className={styles.label}>
              Address
              <Field
                className={styles.input}
                label="Address"
                name="address"
                type="text"
                placeholder="Please enter address"
              />
              {errors.address ? <span className={styles.error}>{errors.address}</span> : null}
            </label>
            <Button disabled={errors.address} className={styles.button} type="submit">
              Send
            </Button>
          </Form>
        )}
      </Formik>
      <div className={styles.list}>
        {holderAddresses.length > 0 ? (
          <>
            <h2>Holders addresses list:</h2>
            {holderAddresses.map((address, index) => (
              <span key={index}>{address}</span>
            ))}
          </>
        ) : (
          isSubimtted && <p>There aren&apos;t any holders addresses here yet!</p>
        )}
      </div>
    </Container>
  );
};

export default HoldersForm;
