import axios from 'axios';
import classNames from 'classnames';
import NewTagModal from 'components/Modal/ProposalTag/ProposalTag';
import Block from 'components/Ui/Block';
import Button from 'components/Ui/Button';
import Container from 'components/Ui/Container';
import DateTimeSelectorModal from 'components/Ui/DateTimeSelector/DateTimeSelectorModal';
import Markdown from 'components/Ui/Markdown';
import PageLoading from 'components/Ui/PageLoading';
import { useGlobalState } from 'globalStateStore';
import { getSignOptions } from 'helpers/lists';
import { getSnapshotPerNetwork } from 'helpers/utils';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';
import Dropdown from 'react-dropdown';
import 'react-dropdown/style.css';
import { toast } from 'react-toastify';
import stylesNewProposal from '../NewProposal.module.scss';
import styles from './NewQuestion.module.scss';

const typeOptions = [
  { value: 'text', label: 'text', className: styles.option },
  { value: 'media', label: 'media', className: styles.option },
  { value: 'mixed', label: 'mixed', className: styles.option }
];

/**
 *
 * Validate form
 *
 * @param {Object} values - object to validate
 * @returns errors object
 */
const validate = (values, signOption) => {
  let errors = {};
  if (!values.title || values.title.trim() === '') {
    errors.title = 'Title is required';
  } else if (values.title.length > 32) {
    errors.title = 'Title is too long';
  }
  if (!values.question || values.question.trim() === '') errors.question = 'Answer is required';
  else if (values.question.length > 4e4)
    errors.question = 'Answer is too long, must be less than 40 000 characters';

  if (!values.from) errors.from = 'Start date is required';
  if (!values.to) errors.to = 'End date is required';
  if (!values.upDownEndDate) errors.upDownEndDate = 'Up/down vote end date is required';

  if (values.to && values.to <= Date.now()) errors.to = 'End date cannot be set in the past';

  if (values.from && values.to && values.upDownEndDate) {
    if (Date.parse(values.from) >= Date.parse(values.to)) {
      errors.to = 'End date should be later than start date';
    }
    if (Date.parse(values.upDownEndDate) <= Date.parse(values.to))
      errors.upDownEndDate = 'Up/down vote end date should be later than task end date';
  }
  if (!values.idHolder && !values.tokenHolder) {
    errors.noHolders = 'At least one checkbox have to be checked';
  }

  if (Number(values.maxLength) < 0) {
    errors.maxLength = 'Submission max length must be positive numbers';
  }

  if (Number(values.submissionsPerId) < 0 || Number(values.submissionsPerAddress) < 0) {
    errors.negativeNumbers = 'Must be positive numbers';
  }
  if (!signOption) {
    errors.signOption = 'Choose a tag';
  }
  return errors;
};

/**
 *
 * New question page
 *
 * @returns The form for create a new question
 */
const NewQuestion = () => {
  const router = useRouter();
  const [space] = useGlobalState('space');
  const [address] = useGlobalState('address');
  const [isMember] = useGlobalState('isMember');
  const [currentNetwork] = useGlobalState('currentNetwork');

  const [data, setData] = useState({
    question: '',
    answer: '',
    from: null,
    to: null,
    upDownEndDate: null,
    maxLength: 10000,
    idHolder: false,
    submissionsPerId: 1,
    tokenHolder: false,
    submissionsPerAddress: 1,
    type: typeOptions[0].value
  });

  const [isWrongNetwork, setIsWrongNetwork] = useState(true);
  const [isLoading, setIsLoading] = useState(false);
  const [isSnapshotLoading, setIsSnapshotLoading] = useState(false);
  const [networksSnapshots, setNetworkSnapshots] = useState({});
  const [errors, setErrors] = useState({});
  const [signOptions, setSignOptions] = useState([]);
  const [isNewTagModalOpen, setIsNewTagModalOpen] = useState(false);
  const [selectedSignOption, setSelectedSignOption] = useState();

  const toggleNewTagModal = () => setIsNewTagModalOpen(!isNewTagModalOpen);

  useEffect(() => {
    const controller = new AbortController();
    (async () => {
      try {
        setIsLoading(true);
        const signOpts = await getSignOptions(space.key, true, styles.option, controller.signal);
        setSignOptions(signOpts ?? []);
      } catch (e) {
        console.log(e);
      }
    })();
    setIsLoading(false);
    return () => controller.abort();
  }, []);

  const onNewProposalTagSave = ({ name, color }) => {
    const newSignOption = {
      label: name.toUpperCase(),
      color: color,
      className: styles.option
    };
    setSignOptions(signOptions.concat([newSignOption]));
    setSelectedSignOption(newSignOption);
    toggleNewTagModal();
  };

  useEffect(() => {
    if (!isMember) {
      router.push(`/${space.key}`);
    } else {
      (async () => {
        try {
          setIsSnapshotLoading(true);
          let networks = space?.networks || [];

          if (!networks.includes(parseInt(currentNetwork))) {
            networks.push(parseInt(currentNetwork));
          }
          const networksSnapshots = await getSnapshotPerNetwork(networks);
          setNetworkSnapshots(networksSnapshots);
          setIsWrongNetwork(
            !networks.find((network) => parseInt(network) === parseInt(currentNetwork))
          );
        } catch (error) {
          console.log('Tasks: Snapshots loading error', error);
        } finally {
          setIsSnapshotLoading(false);
        }
      })();
    }
  }, [isMember, space.networks?.length, currentNetwork]);

  const handleChange = (name, value) => {
    setData({ ...data, [name]: value });
  };

  /**
   *
   * If user selected start date is
   * in the past, returns minimum possible
   * end date set in the future
   *
   * @param {Date} [minStartDate] - User selected minimum startDate
   */
  const getMinimumEndDate = (minStartDate) => {
    return Math.max(minStartDate, Date.now());
  };

  const saveQuestion = async () => {
    try {
      const errors = validate(data, selectedSignOption);
      if (Object.keys(errors).length) {
        return setErrors(errors);
      } else {
        setIsLoading(true);
        const model = {
          startDate: data.from.getTime(),
          endDate: data.to.getTime(),
          upDownEndDate: data.upDownEndDate.getTime(),
          title: data.title,
          body: data.question,
          maxLength: data.maxLength,
          idHolder: data.idHolder,
          submissionsPerId: data.submissionsPerId,
          tokenHolder: data.tokenHolder,
          submissionsPerAddress: data.submissionsPerAddress,
          snapshot: networksSnapshots,
          type: data.type,
          signOption: { ...selectedSignOption, type: 'task' }
        };

        const msgString = JSON.stringify({
          payload: model,
          space: space.key,
          timestamp: Date.now()
        });

        const requestData = {
          msg: msgString,
          address
        };

        const response = await axios.post('/api/tasks', { data: requestData, address });

        if (response.status === 200)
          toast.success('The task has been added!', {
            position: 'top-center'
          });

        router.push(`/${space.key}`);
      }
    } catch (error) {
      console.error('Create new task error: ' + error);
      toast.error('Task could not be created', {
        position: 'top-center'
      });
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <>
      <NewTagModal
        isOpen={isNewTagModalOpen}
        tagOptions={signOptions}
        onSave={onNewProposalTagSave}
        onClose={toggleNewTagModal}
      />
      {isLoading || isSnapshotLoading ? (
        <PageLoading />
      ) : (
        <Container>
          <div className={styles.container}>
            <div className={styles.row}>
              <div className={stylesNewProposal.description}>
                <div className={styles.box}>
                  <input
                    name="title"
                    maxLength={32}
                    className={classNames(stylesNewProposal.input, styles.question)}
                    placeholder="Title"
                    onChange={(e) => handleChange(e.target.name, e.target.value)}
                    value={data.title}
                  />
                  {errors.title && <p style={{ color: 'red', fontSize: 20 }}>{errors.title}</p>}
                  <textarea
                    name="question"
                    className={stylesNewProposal.input}
                    placeholder="Add a description"
                    onChange={(e) => handleChange(e.target.name, e.target.value)}
                    value={data.question}
                  />
                  {errors.question && (
                    <p style={{ color: 'red', fontSize: 20 }}>{errors.question}</p>
                  )}

                  {data.question.length > 0 && (
                    <div>
                      <h4 className={stylesNewProposal.preview}>Preview</h4>
                      <div className="markdown-body break-word">
                        <Markdown className="mb-6" body={data.question} />
                      </div>
                    </div>
                  )}
                </div>
              </div>

              <div className={styles.details}>
                <Block title="DETAILS">
                  <div className={styles.detail}>
                    <div className={classNames(styles.item, styles.itemDefault)}>
                      <span className={styles.label}>Start Date:</span>
                      <span className={styles.info}>
                        <span className="tooltipped tooltipped-n">
                          <DateTimeSelectorModal
                            onSelected={(date) => {
                              handleChange('from', date);
                            }}
                            defaultText="Select start date"
                            dateValue={data.from}
                          />
                          {errors.from && (
                            <p style={{ color: 'red', fontSize: 18 }}>{errors.from}</p>
                          )}
                        </span>
                      </span>
                    </div>
                    <div className={classNames(styles.item, styles.itemDefault)}>
                      <span className={styles.label}>End Date:</span>
                      <span className={styles.info}>
                        <span className="tooltipped tooltipped-n">
                          <DateTimeSelectorModal
                            onSelected={(date) => {
                              handleChange('to', date);
                            }}
                            defaultText="Select end date"
                            minDate={getMinimumEndDate(data.from)}
                            dateValue={data.to}
                          />
                          {errors.to && <p style={{ color: 'red', fontSize: 18 }}>{errors.to}</p>}
                        </span>
                      </span>
                    </div>
                    <div className={classNames(styles.item, styles.itemDefault)}>
                      <span className={styles.label}>Ranking end date:</span>
                      <span className={styles.info}>
                        <span className="tooltipped tooltipped-n">
                          <DateTimeSelectorModal
                            onSelected={(date) => {
                              handleChange('upDownEndDate', date);
                            }}
                            isRankingDate
                            defaultText="Select ranking end date"
                            dateValue={data.upDownEndDate}
                            minDate={getMinimumEndDate(data.to)}
                          />
                          {errors.upDownEndDate && (
                            <p style={{ color: 'red', fontSize: 18 }}>{errors.upDownEndDate}</p>
                          )}
                        </span>
                      </span>
                    </div>
                    <div className={classNames(styles.item, styles.itemDefault)}>
                      <span className={styles.label}>Max sub. length:</span>
                      <span className={styles.info}>
                        <input
                          className={styles.input}
                          name="maxLength"
                          type="number"
                          defaultValue={data.maxLength}
                          onChange={(e) => handleChange(e.target.name, e.target.value)}
                        />
                        {errors.maxLength && (
                          <p style={{ color: 'red', fontSize: 20 }}>{errors.maxLength}</p>
                        )}
                      </span>
                    </div>

                    <div className={classNames(styles.item, styles.itemOpenTo)}>
                      <span className={styles.label}>Task type:</span>
                      <span className={styles.info}>
                        <Dropdown
                          className={styles.dropdown}
                          controlClassName={styles.control}
                          placeholderClassName={styles.placeholder}
                          menuClassName={styles.menu}
                          options={typeOptions}
                          placeholder={data.type.label}
                          value={data.type.value}
                          onChange={(item) => handleChange('type', item.value)}
                          arrowClosed={<span className={styles.arrowClosed} />}
                          arrowOpen={<span className={styles.arrowOpen} />}
                        />
                      </span>
                    </div>
                    <div className={styles.item}>
                      <span className={styles.label} style={{ marginBottom: 21 }}>
                        Tag:
                      </span>
                      <span className={styles.info}>
                        <Dropdown
                          className={styles.dropdown}
                          controlClassName={styles.control}
                          placeholderClassName={styles.placeholder}
                          menuClassName={styles.menu}
                          options={signOptions}
                          placeholder="Tag"
                          value={selectedSignOption?.label}
                          onChange={(i) => setSelectedSignOption(i)}
                          arrowClosed={<span className={styles.arrowClosed} />}
                          arrowOpen={<span className={styles.arrowOpen} />}
                        />
                        <button onClick={toggleNewTagModal} className={styles.newTagButton}>
                          Add new tag
                        </button>
                        {errors.signOption ? (
                          <p style={{ color: 'red', fontSize: 18, margin: 0 }}>
                            {errors.signOption}
                          </p>
                        ) : null}
                      </span>
                    </div>
                    <div className={classNames(styles.item, styles.itemOpenTo)}>
                      <span className={styles.label}>Open to:</span>
                    </div>
                    <div className={classNames(styles.item, styles.itemHolder)}>
                      <div className={styles.holders}>
                        <input
                          name="idHolder"
                          defaultChecked={data.idHolder}
                          type="checkbox"
                          onChange={(e) => handleChange(e.target.name, e.target.checked)}
                        />
                        <span className={styles.label}>ID holders</span>
                      </div>
                    </div>
                    <div className={classNames(styles.item, styles.itemToken)}>
                      <div className={styles.tokens}>
                        <span className={styles.tokensLabel}>Submissions per ID</span>
                        <input
                          className={styles.input}
                          name="submissionsPerId"
                          type="number"
                          placeholder="1"
                          min={1}
                          disabled={!data.idHolder}
                          defaultValue={data.submissionsPerId}
                          onChange={(e) => handleChange(e.target.name, e.target.value)}
                        />
                      </div>
                    </div>
                    <div>
                      <div className={classNames(styles.item, styles.itemHolder)}>
                        <div className={styles.holders}>
                          <input
                            name="tokenHolder"
                            defaultChecked={data.tokenHolder}
                            type="checkbox"
                            onChange={(e) => handleChange(e.target.name, e.target.checked)}
                          />
                          <span className={styles.label}>Token holders</span>
                        </div>
                      </div>
                      <div className={classNames(styles.item, styles.itemDefault)}>
                        <div className={styles.tokens}>
                          <span className={styles.tokensLabel}>Submissions per address</span>
                          <input
                            className={styles.input}
                            name="submissionsPerAddress"
                            type="number"
                            min={1}
                            placeholder="1"
                            disabled={!data.tokenHolder}
                            defaultValue={data.submissionsPerAddress}
                            onChange={(e) => handleChange(e.target.name, e.target.value)}
                          />
                        </div>
                      </div>
                      {errors.negativeNumbers && (
                        <p style={{ color: 'red', fontSize: 18 }}>{errors.negativeNumbers}</p>
                      )}
                      {errors.noHolders && (
                        <p style={{ color: 'red', fontSize: 18 }}>{errors.noHolders}</p>
                      )}
                    </div>
                    <Button
                      disabled={isWrongNetwork}
                      onClick={saveQuestion}
                      className={styles.button}>
                      Publish
                    </Button>
                  </div>
                </Block>
              </div>
            </div>
          </div>
        </Container>
      )}
    </>
  );
};

export default NewQuestion;
