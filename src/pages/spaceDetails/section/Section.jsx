import If from 'components/If';
import FilterSpace from 'components/Ui/FilterSpace';
import LogoLoader from 'components/Ui/LogoLoader';
import TablePagination from 'components/Ui/TablePagination';
import { setGlobalState, useGlobalState } from 'globalStateStore';
import filterImg from 'images/filter.svg';
import plusImage from 'images/plus.svg';
import Image from 'next/image';
import Link from 'next/link';
import { useState } from 'react';
import styles from './Section.module.scss';

const Section = ({
  isLoading,
  header,
  data,
  signOptions,
  sectionKey,
  ItemComponent,
  stats,
  isMember,
  createLink,
  onDeleteButtonClick,
  onPublishButtonClick,
  getSpaceKey,
  listFilterIndicator
}) => {
  const FILTERS_KEY = {
    FILTERS: `${sectionKey}ListFilters`,
    ACTIVE_PAGE: `${sectionKey}ListActivePage`,
    SORTING: `${sectionKey}ListSorting`
  };

  const [listActivePage] = useGlobalState(FILTERS_KEY.ACTIVE_PAGE);
  const [settings, setSettings] = useState(false);

  const onPaginationChange = (pagination) => {
    setGlobalState(FILTERS_KEY.ACTIVE_PAGE, pagination.selected + 1);
  };

  const filterCloseHandler = () => {
    setSettings(false);
  };
  return (
    <div className={styles.container}>
      {settings && (
        <FilterSpace
          listFilterIndicator={listFilterIndicator}
          tagItems={signOptions}
          onClose={filterCloseHandler}
        />
      )}
      <div className={styles.headerContainer}>
        <div className={styles.header}>{header}</div>
        <div className={styles.actionBtn}>
          <If condition={isMember}>
            <Link href={createLink} className={styles.linkbtn}>
              <Image src={plusImage} alt="add-button" />
            </Link>
          </If>
          <button className={styles.filterBtn} onClick={() => setSettings((settings) => !settings)}>
            <Image src={filterImg} alt="filter-button" />
          </button>
        </div>
      </div>
      <div className={styles.itemsContainer}>
        {isLoading ? (
          <div className={styles.logoLoader}>
            <LogoLoader />
          </div>
        ) : null}

        {data?.length && !isLoading > 0
          ? data.map((item, index) => (
              <ItemComponent
                key={index}
                data={item}
                onDeleteButtonClick={onDeleteButtonClick}
                onPublishButtonClick={onPublishButtonClick}
                isMember={isMember}
                getSpaceKey={getSpaceKey}
              />
            ))
          : null}

        {!isLoading && data?.length === 0 ? <p>There aren&apos;t any items here yet!</p> : null}
      </div>
      <div className={styles.pagination}>
        <TablePagination
          forcePage={listActivePage - 1}
          onPageChange={onPaginationChange}
          pageCount={stats?.pagesCount}
          totalRows={stats?.total}
        />
      </div>
    </div>
  );
};

export default Section;
