import axios from 'axios';
import { useGlobalState } from 'globalStateStore';
import { getSignOptions } from 'helpers/lists';
import { useEffect, useState } from 'react';
import Section from '../Section';

const SubmissionSection = (props) => {
  const { space } = props;
  const [isLoading, setIsLoading] = useState(true);
  const [signOptions, setSignOptions] = useState([]);
  const [taskStats, setTaskStats] = useState({ pagesCount: 1, total: 0 });
  const [questionsList, setQuestionsList] = useState([]);

  const [taskListFilters] = useGlobalState('taskListFilters');
  const [taskListSorting] = useGlobalState('taskListSorting');
  const [taskListActivePage] = useGlobalState('taskListActivePage');

  const getSpaceKey = () => {
    let key = space.key;
    if (space.parentNamespace?.key) key = `${space.parentNamespace.key}-${space.key}`;
    return key;
  };

  const getQuestionsList = async () => {
    const requestParams = {
      status: taskListFilters.status.join(),
      signId: taskListFilters.sign.join(),
      pageNumber: taskListActivePage,
      itemsPerPage: taskListFilters.itemsPerPage,
      sortColumnId: taskListSorting.column,
      sortDirection: taskListSorting.direction
    };
    if (space.key) {
      setIsLoading(true);
      try {
        const signOptions = await getSignOptions(space.key);
        const { data } = await axios.get(`/api/tasks/${space.key}`, {
          params: requestParams
        });

        const pagesCount = Math.ceil(data.totalCount / Number(taskListFilters.itemsPerPage));
        setTaskStats({ pagesCount, total: data.totalCount });

        setSignOptions(signOptions);
        setQuestionsList(data.questions);
      } catch (error) {
        console.log('GET_SUBMISSIONS_TASKS_LIST_FAILURE', error);
      } finally {
        setIsLoading(false);
      }
    }
  };

  useEffect(() => {
    (async () => {
      await getQuestionsList();
    })();
  }, [
    space.key,
    taskListFilters.status,
    taskListFilters.sign,
    taskListFilters.itemsPerPage,
    taskListActivePage,
    taskListSorting.column,
    taskListSorting.direction
  ]);

  return (
    <Section
      {...props}
      createLink={`/${space.key}/admin/task`}
      isLoading={isLoading}
      data={questionsList}
      stats={taskStats}
      signOptions={signOptions}
      getSpaceKey={getSpaceKey}
      listFilterIndicator="task"
    />
  );
};

export default SubmissionSection;
