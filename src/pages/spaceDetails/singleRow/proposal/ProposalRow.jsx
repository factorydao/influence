import classNames from 'classnames';
import Button from 'components/Ui/Button';
import DeleteIcon from 'components/Ui/DeleteIcon/DeleteIcon.component';
import PublishIcon from 'components/Ui/PublishIcon/PublishIcon.component';
import Sign from 'components/Ui/Sign';
import State from 'components/Ui/State';
import { n } from 'helpers/utils';
import moment from 'moment';
import { useRouter } from 'next/router';
import styles from '../SingleRow.module.scss';
import SingleColumn from '../singleColumn/SingleColumn';

const ProposalItemComponent = ({ data, onDeleteButtonClick, onPublishButtonClick, isMember }) => {
  const { sign, color } = data;
  const { msg, strategyName, score, isDraft } = data[1];
  const hash = data[0];

  const router = useRouter();

  const handleSingleRowClick = () => {
    if (window?.location.hostname === 'vote.ethbrno.cz') {
      return router.push(`ethbrno/proposal/${hash}`);
    }
    router.push(`${router.query.spaceKey}/proposal/${hash}`);
  };
  return (
    <div className={styles.singleRow} onClick={handleSingleRowClick}>
      <div className={styles.mainData}>
        <div className={styles.headerRow}>
          <div className={styles.itemCol}>
            <div className={styles.sign}>
              <Sign color={color}>{sign}</Sign>
            </div>
          </div>
          <div className={styles.itemCol}>
            <div className={styles.title}>{msg.payload.name}</div>
          </div>
        </div>
        <div className={styles.dataColumns}>
          <SingleColumn name="start date" value={moment(msg.payload.start).format('DD/MM/YYYY')} />
          <SingleColumn name="end date" value={moment(msg.payload.end).format('DD/MM/YYYY')} />
          <SingleColumn name="strategy" value={strategyName} width={120} />
          <SingleColumn name="votes" value={n(score)} width={60} />
        </div>
      </div>
      <div className={styles.actionColumn}>
        <div className={styles.itemCol}>
          <div>
            <State
              startDate={msg.payload.start}
              endDate={msg.payload.end}
              // isPrevote={msg.payload.preVote}
              isDraft={isDraft}
            />
          </div>
        </div>
        <div className={styles.deleteTd}>
          {isDraft && isMember ? (
            <div className={styles.icons}>
              <Button onClick={(e) => onDeleteButtonClick(e, hash)} className={styles.icon}>
                <DeleteIcon />
              </Button>
              {msg.payload.end < Date.now() ? null : (
                <Button
                  onClick={(e) => onPublishButtonClick(e, hash)}
                  className={classNames(styles.icon, styles.publish)}>
                  <PublishIcon />
                </Button>
              )}
            </div>
          ) : null}
        </div>
      </div>
    </div>
  );
};
export default ProposalItemComponent;
