import styles from './SingleColumn.module.scss';

const SingleColumn = ({ name, value, width }) => {
  return (
    <div className={styles.dataColumn} style={{ width: width }}>
      <span className={styles.columnName}>{name}</span>
      <div className={styles.columnValue}>{value}</div>
    </div>
  );
};

export default SingleColumn;
