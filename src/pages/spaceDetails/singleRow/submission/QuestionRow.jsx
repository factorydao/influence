import Sign from 'components/Ui/Sign';
import State from 'components/Ui/State';
import moment from 'moment';
import { useRouter } from 'next/router';
import SingleColumn from '../singleColumn/SingleColumn';
import styles from '../SingleRow.module.scss';

const QuestionRowComponent = ({ data, getSpaceKey }) => {
  const router = useRouter();
  return (
    <div
      className={styles.singleRow}
      onClick={() => {
        router.push(`/${getSpaceKey()}/task/${data.questionHash}`);
      }}>
      <div className={styles.mainData}>
        <div className={styles.headerRow}>
          <div className={styles.itemCol}>
            <div className={styles.sign}>
              <Sign color={data.sign?.color}>{data.sign?.label}</Sign>
            </div>
          </div>
          <div className={styles.itemCol}>
            <div className={styles.title}>{data.title}</div>
          </div>
        </div>
        <div className={styles.dataColumns}>
          <SingleColumn name="start date" value={moment(data.startDate).format('DD/MM/YYYY')} />
          <SingleColumn name="end date" value={moment(data.endDate).format('DD/MM/YYYY')} />
          <SingleColumn name="type" value={data.type ?? 'text'} />
          <SingleColumn name="submissions" value={data.submissionsCount} />
        </div>
      </div>
      <div className={styles.actionColumn}>
        <div className={styles.itemCol}>
          <div>
            <State
              isProposal={false}
              startDate={data.startDate}
              endDate={data.endDate}
              upDownEndDate={data.upDownEndDate}
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default QuestionRowComponent;
