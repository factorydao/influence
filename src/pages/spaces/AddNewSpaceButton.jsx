import { useRouter } from 'next/router';
import React from 'react';
import styles from './Spaces.module.scss';

/**
 * Add new space button
 *
 * @returns Add new space button
 */

const AddNewSpaceButton = () => {
  const router = useRouter();

  const goToNewSpaceForm = () => {
    router.push('/admin/add-new-realm');
  };

  return (
    <button className={styles.addSpaceContainer} onClick={goToNewSpaceForm}>
      <div className={styles.addButton}>
        <div className={styles.oval}>
          <div className={styles.cross} />
        </div>
      </div>
      <div className={styles.addName}>add realm</div>
    </button>
  );
};

export default AddNewSpaceButton;
