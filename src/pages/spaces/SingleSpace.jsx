import editIcon from 'assets/edit.svg';
import { useGlobalState } from 'globalStateStore';
import Image from 'next/image';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { useState } from 'react';

import styles from './SingleSpace.module.scss';

/**
 *
 * Single space component
 *
 * @returns Single space component
 */

const SingleSpaceComponent = ({ space }) => {
  const [isGlobalAdmin] = useGlobalState('isGlobalAdmin');
  const [globalSpace] = useGlobalState('space');
  const router = useRouter();
  const [imageSrc, setImageSrc] = useState(
    `https://factorydao.infura-ipfs.io/ipfs/${space.imageHash}`
  );
  /**
   * Go to single space to show details
   *
   * @param {string} spaceKey - single space key
   */
  const goToSingleSpaceDetails = (spaceKey) => {
    let url = `/${spaceKey}`;
    if (globalSpace.isNamespace) url = `/${globalSpace.key}-${spaceKey}`;
    router.push(url);
  };

  const isSpaceVisible = isGlobalAdmin ? true : !space.hidden;

  return space && isSpaceVisible ? (
    <div className={styles.container}>
      <div className={styles.spaceNav}>
        {/* {space.totalActiveProposals ? (
            <div className={styles.activesCount}>{space.totalActiveProposals.count}</div>
          ) : null} */}
        {isGlobalAdmin ? (
          <Link href={`/${space.key}/admin/edit`} className={styles.spaceNav}>
            <Image src={editIcon} alt="Edit" />
          </Link>
        ) : null}
      </div>
      <button className={styles.button} onClick={() => goToSingleSpaceDetails(space.key)}>
        <Image
          alt={space.name + '_logo'}
          style={{ width: space.width || '70px', height: space.height || '100px' }}
          className={styles.icon}
          width={space.width || 70}
          height={space.height || 100}
          src={imageSrc}
          onError={() => setImageSrc(`/api/spaces/${space.key}/image?type=logo`)}
        />
        <div className={styles.name}>{space.name}</div>
      </button>
    </div>
  ) : null;
};

export default SingleSpaceComponent;
