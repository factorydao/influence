import axios from 'axios';
import SplashScreen from 'components/Spaces/Influence/SplashScreen';
import PageLoading from 'components/Ui/PageLoading';
import SearchBox from 'components/Ui/SearchBox';
import { useGlobalState } from 'globalStateStore';
import Image from 'next/image';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';
import banner from '../../assets/ethprague_canvas_banner.jpg';
import mobileBanner from '../../assets/ethprague_canvas_banner_mobile.jpg';
import AddNewSpaceButton from './AddNewSpaceButton';
import SingleSpaceComponent from './SingleSpace';
import styles from './Spaces.module.scss';

const LOADING_TYPE = {
  splash: 'splash',
  loading: 'loading'
};

/**
 *
 * Spaces page
 *
 * @returns The view to show all spaces
 */

const SpacesPage = () => {
  const [isGlobalAdmin] = useGlobalState('isGlobalAdmin');
  const [space] = useGlobalState('space');
  const [spaces, setSpaces] = useState([]);
  const router = useRouter();
  const [loading, setLoading] = useState(
    router.state?.prevPath ? LOADING_TYPE.loading : LOADING_TYPE.splash
  );
  const [searchValue, setSearchValue] = useState('');
  const spaceKey = router.query.spaceKey || undefined;
  const showEthPragueBanner = window?.location.href.includes('/ethprague');

  useEffect(() => {
    (async () => {
      await getAllSpaces();
    })();
  }, []);

  /**
   * Gets all spaces data from API
   */
  const getAllSpaces = async () => {
    try {
      let apiUrl = `/api/spaces`;
      if (spaceKey) {
        apiUrl += `/namespace/${space._id}`;
      }
      let spacesData = await axios.get(apiUrl);
      if (spacesData?.data) {
        setSpaces(spacesData.data);
      }
    } catch (error) {
      console.log('Get all spaces error:' + error);
    } finally {
      setLoading(false);
    }
  };
  /**
   * Search for matching spaces
   *
   * @param {string} value - value to set in search input
   */
  const onChangeSearchValue = (value) => {
    setSearchValue(value);
  };

  return (
    <>
      {loading ? (
        <>
          {loading === LOADING_TYPE.splash && <SplashScreen />}
          {loading === LOADING_TYPE.loading && <PageLoading />}
        </>
      ) : (
        <div className={styles.container}>
          {showEthPragueBanner ? (
            <Image
              style={{ maxWidth: '100%', height: 'auto' }}
              src={window?.innerWidth < 768 ? mobileBanner : banner}
              alt="ETH Prague Banner"
            />
          ) : null}
          <div className={styles.searchContainer}>
            <SearchBox searchValue={searchValue} onChangeSearchValue={onChangeSearchValue} />
          </div>
          {spaces.length <= 0 ? <p>There aren&apos;t any realms here yet!</p> : null}
          <div className={styles.spaces}>
            {spaces.length > 0
              ? spaces
                  .filter((space) => space.name.toUpperCase().includes(searchValue.toUpperCase()))
                  .map((space) => <SingleSpaceComponent key={space.key} space={space} />)
              : null}
            {isGlobalAdmin ? <AddNewSpaceButton /> : null}
          </div>
        </div>
      )}
    </>
  );
};

export default SpacesPage;
