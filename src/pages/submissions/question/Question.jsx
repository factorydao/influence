import If from 'components/If';
import Contributors from 'components/Submissions/Contributors';
import DetailsSection from 'components/Submissions/DetailsSection';
import QuestionDetails from 'components/Submissions/QuestionDetails';
import SubmissionsTab from 'components/Submissions/SubmissionsTab';
import TabBar from 'components/Submissions/TabBar';
import Button from 'components/Ui/Button';
import Container from 'components/Ui/Container';
import Dialog from 'components/Ui/Dialog';
import Icon from 'components/Ui/Icon';
import PageLoading from 'components/Ui/PageLoading';
import { useGlobalState } from 'globalStateStore';
import { useGetTaskData } from 'helpers/queries';
import { STATUSES } from 'helpers/statuses';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';
import styles from './Question.module.scss';

const NumberOfSubmissionSelector = ({
  isOpen,
  onClick,
  submissionsAmount,
  setSubmissionsAmount,
  onClose
}) => {
  return (
    <Dialog isOpen={isOpen} handleClose={onClose}>
      <div className={styles.triggerContainer}>
        <span className={styles.triggerTitle}>
          Number of submissions with the highest upvote count
        </span>
        <input
          type="number"
          defaultValue={submissionsAmount}
          onChange={(e) => setSubmissionsAmount(e.target.value)}
        />
        <Button className={styles.linkbtn} onClick={onClick}>
          Submit
        </Button>
      </div>
    </Dialog>
  );
};

const Question = () => {
  const router = useRouter();
  let { hash } = router.query;
  const [space] = useGlobalState('space');
  const [isMember] = useGlobalState('isMember');
  const [address] = useGlobalState('address');
  const [tab, setTab] = useState('submissions');
  const [triggerVoteDialogOpen, setTriggerVoteDialogOpen] = useState(false);
  const tabChange = (tab) => setTab(tab);
  const [isTwitterSharing, setTwitterSharing] = useState(false);

  const {
    data: {
      identitiesForRealmByUser,
      contributorsData,
      isEligible,
      questionData,
      submissionsData,
      mySubmissionsData
    },
    isFetched,
    isLoading,
    refetch: refetchTaskData
  } = useGetTaskData(hash, space?.key, address);

  const [submissionsAmount, setSubmissionsAmount] = useState(10);

  const getSpaceKey = () => {
    let key = space.key;
    if (space.parentNamespace?.key) key = `${space.parentNamespace.key}-${space.key}`;
    return key;
  };
  const handleBackClick = () => router.push(`/${getSpaceKey()}`);

  const redirectToNewProposalForm = () => {
    router.push({
      pathname: `/${space.key}/admin/create/`,
      query: { taskHash: hash, submissionsAmount }
    });
  };

  useEffect(() => {
    if (!submissionsData.length) return;
    setSubmissionsAmount(submissionsData.length);
  }, [submissionsData.length]);

  const submissionProps = {
    ...questionData,
    isTwitterSharing,
    refetchTaskData,
    setTwitterSharing
  };

  const isPageLoading = isLoading || !isFetched || isTwitterSharing;

  return (
    <Container>
      {isPageLoading ? (
        <PageLoading />
      ) : (
        <div className={styles.container}>
          <NumberOfSubmissionSelector
            isOpen={triggerVoteDialogOpen}
            onClose={() => setTriggerVoteDialogOpen(false)}
            submissionsAmount={submissionsAmount}
            setSubmissionsAmount={setSubmissionsAmount}
            onClick={redirectToNewProposalForm}
          />
          <div className={styles.actionsContainer}>
            <button className={styles.backButton} onClick={handleBackClick}>
              <Icon name="back" className={styles.backIcon} />
              <span style={{ marginLeft: 5 }}>Tasks</span>
            </button>
          </div>
          <div className={styles.row}>
            <QuestionDetails
              data={questionData}
              isEligible={isEligible}
              identities={identitiesForRealmByUser}
              onReload={refetchTaskData}
            />
            <div className={styles.details}>
              <If condition={isMember && questionData?.status === STATUSES.CLOSED}>
                <Button
                  className={styles.linkbtn}
                  onClick={() => setTriggerVoteDialogOpen(true)}
                  disabled={submissionsData.length < 2}>
                  Trigger vote
                </Button>
              </If>
              <DetailsSection space={space} data={{ questionData, submissionsData }} />
            </div>
          </div>
          <div className={styles.row}>
            <div className={styles.navbar}>
              <TabBar
                onItemClick={tabChange}
                selectedTab={tab}
                counter={Object.keys(contributorsData).length || 0}
                isMySubmissionTabDisabled={
                  (!questionData.isOpenTask && mySubmissionsData.length === 0) ||
                  (!isEligible && mySubmissionsData.length === 0)
                }
              />
            </div>
          </div>

          <div className={styles.row}>
            <If condition={tab === 'submissions'}>
              <SubmissionsTab {...submissionProps} data={submissionsData} />
            </If>
            <If condition={tab === 'mySubmissions'}>
              <SubmissionsTab {...submissionProps} data={mySubmissionsData} />
            </If>
            <If condition={tab === 'contributors'}>
              <Contributors
                contributorsData={contributorsData}
                idHolder={questionData.idHolder}
                tokenHolder={questionData.tokenHolder}
              />
            </If>
          </div>
        </div>
      )}
    </Container>
  );
};

export default Question;
