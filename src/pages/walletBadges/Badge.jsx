import classNames from 'classnames';
import LogoLoader from 'components/Ui/LogoLoader';
import { useState } from 'react';
import { Link } from 'react-router-dom';
import styles from './Badge.module.scss';

/**
 *
 * Badge image is assigned to an NFT id which user has
 *
 * @param {Object} badge - badge data
 * @returns An image with a loading spinner with ipfs as source
 */

const Badge = ({ className, badge }) => {
  const { badgeIpfsHash, proposalName, proposalIpfsHash, proposalSpaceKey } = badge;

  const [isLoading, setIsLoading] = useState(true);
  const ipfs = `https://factorydao.infura-ipfs.io/ipfs/${badgeIpfsHash}`;
  const proposalLink = `/${proposalSpaceKey}/proposal/${proposalIpfsHash}`;

  return (
    <div className={classNames(styles.container, className)}>
      <Link href={proposalLink}>
        {/* eslint-disable-next-line @next/next/no-img-element */}
        <img
          alt="badge"
          className={classNames(styles.badge, isLoading && styles.badgeLoading)}
          src={ipfs}
          onLoad={() => setIsLoading(false)}
        />
      </Link>
      <Link href={proposalLink}>
        <p className={styles.title}>
          From &quot;<span className={styles.titleHighlight}>{proposalName}</span>&quot;
        </p>
      </Link>
      {isLoading && <LogoLoader classes={styles.loader} />}
    </div>
  );
};

export default Badge;
