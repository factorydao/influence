import axios from 'axios';
import If from 'components/If';
import Container from 'components/Ui/Container';
import PageLoading from 'components/Ui/PageLoading';
import { useGlobalState } from 'globalStateStore';
import { useEffect, useState } from 'react';
import { toast } from 'react-toastify';
import Badge from './Badge';
import styles from './WalletBadges.module.scss';

/**
 *
 * Wallet badges page
 *
 * @returns The view to show all badges assigned to user's wallet address
 */

const WalletBadges = () => {
  const [address] = useGlobalState('address');
  const [loading, setLoading] = useState(false);
  const [badges, setBadges] = useState([]);
  const pluralBadgesLabel = badges.length === 1 ? 'badge' : 'badges';

  useEffect(() => {
    if (address) {
      (async () => {
        await getUserBadges();
      })();
    }
  }, [address]);

  /**
   * Gets all user badges
   */
  const getUserBadges = async () => {
    const signal = new AbortController().signal;
    // const unlistenHistory = history.listen(() => {
    //   source.cancel();
    // });

    try {
      setLoading(true);
      const { data } = await axios.get(`/api/badges/${address}`, { signal });
      setBadges(data);
    } catch (error) {
      if (error.message !== 'Cancel') {
        console.error('Get user badges error:' + error);
        toast.error('Badges could not be loaded');
      }
    } finally {
      // unlistenHistory();
      setLoading(false);
    }
  };

  if (loading) return <PageLoading />;

  if (!address)
    return (
      <Container>
        <p className={styles.container}>Connect your wallet to view your badges.</p>
      </Container>
    );

  return (
    <Container>
      <div className={styles.container}>
        {!badges.length && <p>No badges found for your wallet</p>}
        <If condition={badges.length}>
          <h1 className={styles.title}>
            Your have {badges.length} {pluralBadgesLabel}!
          </h1>
        </If>
        <div className={styles.badges}>
          {badges.map((badge) => (
            <div className={styles.badge} key={`${badge.nftContract}-${badge.nftId}`}>
              <Badge badge={badge} />
            </div>
          ))}
        </div>
      </div>
    </Container>
  );
};

export default WalletBadges;
